<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
	    <title>Contract confirmation: Welcome to Webfleet Solutions!</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!--[if !mso]><!-->
	    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <!--<![endif]-->
		<link rel="apple-touch-icon" sizes="180x180" href="https://media.webfleet.com/branding/wfs/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="https://media.webfleet.com/branding/wfs/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="194x194" href="https://media.webfleet.com/branding/wfs/favicon-194x194.png">
		<link rel="icon" type="image/png" sizes="192x192" href="https://media.webfleet.com/branding/wfs/android-chrome-192x192.png">
		<link rel="icon" type="image/png" sizes="16x16" href="https://media.webfleet.com/branding/wfs/favicon-16x16.png">
		<link rel="mask-icon" href="https://media.webfleet.com/branding/wfs/safari-pinned-tab.svg" color="#ff0000">
	    <style type="text/css">
		    /* ---------------------------- EMAIL CLIENT RESETS --------------------------------- */
		    #outlook a{
			    padding:0;
			}
	        .ReadMsgBody{
		        width:100%;
		    } .ExternalClass{
			    width:100%;
			}
	        .ExternalClass, .ExternalClass span, .ExternalClass td, .ExternalClass div {
		        line-height: 100%;
		    }
			table, td{
				mso-table-lspace:0pt;
				mso-table-rspace:0pt;
				border-collapse: collapse;
			}
			body {
				height: 100% !important;
				width: 100% !important;
			    margin: 0 !important;
			    padding: 0 !important;
			}
			img {
				-ms-interpolation-mode: bicubic;
				border: 0px;
				outline: none;
				text-decoration: none;
				display: block;
			}
			table {
			    border-spacing: 0;
			}
			td, th {
			    padding: 0;
			    font-weight: normal;
			}
			div[style*="margin: 16px 0"] { 
				margin:0 !important;
			}
			p {
				margin-bottom: 10px;
			}
		</style>
	    <style type="text/css">
		    /* ---------------------------- TOMTOM STYLES --------------------------------- */
		    /* ---------------- Body ---------------- */
		    body {
			    font-family: Verdana, sans-serif;
			    font-weight: normal;
			    color: #455560;
			    font-size: 12px;
			    line-height: normal;
		    }
		    a {
			    color: #a3bd31;
			    text-decoration: none;
			    border: none;
			}
			a:hover {
			    color: #a3bd31 !important;
			    text-decoration: underline !important;
			    border: none !important;
			}		    
		    /* ---------------- Layouts ---------------- */		    
		    container {
			    margin: 0 auto;
		    }
		    .one-column, .two-column, .three-column, .four-column {
				text-align: left;
				font-weight: 400;
			}		    
		    .login {
			    height: 85px;
			    border-bottom: 5px solid #BDD731;
		    }
		    .login a {
			    color: #455560;
			    font-weight: bold;
		    }
		    .login a:hover {
			    color: #455560 !important;
			}
			.divider {
				width: 25px;
				text-align: center;
				display: inline-block;
				color: #616e78;
			}
		    /* ---------------- Text Styles ---------------- */
		    strong {
			    font-weight: bold;
		    }
		    i { 
			    font-style: italic;
		    }
		    .view-online {
			    font-size: 10px;
			    font-family: Verdana, sans-serif;
			    font-weight: normal;			    
			    color: #455560;
			    text-decoration: underline;
		    }
		    .view-online:hover {
			    color: #455560 !important;			    
			    text-decoration: none !important;
		    }
		    .h1-txt {
			    font-size: 28px; 
			    line-height: 32px;
			    color: #455560 !important;			    
			    font-family: Verdana, sans-serif;
			    padding-bottom: 20px;
			    font-weight: 600;
		    }
		    .h2-txt {
			    font-size: 20px; 
			    line-height: 24px;
			    color: #455560;			    
			    font-family: Verdana, sans-serif;
			    padding-bottom: 20px;
			    font-weight: 600;
			    text-decoration: none;
		    }
		    .h3-txt {
			    font-size: 16px; 
			    line-height: 20px;
			    color: #455560;			    
			    font-family: Verdana, sans-serif;
			    padding-bottom: 20px;
			    font-weight: 600;
			    text-decoration: none;
		    }
		    .h4-txt {
			    font-size: 14px; 
			    line-height: 18px;
			    color: #455560;			    
			    font-family: Verdana, sans-serif;
			    padding-bottom: 20px;
			    font-weight: 600;
			    text-decoration: none;
		    }		    
		    .h1-txt a, .h2-txt a, .h3-txt a, .h4-txt a {
			    color: #455560;			    
			    text-decoration: none;
		    }
		    .h1-txt a:hover, .h2-txt a:hover, .h3-txt a:hover, .h4-txt a:hover {
			    color: #455560 !important;			    
			    text-decoration: none !important;
		    }		    
		    .normal-weight {
			    font-weight: normal !important;
		    }
		    .normal-txt {
			    font-family: Verdana, sans-serif;
			    font-weight: normal;
			    color: #455560;
			    font-size: 12px;
			    line-height: 18px;
		    }
		    .small-txt {
			    font-size: 10px;
			    font-weight: normal;
		    }		    
		    .footer-txt {
			    font-family: Verdana, sans-serif;
			    font-weight: normal;
			    color: #ffffff !important;
			    font-size: 12px;
			    line-height: 18px;
		    }
		    .footer-menu {
			    font-family: Verdana, sans-serif;
			    font-weight: normal;
			    color: #ffffff;
			    font-size: 10px;
			    line-height: 16px;
		    }		    		    
		    .footer-menu a {		    
			    font-family: Verdana, sans-serif;
			    font-weight: normal;
			    color: #ffffff;
			    font-size: 10px;
			    line-height: 16px;			    
			}
		    .footer-menu a:hover {		    
			    color: #ffffff !important;	    
			    text-decoration: underline !important;
			}			
			/*ul, ol {
				margin: 0;
			}		*/    
			.mobile-show {
				display: none;
				width: 0;
				height: 0;
				font-size: 0;
			}
			.no-padding {
				padding-bottom: 0; 
			}
		    /* ---------------- CTA's ---------------- */
		    .text-cta {
			    color: #455560;
			    text-decoration: none;
			    font-weight: normal;
		    }
		    text-cta:hover {
			    color: #000000 !important;
		    }
		    .footer {
			    color: #ffffff !important;
		    }
		    .footer-link {
			    color: #ffffff;
			    text-decoration: none;
			    border: none;
			}
			.footer-link:hover {
			    color: #ffffff !important;
			    text-decoration: underline !important;
			    border: none !important;
			}
			/* ---------------- Buttons ---------------- */
			.button-green-bg, 
			.button-black-bg, 
			.button-grey-bg, 
			.button-white-bg { height: 38px; line-height: 38px; -moz-border-radius: 30px; -webkit-border-radius: 30px; border-radius: 30px; }
			
			.button-green, 
			.button-black, 
			.button-grey,
			.button-white { padding: 15px; height: 8px; line-height: 7px; text-decoration: none; text-align: center; font-weight: normal; font-size: 12px; display: block; -moz-border-radius: 30px; -webkit-border-radius: 30px; border-radius: 30px;  }
			
			.button-green:hover,
			.button-black:hover,
			.button-grey:hover,
			.button-white:hover { text-decoration: none !important; }			
			
			.button-green span, 
			.button-black span, 
			.button-grey span,
			.button-white span { font-size: 14px; font-weight: bold !important; }			
			
			.button-green-bg 	{ background-color: #BDD731; } /* add this to TD bgcolor="#BDD731" */
			.button-green 		{ background-color: #BDD731; border: 1px solid #BDD731; color: #455560; }
			.button-green:hover { background-color: #D1E952 !important; border: 1px solid #94AA21 !important; color: #455560 !important; }
			.button-green span	{ color: #455560; }
			
			.button-black-bg 	{ background-color: #455560; } /* add this to TD bgcolor="#455560" */
			.button-black 		{ background-color: #455560; border: 1px solid #455560; color: #ffffff; }
			.button-black:hover { background-color: #303b42 !important; border: 1px solid #455560 !important; color: #ffffff !important; }
			.button-green span	{ color: #ffffff; }			

			.button-grey-bg 	{ background-color: #dededf; } /* add this to TD bgcolor="#dededf" */
			.button-grey 		{ background-color: #dededf; border: 1px solid #dededf; color: #455560; }
			.button-grey:hover 	{ background-color: #d2d4d5 !important; border: 1px solid #dededf !important; color: #455560 !important; }
			.button-green span	{ color: #455560; }			

			.button-white-bg 	{ background-color: #ffffff; } /* add this to TD bgcolor="#ffffff" */
			.button-white 		{ background-color: #ffffff; border: 1px solid #d2d4d5; color: #455560; }
			.button-white:hover { background-color: #cccccc !important; border: 1px solid #b1b6b9 !important; color: #455560 !important; }				
			.button-green span	{ color: #455560; }
			
			.button-link { color: #455560; }
			.button-link:hover { text-decoration: none !important; }			
			.button-link span { color: #BDD731 }
	    </style>
	    
	    <style type="text/css">
		    /* ---------------------------- MEDIA QUERIES --------------------------------- */
		    @media screen and (max-width: 649px) {
				/* ---------------- Layouts ---------------- */
				.container {
					width: 100% !important;
					margin: 0 auto !important;
				}
				.padding-container {
					width: 85% !important;
					margin: 0 auto !important;
				}
				.border img {
				    display: block !important;
				    width: 100% !important;					
				}
				.stack {
				    display: block !important;
				    width: 100% !important;
				}
				.view-online {
					height: 20px !important;
					padding-top: 13px !important;
				}				
				.one-column, .two-column, .three-column {
					width: 100% !important;
					display: block !important;
					margin: 0px auto !important;
				}
			    .login {
				    border-bottom: none !important;
				    height: 40px !important;
				    text-align: center !important;
			    }
				.header-text img {
					width: 80% !important;
					height: auto !important;
					margin: 0 auto !important;
					text-align: center !important;
				}
			    .image-scale img {
					width: 100% !important;
			        height: auto !important;
				}			    
				.mobile-width-100 	{ width: 100%; }
				.mobile-height-auto { height: auto !important; }
				.mobile-padding 	{ padding: 0px 0 30px 0 !important; }
				.mobile-no-padding 	{ padding: 0px !important; }
				.mobile-center 		{ text-align: center !important; margin: 0 auto; display: block; }
				.mobile-left 		{ text-align: left !important; }				
				.mobile-show 		{ display: inline-block !important; width: auto !important; height: auto !important; font-size: auto !important; }												
			    .mobile-hide 		{ display: none !important; width: 0px !important; height: 0px !important; line-height: 0px; }
			    .mobile-adjust-width{ width: 85% !important; margin: 0 auto !important; }
				.mobile-align-left 	{ text-align: left !important; }
				
				/* ---------------- Logos &amp; headers ---------------- */
				.logo {
					margin: 0 auto !important;
					text-align: center !important;
				}
				.logo img {
					display: inline-block !important;
					text-align: center !important;
					margin: 0 auto !important;
			    }
				/* ---------------- Text Styles ---------------- */
				.slogan {
					text-align: center !important;
				}
				.transactional-padding{
					padding: 30px 0 30px 0 !important;
				}
				.social-txt {
					text-align: center !important;
					padding: 0px !important;
				}
				/* ---------------- Backgrounds ---------------- */
				.adjust-background {
					background-image: url('https://img06.en25.com/EloquaImages/clients/TomTomTelematics/{048bc7c3-a7bb-4b06-8598-9717b766e220}_Welcome_Mobile_Outlook_640x275.png') !important;
					background-repeat: no-repeat !important;
					background-position: center center !important;
					background-size: cover !important;
					width: auto !important;
					height: 275px !important;
				}				
			}
		</style>
		<!--[if (gte mso 9)|(IE)]>
		<style type="text/css">
			table {border-collapse: collapse;}
			body, table, th, td, a, p {font-family: Verdana, Arial, sans-serif}
			.h1-txt a, .h2-txt a, .h3-txt a, .h4-txt a { color: #455560; }
		</style>
		<![endif]-->
	    <!--[if gte mso 9]><xml>
		 <o:OfficeDocumentSettings>
		  <o:AllowPNG/>
		  <o:PixelsPerInch>96</o:PixelsPerInch>
		 </o:OfficeDocumentSettings>
		</xml><![endif]-->		
	</head>
	<body style="font-family: Verdana, sans-serif;font-weight: normal;color: #455560;font-size: 12px;line-height: normal;height: 100% !important;width: 100% !important;margin: 0 !important;padding: 0 !important;">
<!-- 0.1 HIDDEN PREHEADER  -->
<div style="display:none; font-size:1px; color:#ffffff; font-family: Arial, sans-serif; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden;">Important - review your Webfleet contract confirmation</div>
<!-- /0.1 HIDDEN PREHEADER  --><!-- 1.2  PREHEADER - VIEW ONLINE + SLOGAN -->

<table align="center" bgcolor="#e4e4e5" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
	<tbody>
		<tr>
			<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
			<table align="center" class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" valign="middle" width="640">
				<tbody>
					<tr>
						<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="middle">
						<table class="padding-container" height="40" style="height: 40px; line-height: auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" valign="middle" width="100%">
							<tbody>
								<tr>
									<th align="left" class="mobile-hide" height="40" style="padding: 0;font-weight: normal;" width="320"><span class="mobile-hide small-txt" style="font-size: 10px;font-weight: normal;">Let’s drive business. Further.</span></th>
									<th class="stack mobile-center view-online" height="40" style="text-align: right;padding: 0;font-weight: normal;" width="320">&nbsp;</th>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<!-- /1.2  PREHEADER - VIEW ONLINE + SLOGAN --><!-- 2.5 HEADER - TTWEB LOGO LEFT -->

<table align="center" bgcolor="#ffffff" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
	<tbody>
		<tr>
			<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
			<table align="center" class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640">
				<tbody>
					<tr>
						<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						<table align="center" class="padding-container" height="90" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
							<tbody>
								<tr>
									<td align="left" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="middle" class=""><a href="https://webfleet.com/?elqTrackId=ee2599e53e3744c5bf20a0ca2740cab5" style="color: #BDD731;text-decoration: none;border: none;" target="_blank" title="Webfleet Solutions"><img alt="WEBFLEET logo" class="mobile-center" height="90" src="https://media.webfleet.com/email/elq/WFS_logo_for_Eloqua_300x180.png" style="-ms-interpolation-mode: bicubic;border: 0px;outline: none;text-decoration: none;display: block;" width="150"> </a></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<!-- /2.5 HEADER - TTWEB LOGO LEFT --><!-- 3.2.1 CLICKABLE HEADER IMAGE -->

<table border="0" cellpadding="0" cellspacing="0" height="275" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
	<tbody>
		<tr><!--[if !mso]><!-- -->
			<td align="center" background="https://e.webfleet.com/l/844203/2020-12-17/2gdj43/844203/1608218595puDQEqWz/EN_Welcome_Normal_1920x275.png" bgcolor="#FFFFFF" class="adjust-background" style="background-size: cover;background-position: center center;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="top"><a href="https://help.webfleet.com/en_gb/get-started/?elqTrackId=47358b81876c44949c2749e0c7162ec2" style="text-decoration: none; border: none; width: 640px; width: 100%; height: 275px; background-color: transparent; display: inline-block;padding:0;" target="_blank"><!--<![endif]--> <!--[if gte mso 9]>						
					<td width="640" bgcolor="#FFFFFF" valign="top" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" align="center">				
						<a href="https://help.webfleet.com/en_gb/get-started/" target="_blank" style="text-decoration: none; border: none;padding:0;">					
							<img src="https://e.webfleet.com/l/844203/2021-02-05/2w12wb/844203/1612520788GDynd1pQ/EN_correct_Welcome_Mobile_Outlook_640x275.png" alt="Thank you for your additional order">
						</a>
					</td>					
					<![endif]--> <!--[if !mso]><!-- --> </a></td>
			<!--<![endif]-->
		</tr>
	</tbody>
</table>
<!-- /3.2.1 CLICKABLE HEADER IMAGE --><!-- 4.1 DATE BAR --><!--[if !mso]><!-- -->

<table align="center" bgcolor="#eeeeef" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
	<tbody>
		<tr>
			<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
			<table align="center" class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640"><!--<![endif]--><!--[if gte mso 9]>    		
    	<table width="640" class="container" align="center" bgcolor="#eeeeef" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;">
	    	<tr>
				<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
					<table width="580" class="container" align="center" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;">
		
		<![endif]-->
				<tbody>
					<tr>
						<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						<table align="center" class="padding-container" height="96" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
							<tbody>
								<tr>
									<td align="center" class="h2-txt no-padding" height="96" style="height: 96px; border-collapse: collapse; padding: 0px; font-weight: 600; font-size: 20px; line-height: 24px; color: rgb(69, 85, 96); font-family: Verdana, sans-serif; text-decoration: none; border-style: none;" valign="middle">Welcome to Webfleet Solutions!&nbsp;</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<!--[if gte mso 9]>    	
			    	</table>
				</tr>
			</td>			
		</table>			    				    	
		<![endif]--><!--[if !mso]><!-- -->
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<!--<![endif]--><!-- /4.1 DATE BAR --><!-- /SPACING -->

<table bgcolor="#ffffff" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
	<tbody>
		<tr>
			<td height="40" style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="top" class="">&nbsp;</td>
		</tr>
	</tbody>
</table>
<!-- /SPACING --><!-- 5.1 CONTENT - ONE COLUM -->

<table align="center" bgcolor="#ffffff" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
	<tbody>
		<tr>
			<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
			<table align="center" class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640">
				<tbody>
					<tr>
						<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						<table align="center" class="padding-container" height="96" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" valign="top" width="100%"><!-- NORMAL TEXT -->
							<tbody>
								<tr>
									<td class="normal-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;color: #455560;font-size: 12px;line-height: 18px;">&nbsp;<br>
									Dear customer,<br>
									<br>
									Thank you for choosing our solution!&nbsp;<br>
									Your WEBFLEET contract has been registered.&nbsp;<strong>Please carefully review the attached documents</strong>, which include details on invoicing and payment terms:

									<ul>
										<li>Contract Confirmation</li>
										<li>Terms and Conditions</li>
									</ul>
									If you notice any errors, please report them to: <a href="mailto:sales.uk@webfleet.com" target="_blank" title="Contact Us">sales.uk@webfleet.com</a>.<br>
									&nbsp;</td>
								</tr>
								<tr>
									<td class="normal-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;color: #455560;font-size: 12px;line-height: 18px;"><strong>Activation &amp; Installation</strong>
									<ul>
										<li>If your device requires fixed installation, you will be contacted shortly by our partner.</li>
										<li>If not, <strong>please activate and install your device(s) </strong>by following the instructions in the attached Installation Guide or by downloading it from the <a href="https://portals.webfleet.com/s/article/Product-essentials-overview?language=en_US" title="Customer Cummunity">Product Essentials - Overview</a>. For the <u>activation</u>, you will need your customer number (included in contract confirmation), <a href="https://portals.webfleet.com/s/article/Where-to-find-the-WEBFLEET-activation-code?language=en_GB&amp;elqTrackId=afcbbf497b2945c2bcde392eec761542#contract" title="Customer Cummunity">activation code</a> and the device serial number.&nbsp;</li>
									</ul>
									We look forward to helping you on your journey to driving excellence!”<br>
									<br>
									Best Regards,<br>
									<br>
									Your Webfleet Solutions Team</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
	<!-- /5.1 CONTENT - ONE COLUM --><!-- BORDER AND SPACING -->
</table>

<table bgcolor="#ffffff" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
	<tbody>
		<tr>
			<td height="30" style="height: 30px;line-height: 30px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="top" class="">&nbsp;</td>
		</tr>
	</tbody>
</table>
<!-- /BORDER AND SPACING --><!-- 6.1 CONTENT - CTA GRAY BAR --><!--[if !mso]><!-- -->

<table align="center" bgcolor="#dededf" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
	<tbody>
		<tr>
			<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
			<table align="center" class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640"><!--<![endif]--><!--[if gte mso 9]>    		
    	<table width="640" class="container" align="center" bgcolor="#dededf" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;">
	    	<tr>
				<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
					<table width="580" class="container" align="center" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;">
		
		<![endif]-->
				<tbody>
					<tr>
						<td height="40" style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="top" class="">&nbsp;</td>
					</tr>
					<tr>
						<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
							<tbody>
								<tr>
									<th class="two-column" style="padding: 0;font-weight: 400;text-align: left;" valign="top" width="470">
									<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
										<tbody>
											<tr>
												<td class="h4-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: 600;font-size: 14px;line-height: 18px;color: #455560;font-family: Verdana, sans-serif;padding-bottom: 20px;text-decoration: none;">Any support needed?</td>
											</tr>
											<tr>
												<td class="normal-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;color: #455560;font-size: 12px;line-height: 18px;">We are here to help!<br>
												For any questions, please don't hesitate to contact your reseller or our Customer Support team.</td>
											</tr>
										</tbody>
									</table>
									</th>
									<th class="two-column" style="padding: 0;font-weight: 400;text-align: left;" valign="top" width="170">
									<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%"><!-- CTA BUTTON GREY -->
										<tbody>
											<tr>
												<td align="right" class="mobile-align-left" style="padding: 20px 0 0 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;font-weight: normal;">
												<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;">
													<tbody>
														<tr>
															<td bgcolor="#455560" class="button-black-bg" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;height: 38px;line-height: 38px;-moz-border-radius: 30px;-webkit-border-radius: 30px;border-radius: 30px;background-color: #455560;"><a class="button-black" href="https://help.webfleet.com/en_gb/contact/?elqTrackId=91a90d1d6f524f3a8691f0f69fb3b259" style="color: #ffffff;text-decoration: none;border: 1px solid #455560;padding: 13px 12px 18px 15px;height: 8px;line-height: 7px;text-align: center;font-weight: normal;font-size: 12px;display: block;-moz-border-radius: 30px;-webkit-border-radius: 30px;border-radius: 30px;background-color: #455560;" target="_blank" title="Contact Us">Contact Us <img height="10" src="https://media.webfleet.com/email/elq/icon-cta-white.png" style="line-height:20px; display:inline-block" width="14"></a></td>
														</tr>
													</tbody>
												</table>
												</td>
											</tr>
											<!-- /CTA BUTTON GREY -->
										</tbody>
									</table>
									</th>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td height="40" style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="top" class="">&nbsp;</td>
					</tr>
					<!--[if gte mso 9]>    	
			    	</table>
				</tr>
			</td>			
		</table>			    				    	
		<![endif]--><!--[if !mso]><!-- -->
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<!--<![endif]--><!-- /6.1 CONTENT - CTA GRAY BAR --><!-- ADD FOOTER IN CORRECT LANGUAGE --><!-- 17.1 FOOTER SMALL -->

<table align="center" bgcolor="#455560" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
	<tbody>
		<tr>
			<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
			<table align="center" class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640">
				<tbody>
					<tr>
						<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
							<tbody>
								<tr>
									<td height="40" style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" class="">&nbsp;</td>
								</tr>
							</tbody>
						</table>

						<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
							<tbody>
								<tr>
									<th class="two-column" style="padding: 0;font-weight: 400;text-align: left;" valign="top" width="210">
									<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%"><!-- CONTENT -->
										<tbody>
											<tr>
												<td align="left" class="footer-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;font-size: 12px;line-height: 18px;color: #ffffff !important;" valign="top" width="60"><strong style="font-weight: bold;">Follow</strong></td>
												<td align="center" class="footer-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;font-size: 12px;line-height: 18px;color: #ffffff !important;" valign="middle" width="50"><a href="https://twitter.com/WebfleetNews" style="color: #a3bd31;text-decoration: none;border: none;" target="_blank" title="Follow us on Twitter"><img alt="Twitter" height="23" src="https://media.webfleet.com/email/elq/icon-twitter.png" style="-ms-interpolation-mode: bicubic;border: 0px;outline: none;text-decoration: none;display: block;" width="28"> </a></td>
												<td align="center" class="footer-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;font-size: 12px;line-height: 18px;color: #ffffff !important;" valign="middle" width="50"><a href="https://www.youtube.com/channel/UCeahLAfuyucUVeoZBjGWm3A/" style="color: #a3bd31;text-decoration: none;border: none;" target="_blank" title="Follow us on YouTube"><img align="middle" alt="YouTube" height="23" src="https://media.webfleet.com/email/elq/icon-youtube.png" style="-ms-interpolation-mode: bicubic;border: 0px;outline: none;text-decoration: none;display: block;" width="28"> </a></td>
												<td align="center" class="footer-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;font-size: 12px;line-height: 18px;color: #ffffff !important;" valign="middle" width="50"><a href="https://www.linkedin.com/company/webfleet-solutions" style="color: #a3bd31;text-decoration: none;border: none;" target="_blank" title="Follow us on LinkedIn"><img alt="LinkedIn" height="23" src="https://media.webfleet.com/email/elq/icon-linkedin.png" style="-ms-interpolation-mode: bicubic;border: 0px;outline: none;text-decoration: none;display: block;" width="28"> </a></td>
											</tr>
											<!-- /CONTENT -->
										</tbody>
									</table>
									</th>
									<th class="stack" style="line-height: 40px;height: 40px;padding: 0;font-weight: normal;" valign="top" width="40">&nbsp;</th>
									<th class="two-column footer" style="padding: 0;font-weight: 400;text-align: left;color: #ffffff !important;" valign="top" width="390">
									<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%"><!-- CONTENT -->
										<tbody>
											<tr>
											</tr>
											<!-- /CONTENT -->
										</tbody>
									</table>
									</th>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table align="center" class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
				<tbody>
					<tr>
						<td align="center" class="border" height="40" style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="middle"><img height="2" src="https://media.webfleet.com/email/elq/border-footer.png" style="-ms-interpolation-mode: bicubic;border: 0px;outline: none;text-decoration: none;display: block;" width="720"></td>
					</tr>
				</tbody>
			</table>

			<table align="center" class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640">
				<tbody>
					<tr>
						<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" valign="bottom" width="100%">
							<tbody>
								<tr>
									<th class="one-column footer" style="padding: 0;font-weight: 400;text-align: left;color: #ffffff !important;" valign="middle" width="640">
									<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%"><!-- CONTENT -->
										<tbody>
											<tr>
												<td class="footer-txt" height="50" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;font-size: 12px;line-height: 18px;color: #ffffff !important;" valign="bottom"><span class="mobile-hide"><a class="footer-link" href="https://www.webfleet.com/webfleet/legal/privacy/" style="color: #ffffff;text-decoration: none;border: none;" target="_blank" title="Privacy Policy">Privacy policy</a> <span class="divider" style="width: 25px;text-align: center;display: inline-block;color: #616e78;">|</span> <a class="footer-link" href="https://www.webfleet.com/webfleet/blog/" style="color: #ffffff;text-decoration: none;border: none;" target="_blank" title="Blog">Blog</a> <span class="divider" style="width: 25px;text-align: center;display: inline-block;color: #616e78;">|</span> <a class="footer-link" href="https://www.webfleet.com/webfleet/support/" style="color: #ffffff;text-decoration: none;border: none;" target="_blank" title="Support">Support</a></span><br>
												<span class="mobile-center" style="display: inline-block">Copyright © 2006 - 2021 Webfleet Solutions Sales B.V. All rights reserved.</span></td>
											</tr>
											<!-- /CONTENT -->
										</tbody>
									</table>
									</th>
								</tr>
							</tbody>
						</table>

						<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
							<tbody>
								<tr>
									<td height="40" style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" class="">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<!-- /17.1 FOOTER SMALL -->
</body></html>