<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
	    <title>Welkom bij Webfleet Solutions!</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!--[if !mso]><!-->
	    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <!--<![endif]-->
		<link rel="apple-touch-icon" sizes="180x180" href="https://media.webfleet.com/branding/wfs/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="https://media.webfleet.com/branding/wfs/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="194x194" href="https://media.webfleet.com/branding/wfs/favicon-194x194.png">
		<link rel="icon" type="image/png" sizes="192x192" href="https://media.webfleet.com/branding/wfs/android-chrome-192x192.png">
		<link rel="icon" type="image/png" sizes="16x16" href="https://media.webfleet.com/branding/wfs/favicon-16x16.png">
		<link rel="mask-icon" href="https://media.webfleet.com/branding/wfs/safari-pinned-tab.svg" color="#ff0000">
	    <style type="text/css">
		    /* ---------------------------- EMAIL CLIENT RESETS --------------------------------- */
		    #outlook a{
			    padding:0;
			}
	        .ReadMsgBody{
		        width:100%;
		    } .ExternalClass{
			    width:100%;
			}
	        .ExternalClass, .ExternalClass span, .ExternalClass td, .ExternalClass div {
		        line-height: 100%;
		    }
			table, td{
				mso-table-lspace:0pt;
				mso-table-rspace:0pt;
				border-collapse: collapse;
			}
			body {
				height: 100% !important;
				width: 100% !important;
			    margin: 0 !important;
			    padding: 0 !important;
			}
			img {
				-ms-interpolation-mode: bicubic;
				border: 0px;
				outline: none;
				text-decoration: none;
				display: block;
			}
			table {
			    border-spacing: 0;
			}
			td, th {
			    padding: 0;
			    font-weight: normal;
			}
			div[style*="margin: 16px 0"] { 
				margin:0 !important;
			}
			p {
				margin-bottom: 10px;
			}
		</style>
	    <style type="text/css">
		    /* ---------------------------- TOMTOM STYLES --------------------------------- */
		    /* ---------------- Body ---------------- */
		    body {
			    font-family: Verdana, sans-serif;
			    font-weight: normal;
			    color: #455560;
			    font-size: 12px;
			    line-height: normal;
		    }
		    a {
			    color: #a3bd31;
			    text-decoration: none;
			    border: none;
			}
			a:hover {
			    color: #a3bd31 !important;
			    text-decoration: underline !important;
			    border: none !important;
			}		    
		    /* ---------------- Layouts ---------------- */		    
		    container {
			    margin: 0 auto;
		    }
		    .one-column, .two-column, .three-column, .four-column {
				text-align: left;
				font-weight: 400;
			}		    
		    .login {
			    height: 85px;
			    border-bottom: 5px solid #BDD731;
		    }
		    .login a {
			    color: #455560;
			    font-weight: bold;
		    }
		    .login a:hover {
			    color: #455560 !important;
			}
			.divider {
				width: 25px;
				text-align: center;
				display: inline-block;
				color: #616e78;
			}
		    /* ---------------- Text Styles ---------------- */
		    strong {
			    font-weight: bold;
		    }
		    i { 
			    font-style: italic;
		    }
		    .view-online {
			    font-size: 10px;
			    font-family: Verdana, sans-serif;
			    font-weight: normal;			    
			    color: #455560;
			    text-decoration: underline;
		    }
		    .view-online:hover {
			    color: #455560 !important;			    
			    text-decoration: none !important;
		    }
		    .h1-txt {
			    font-size: 28px; 
			    line-height: 32px;
			    color: #455560 !important;			    
			    font-family: Verdana, sans-serif;
			    padding-bottom: 20px;
			    font-weight: 600;
		    }
		    .h2-txt {
			    font-size: 20px; 
			    line-height: 24px;
			    color: #455560;			    
			    font-family: Verdana, sans-serif;
			    padding-bottom: 20px;
			    font-weight: 600;
			    text-decoration: none;
		    }
		    .h3-txt {
			    font-size: 16px; 
			    line-height: 20px;
			    color: #455560;			    
			    font-family: Verdana, sans-serif;
			    padding-bottom: 20px;
			    font-weight: 600;
			    text-decoration: none;
		    }
		    .h4-txt {
			    font-size: 14px; 
			    line-height: 18px;
			    color: #455560;			    
			    font-family: Verdana, sans-serif;
			    padding-bottom: 20px;
			    font-weight: 600;
			    text-decoration: none;
		    }		    
		    .h1-txt a, .h2-txt a, .h3-txt a, .h4-txt a {
			    color: #455560;			    
			    text-decoration: none;
		    }
		    .h1-txt a:hover, .h2-txt a:hover, .h3-txt a:hover, .h4-txt a:hover {
			    color: #455560 !important;			    
			    text-decoration: none !important;
		    }		    
		    .normal-weight {
			    font-weight: normal !important;
		    }
		    .normal-txt {
			    font-family: Verdana, sans-serif;
			    font-weight: normal;
			    color: #455560;
			    font-size: 12px;
			    line-height: 18px;
		    }
		    .small-txt {
			    font-size: 10px;
			    font-weight: normal;
		    }		    
		    .footer-txt {
			    font-family: Verdana, sans-serif;
			    font-weight: normal;
			    color: #ffffff !important;
			    font-size: 12px;
			    line-height: 18px;
		    }
		    .footer-menu {
			    font-family: Verdana, sans-serif;
			    font-weight: normal;
			    color: #ffffff;
			    font-size: 10px;
			    line-height: 16px;
		    }		    		    
		    .footer-menu a {		    
			    font-family: Verdana, sans-serif;
			    font-weight: normal;
			    color: #ffffff;
			    font-size: 10px;
			    line-height: 16px;			    
			}
		    .footer-menu a:hover {		    
			    color: #ffffff !important;	    
			    text-decoration: underline !important;
			}			
			/*ul, ol {
				margin: 0;
			}		*/    
			.mobile-show {
				display: none;
				width: 0;
				height: 0;
				font-size: 0;
			}
			.no-padding {
				padding-bottom: 0; 
			}
		    /* ---------------- CTA's ---------------- */
		    .text-cta {
			    color: #455560;
			    text-decoration: none;
			    font-weight: normal;
		    }
		    text-cta:hover {
			    color: #000000 !important;
		    }
		    .footer {
			    color: #ffffff !important;
		    }
		    .footer-link {
			    color: #ffffff;
			    text-decoration: none;
			    border: none;
			}
			.footer-link:hover {
			    color: #ffffff !important;
			    text-decoration: underline !important;
			    border: none !important;
			}
			/* ---------------- Buttons ---------------- */
			.button-green-bg, 
			.button-black-bg, 
			.button-grey-bg, 
			.button-white-bg { height: 38px; line-height: 38px; -moz-border-radius: 30px; -webkit-border-radius: 30px; border-radius: 30px; }
			
			.button-green, 
			.button-black, 
			.button-grey,
			.button-white { padding: 15px; height: 8px; line-height: 7px; text-decoration: none; text-align: center; font-weight: normal; font-size: 12px; display: block; -moz-border-radius: 30px; -webkit-border-radius: 30px; border-radius: 30px;  }
			
			.button-green:hover,
			.button-black:hover,
			.button-grey:hover,
			.button-white:hover { text-decoration: none !important; }			
			
			.button-green span, 
			.button-black span, 
			.button-grey span,
			.button-white span { font-size: 14px; font-weight: bold !important; }			
			
			.button-green-bg 	{ background-color: #BDD731; } /* add this to TD bgcolor="#BDD731" */
			.button-green 		{ background-color: #BDD731; border: 1px solid #BDD731; color: #455560; }
			.button-green:hover { background-color: #D1E952 !important; border: 1px solid #94AA21 !important; color: #455560 !important; }
			.button-green span	{ color: #455560; }
			
			.button-black-bg 	{ background-color: #455560; } /* add this to TD bgcolor="#455560" */
			.button-black 		{ background-color: #455560; border: 1px solid #455560; color: #ffffff; }
			.button-black:hover { background-color: #303b42 !important; border: 1px solid #455560 !important; color: #ffffff !important; }
			.button-green span	{ color: #ffffff; }			

			.button-grey-bg 	{ background-color: #dededf; } /* add this to TD bgcolor="#dededf" */
			.button-grey 		{ background-color: #dededf; border: 1px solid #dededf; color: #455560; }
			.button-grey:hover 	{ background-color: #d2d4d5 !important; border: 1px solid #dededf !important; color: #455560 !important; }
			.button-green span	{ color: #455560; }			

			.button-white-bg 	{ background-color: #ffffff; } /* add this to TD bgcolor="#ffffff" */
			.button-white 		{ background-color: #ffffff; border: 1px solid #d2d4d5; color: #455560; }
			.button-white:hover { background-color: #cccccc !important; border: 1px solid #b1b6b9 !important; color: #455560 !important; }				
			.button-green span	{ color: #455560; }
			
			.button-link { color: #455560; }
			.button-link:hover { text-decoration: none !important; }			
			.button-link span { color: #BDD731 }
	    </style>
	    
	    <style type="text/css">
		    /* ---------------------------- MEDIA QUERIES --------------------------------- */
		    @media screen and (max-width: 649px) {
				/* ---------------- Layouts ---------------- */
				.container {
					width: 100% !important;
					margin: 0 auto !important;
				}
				.padding-container {
					width: 85% !important;
					margin: 0 auto !important;
				}
				.border img {
				    display: block !important;
				    width: 100% !important;					
				}
				.stack {
				    display: block !important;
				    width: 100% !important;
				}
				.view-online {
					height: 20px !important;
					padding-top: 13px !important;
				}				
				.one-column, .two-column, .three-column {
					width: 100% !important;
					display: block !important;
					margin: 0px auto !important;
				}
			    .login {
				    border-bottom: none !important;
				    height: 40px !important;
				    text-align: center !important;
			    }
				.header-text img {
					width: 80% !important;
					height: auto !important;
					margin: 0 auto !important;
					text-align: center !important;
				}
			    .image-scale img {
					width: 100% !important;
			        height: auto !important;
				}			    
				.mobile-width-100 	{ width: 100%; }
				.mobile-height-auto { height: auto !important; }
				.mobile-padding 	{ padding: 0px 0 30px 0 !important; }
				.mobile-no-padding 	{ padding: 0px !important; }
				.mobile-center 		{ text-align: center !important; margin: 0 auto; display: block; }
				.mobile-left 		{ text-align: left !important; }				
				.mobile-show 		{ display: inline-block !important; width: auto !important; height: auto !important; font-size: auto !important; }												
			    .mobile-hide 		{ display: none !important; width: 0px !important; height: 0px !important; line-height: 0px; }
			    .mobile-adjust-width{ width: 85% !important; margin: 0 auto !important; }
				.mobile-align-left 	{ text-align: left !important; }
				
				/* ---------------- Logos &amp; headers ---------------- */
				.logo {
					margin: 0 auto !important;
					text-align: center !important;
				}
				.logo img {
					display: inline-block !important;
					text-align: center !important;
					margin: 0 auto !important;
			    }
				/* ---------------- Text Styles ---------------- */
				.slogan {
					text-align: center !important;
				}
				.transactional-padding{
					padding: 30px 0 30px 0 !important;
				}
				.social-txt {
					text-align: center !important;
					padding: 0px !important;
				}
				/* ---------------- Backgrounds ---------------- */
				.adjust-background {
					background-image: url('https://img06.en25.com/EloquaImages/clients/TomTomTelematics/{9544605b-b259-4863-908a-c346f828a1c9}_NL_Welcome_Mobile_Outlook_640x275.png') !important;
					background-repeat: no-repeat !important;
					background-position: center center !important;
					background-size: cover !important;
					width: auto !important;
					height: 275px !important;
				}				
			}
		</style>
		<!--[if (gte mso 9)|(IE)]>
		<style type="text/css">
			table {border-collapse: collapse;}
			body, table, th, td, a, p {font-family: Verdana, Arial, sans-serif}
			.h1-txt a, .h2-txt a, .h3-txt a, .h4-txt a { color: #455560; }
		</style>
		<![endif]-->
	    <!--[if gte mso 9]><xml>
		 <o:OfficeDocumentSettings>
		  <o:AllowPNG/>
		  <o:PixelsPerInch>96</o:PixelsPerInch>
		 </o:OfficeDocumentSettings>
		</xml><![endif]-->		
	</head>
	<body style="font-family: Verdana, sans-serif;font-weight: normal;color: #455560;font-size: 12px;line-height: normal;height: 100% !important;width: 100% !important;margin: 0 !important;padding: 0 !important;">
		<!-- 0.1 HIDDEN PREHEADER  -->
		<div style="display:none; font-size:1px; color:#ffffff; font-family: Arial, sans-serif; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden;">
			Belangrijk - uw contractbevestiging    
		</div>
		<!-- /0.1 HIDDEN PREHEADER  -->
        
        <!-- 1.2  PREHEADER - VIEW ONLINE + SLOGAN -->
		<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%" bgcolor="#e4e4e5" align="center">
			<tbody><tr>
		    	<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
			    	<table class="container" valign="middle" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640" align="center">
				    	<tbody><tr>
							<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="middle">
						    	<table class="padding-container" valign="middle" style="height: 40px; line-height: auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" height="40" width="100%">
							    	<tbody><tr>
										<th class="mobile-hide" style="padding: 0;font-weight: normal;" height="40" width="320" align="left">
											<span class="mobile-hide small-txt" style="font-size: 10px;font-weight: normal;">Let’s drive business. Further.</span>
										</th>
										<th class="stack mobile-center view-online" style="text-align: right;padding: 0;font-weight: normal;" height="40" width="320">
																									
										</th>																	
									</tr>
								</tbody></table>
							</td>					
						</tr>
					</tbody></table>
				</td>
			</tr>
		</tbody></table>
        <!-- /1.2  PREHEADER - VIEW ONLINE + SLOGAN -->
                
       
                
        <!-- 2.5 HEADER - TTWEB LOGO LEFT -->
		<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%" bgcolor="#ffffff" align="center">
			<tbody><tr>
		    	<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
			    	<table class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640" align="center">
				    	<tbody><tr>
							<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						    	<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" height="90" width="100%" align="center">
							    	<tbody><tr>
										<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="middle" align="left">
											<a href="https://webfleet.com/?elqTrackId=ee2599e53e3744c5bf20a0ca2740cab5" title="Webfleet Solutions" target="_blank" style="color: #BDD731;text-decoration: none;border: none;"> <img src="https://img06.en25.com/EloquaImages/clients/TomTomTelematics/{99a81543-baf0-4a89-b7e9-c410a4577090}_WFS_logo_for_Eloqua_300x180.png" alt="WEBFLEET-logo" class="mobile-center" style="-ms-interpolation-mode: bicubic;border: 0px;outline: none;text-decoration: none;display: block;" height="90" width="150"> </a>
										</td>																
									</tr>
								</tbody></table>
							</td>					
						</tr>
					</tbody></table>
				</td>
			</tr>
		</tbody></table>
        <!-- /2.5 HEADER - TTWEB LOGO LEFT -->
        
        <!-- 3.2.1 CLICKABLE HEADER IMAGE -->
		<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" height="275" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr>
				<!--[if !mso]><!-- -->				
				<td style="background-size: cover;background-position: center center;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" class="adjust-background" valign="top" bgcolor="#FFFFFF" background="https://img06.en25.com/EloquaImages/clients/TomTomTelematics/{ef9a7417-7665-4771-9472-61b1ff498d6f}_NL_Welcome_Normal_1920x275.png" align="center">					
					<a href="https://help.webfleet.com/nl_nl/get-started/?elqTrackId=435096f46c5742689e33113304317133" target="_blank" style="text-decoration: none; border: none; width: 640px; width: 100%; height: 275px; background-color: transparent; display: inline-block;padding:0;">					
				<!--<![endif]-->			
					
					<!--[if gte mso 9]>						
					<td width="640" bgcolor="#FFFFFF" valign="top" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" align="center">				
						<a href="https://help.webfleet.com/nl_nl/get-started/" target="_blank" style="text-decoration: none; border: none;padding:0;">					
							<img src="https://img06.en25.com/EloquaImages/clients/TomTomTelematics/{9544605b-b259-4863-908a-c346f828a1c9}_NL_Welcome_Mobile_Outlook_640x275.png" alt="Start with WEBFLEET">
						</a>
					</td>					
					<![endif]-->											

				<!--[if !mso]><!-- -->				
					</a>				
				</td>
				<!--<![endif]-->							
			</tr>
		</tbody></table>			        	
		<!-- /3.2.1 CLICKABLE HEADER IMAGE -->
        
    	<!-- 4.1 DATE BAR -->
    	
        <!--[if !mso]><!-- --> 
		<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" class="" width="100%" bgcolor="#eeeeef" align="center">
			<tbody><tr>
		    	<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" class="">
			    	<table class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640" align="center">
		<!--<![endif]-->			    	
		<!--[if gte mso 9]>    		
    	<table width="640" class="container" align="center" bgcolor="#eeeeef" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;">
	    	<tr>
				<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
					<table width="580" class="container" align="center" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;">
		
		<![endif]-->	        
						<tbody><tr>
							<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						    	<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" height="96" width="100%" align="center">
							    	<tbody><tr>
								    	<td class="h2-txt no-padding" style="height: 96px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: 600;font-size: 20px;line-height: 24px;color: #455560;font-family: Verdana, sans-serif;padding-bottom: 0;text-decoration: none;" height="96" valign="middle" align="center">
								    		Welkom op de gezamenlijke weg naar fantastische rijprestaties
								    	</td>
							    	</tr>
						    	</tbody></table>
							</td>
				    	</tr>
		<!--[if gte mso 9]>    	
			    	</table>
				</tr>
			</td>			
		</table>			    				    	
		<![endif]-->					
			            				    	
        <!--[if !mso]><!-- -->        
			    	</tbody></table>        
		    	</td>
			</tr>
		</tbody></table>
		<!--<![endif]-->
		
		<!-- /4.1 DATE BAR -->
      
        <!-- /SPACING -->
        <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%" bgcolor="#ffffff">
            <tbody><tr>
	            <td style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" class="" height="40" valign="top">
		             
	            </td>
            </tr>
        </tbody></table>
        <!-- /SPACING -->      
      
        <!-- 5.1 CONTENT - ONE COLUM -->
		<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%" bgcolor="#ffffff" align="center">
			<tbody><tr>
		    	<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" class="">
			    	<table class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640" align="center">
				    	<tbody><tr>
							<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						    	<table class="padding-container" valign="top" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" height="96" width="100%" align="center">							    	
																    	
									<!-- NORMAL TEXT -->							    	
							    	<tr>
								    	<td class="normal-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;color: #455560;font-size: 12px;line-height: 18px;">
									    	
Geachte klant,<br>
                                          

<br>Welkom bij Webfleet Solutions! Het doet ons genoegen dat u hebt besloten onze oplossing te gaan gebruiken. <br>
                                          <br>Uw WEBFLEET-contract is geregistreerd. Lees de volgende bijgesloten documenten met details over facturering en betalingsvoorwaarden zorgvuldig door: <br>


							    	<!-- BULLET LIST -->
							    	</td></tr><tr>
								    	<td class="normal-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;color: #455560;font-size: 12px;line-height: 18px;">
											
									    	<ul>
												<li>Contractbevestiging</li>
												<li>Algemene voorwaarden</li>
                                             			    	</ul>
								    	
							    	<!-- /BULLET LIST -->
                                          Rapporteer eventuele fouten aan <a href="mailto:sales.nl@webfleet.com" title="Contact opnemen" target="_blank" style="color: #a3bd31;text-decoration: none;border: none;">sales.nl@webfleet.com</a><br>

<br>Als uw systeem moet worden geinstalleerd, neemt onze (installatie-)partner binnenkort contact met u op.<br>
                                          <br>Indien dit niet nodig is, kunt u uw systeem/systemen activeren aan de hand van de instructies in de bijgesloten Activeringshandleiding. U kunt het ook <a href="https://portals.webfleet.com/s/article/Product-essentials-overview?languagelanguage=nl_NL" target="_blank" style="color: #a3bd31;text-decoration: none;border: none;"> hier</a> downloaden.<br>
                                          <p>We helpen u graag op weg naar fantastische rijprestaties!  </p>
                                          
                                          
<br>Met vriendelijke groeten,<br>
                                          
<br>Het Webfleet Solutions-team

									<!-- /NORMAL TEXT -->							    	
																					
						    
							
				    	
			    	</td></tr></table></td></tr></tbody>
		    	
			
		</table></td></tr></tbody>
        <!-- /5.1 CONTENT - ONE COLUM -->      
      
        <!-- BORDER AND SPACING -->
        <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%" bgcolor="#ffffff">
            <tbody><tr>
	            <td style="height: 30px;line-height: 30px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" class="" height="30" valign="top">
		             
	            </td>
            </tr>
        </tbody></table>
        <!-- /BORDER AND SPACING -->  
                   <!-- 6.1 CONTENT - CTA GRAY BAR -->
        <!--[if !mso]><!-- -->        
		<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%" bgcolor="#dededf" align="center">
			<tbody><tr>
		    	<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
			    	<table class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640" align="center">
		<!--<![endif]-->
		<!--[if gte mso 9]>    		
    	<table width="640" class="container" align="center" bgcolor="#dededf" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;">
	    	<tr>
				<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
					<table width="580" class="container" align="center" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;">
		
		<![endif]-->        
						<tbody><tr>
				            <td style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" height="40" valign="top">
					             
				            </td>
			            </tr>
				    	<tr>
							<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						    	<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">						    	
							    	<tbody><tr>
										<th class="two-column" style="padding: 0;font-weight: 400;text-align: left;" width="470" valign="top">
									    	<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
										    	<tbody><tr>
											    	<td class="h4-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: 600;font-size: 14px;line-height: 18px;color: #455560;font-family: Verdana, sans-serif;padding-bottom: 20px;text-decoration: none;">
												    	Hulp nodig?
											    	</td>
										    	</tr>										    	
										    	<tr>
											    	<td class="normal-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;color: #455560;font-size: 12px;line-height: 18px;">
												    	Wij staan voor u klaar!<br> Als u vragen hebt, kunt u altijd contact opnemen met uw contactpersoon of onze klantenservice.<br>
											    	</td>
										    	</tr>
									    	</tbody></table>
										</th>
										<th class="two-column" style="padding: 0;font-weight: 400;text-align: left;" width="170" valign="top">
									    	<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
										    	<!-- CTA BUTTON GREY -->
												<tbody><tr>
													<td class="mobile-align-left" style="padding: 20px 0 0 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;font-weight: normal;" align="right">
														<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;">
															<tbody><tr>
																<td class="button-black-bg" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;height: 38px;line-height: 38px;-moz-border-radius: 30px;-webkit-border-radius: 30px;border-radius: 30px;background-color: #455560;" bgcolor="#455560">
                                                                  <a href="https://help.webfleet.com/nl_nl/contact/?elqTrackId=d5f6cdecab994915b562783c35a0b207" title="Contact opnemen" target="_blank" class="button-black" style="color: #ffffff;text-decoration: none;border: 1px solid #455560;padding: 13px 12px 18px 15px;height: 8px;line-height: 7px;text-align: center;font-weight: normal;font-size: 12px;display: block;-moz-border-radius: 30px;-webkit-border-radius: 30px;border-radius: 30px;background-color: #455560;">Contact opnemen <img src="https://img06.en25.com/EloquaImages/clients/TomTomTelematics/{f994de8a-652e-47e0-8e4f-dd241db6ebdd}_icon-cta-white.png" style="line-height:20px; display:inline-block" height="10" width="14"></a>
																</td>
															</tr>
														</tbody></table>
													</td>
												</tr>
										    	<!-- /CTA BUTTON GREY -->
									    	</tbody></table>
										</th>										
							    	</tr>
						    	</tbody></table>
							</td>
				    	</tr>
						<tr>
				            <td style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" height="40" valign="top">
					             
				            </td>
			            </tr>				    	
		<!--[if gte mso 9]>    	
			    	</table>
				</tr>
			</td>			
		</table>			    				    	
		<![endif]-->					
			            				    	
        <!--[if !mso]><!-- -->        
			    	</tbody></table>        
		    	</td>
			</tr>
		</tbody></table>
		<!--<![endif]-->							    				
        <!-- /6.1 CONTENT - CTA GRAY BAR -->  
              
        <!-- ADD FOOTER IN CORRECT LANGUAGE -->
        
          <!-- 17.1 FOOTER SMALL -->
<table align="center" bgcolor="#455560" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
	<tbody>
		<tr>
			<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
			<table align="center" class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640">
				<tbody>
					<tr>
						<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
							<tbody>
								<tr>
									<td height="40" style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">&nbsp;</td>
								</tr>
							</tbody>
						</table>

						<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
							<tbody>
								<tr>
									<th class="two-column" style="padding: 0;font-weight: 400;text-align: left;" valign="top" width="210">
									<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%"><!-- CONTENT -->
										<tbody>
											<tr>
												<td align="left" class="footer-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;font-size: 12px;line-height: 18px;color: #ffffff !important;" valign="top" width="60"><strong style="font-weight: bold;">Volg ons</strong></td>
												<td align="center" class="footer-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;font-size: 12px;line-height: 18px;color: #ffffff !important;" valign="middle" width="50"><a href="https://twitter.com/WebfleetNews?elqTrackId=dfa96e1cedc34c909ba45e86051e7977" style="color: #a3bd31;text-decoration: none;border: none;" target="_blank" title="Volg ons op Twitter"><img alt="Twitter" height="23" src="https://img06.en25.com/EloquaImages/clients/TomTomTelematics/{25dfc0d5-7791-4789-a3c8-cff540c83a5f}_icon-twitter.png" style="-ms-interpolation-mode: bicubic;border: 0px;outline: none;text-decoration: none;display: block;" width="28"> </a></td>
												<td align="center" class="footer-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;font-size: 12px;line-height: 18px;color: #ffffff !important;" valign="middle" width="50"><a href="https://www.youtube.com/channel/UCeahLAfuyucUVeoZBjGWm3A/?elqTrackId=d8b963738adf413695d6eebef1b2ab29" style="color: #a3bd31;text-decoration: none;border: none;" target="_blank" title="Volg ons op YouTube"><img align="middle" alt="YouTube" height="23" src="https://img06.en25.com/EloquaImages/clients/TomTomTelematics/{f248e8b2-1c7f-4150-b55c-6b231a4a0326}_icon-youtube.png" style="-ms-interpolation-mode: bicubic;border: 0px;outline: none;text-decoration: none;display: block;" width="28"> </a></td>
												<td align="center" class="footer-txt" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;font-size: 12px;line-height: 18px;color: #ffffff !important;" valign="middle" width="50"><a href="https://www.linkedin.com/company/webfleet-solutions?elqTrackId=cdb064265ad846ceb47f2999637670cf" style="color: #a3bd31;text-decoration: none;border: none;" target="_blank" title="Volg ons op LinkedIn"><img alt="LinkedIn" height="23" src="https://img06.en25.com/EloquaImages/clients/TomTomTelematics/{0c7e1b99-6bfd-4c25-a2ae-b297001f42b4}_icon-linkedin.png" style="-ms-interpolation-mode: bicubic;border: 0px;outline: none;text-decoration: none;display: block;" width="28"> </a></td>
											</tr>
											<!-- /CONTENT -->
										</tbody>
									</table>
									</th>
									<th class="stack" style="line-height: 40px;height: 40px;padding: 0;font-weight: normal;" valign="top" width="40">&nbsp;</th>
									<th class="two-column footer" style="padding: 0;font-weight: 400;text-align: left;color: #ffffff !important;" valign="top" width="390">
									<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%"><!-- CONTENT -->
										<tbody>
											<tr>
												
											</tr>
											<!-- /CONTENT -->
										</tbody>
									</table>
									</th>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table align="center" class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
				<tbody>
					<tr>
						<td align="center" class="border" height="40" style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;" valign="middle"><img height="2" src="https://img06.en25.com/EloquaImages/clients/TomTomTelematics/{7e287967-034b-4623-840b-e342852325ee}_border-footer.png" style="-ms-interpolation-mode: bicubic;border: 0px;outline: none;text-decoration: none;display: block;" width="720"></td>
					</tr>
				</tbody>
			</table>

			<table align="center" class="container" style="margin: 0 auto;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="640">
				<tbody>
					<tr>
						<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">
						<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" valign="bottom" width="100%">
							<tbody>
								<tr>
									<th class="one-column footer" style="padding: 0;font-weight: 400;text-align: left;color: #ffffff !important;" valign="middle" width="640">
									<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%"><!-- CONTENT -->
										<tbody>
											<tr>
												<td class="footer-txt" height="50" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;font-family: Verdana, sans-serif;font-size: 12px;line-height: 18px;color: #ffffff !important;" valign="bottom"><span class="mobile-hide"><a class="footer-link" href="https://www.webfleet.com/webfleet/legal/privacy/?elqTrackId=785879c36b2547cfa786ebd9f6d60da4" style="color: #ffffff;text-decoration: none;border: none;" target="_blank" title="Privacybeleid">Privacybeleid</a> <span class="divider" style="width: 25px;text-align: center;display: inline-block;color: #616e78;">|</span> <a class="footer-link" href="https://webfleet.com/webfleet/blog/?elqTrackId=e1599e5a6590470884c9caa996ef8897" style="color: #ffffff;text-decoration: none;border: none;" target="_blank" title="Blog">Blog</a>
 <span class="divider" style="width: 25px;text-align: center;display: inline-block;color: #616e78;">|</span> <a class="footer-link" href="https://webfleet.com/webfleet/support/?elqTrackId=09c6cfc70c6246dd8ba1aed48c8cb783" style="color: #ffffff;text-decoration: none;border: none;" target="_blank" title="Ondersteuning">Ondersteuning</a></span><br>
												<span class="mobile-center" style="display: inline-block">Copyright &copy; 2006 - 2021 Webfleet Solutions Sales B.V. Alle rechten voorbehouden.</span></td>
											</tr>
											<!-- /CONTENT -->
										</tbody>
									</table>
									</th>
								</tr>
							</tbody>
						</table>

						<table class="padding-container" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border-spacing: 0;" width="100%">
							<tbody>
								<tr>
									<td height="40" style="height: 40px;line-height: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;padding: 0;font-weight: normal;">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<!-- /17.1 FOOTER SMALL -->
</table></body></html>