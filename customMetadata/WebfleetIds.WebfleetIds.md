<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Webfleet Ids</label>
    <protected>false</protected>
    <values>
        <field>CS1stLineQueueID__c</field>
        <value xsi:type="xsd:string">00G1O000005AVro</value>
    </values>
    <values>
        <field>CSMQueueID__c</field>
        <value xsi:type="xsd:string">00G1O000005AVs3</value>
    </values>
    <values>
        <field>CUSUTransactionSurveyId__c</field>
        <value xsi:type="xsd:string">0Kd1O000000GmauSAC</value>
    </values>
    <values>
        <field>CaseBusinessHoursId__c</field>
        <value xsi:type="xsd:string">01m30000000HDOJ</value>
    </values>
    <values>
        <field>Case__c</field>
        <value xsi:type="xsd:string">0121O000001FxHn</value>
    </values>
    <values>
        <field>CustomerPublicGroupId__c</field>
        <value xsi:type="xsd:string">00G1O000005AVyb</value>
    </values>
    <values>
        <field>FirstCaseMilestone__c</field>
        <value xsi:type="xsd:string">5571O00000000Sr</value>
    </values>
    <values>
        <field>FulfillmentQueueID__c</field>
        <value xsi:type="xsd:string">00G1O000005AVsN</value>
    </values>
    <values>
        <field>InstallationPartnerPublicGroupId__c</field>
        <value xsi:type="xsd:string">00G1O000005AVyc</value>
    </values>
    <values>
        <field>InstallationSurveyId__c</field>
        <value xsi:type="xsd:string">0Kd1O000000GmasSAC</value>
    </values>
    <values>
        <field>ResellerPublicGroupId__c</field>
        <value xsi:type="xsd:string">00G1O000005AVye</value>
    </values>
    <values>
        <field>SASUProActiveSupportQueueID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SASUTransactionSurveyID__c</field>
        <value xsi:type="xsd:string">0Kd1O000000GmatSAC</value>
    </values>
    <values>
        <field>SpamQueueId__c</field>
        <value xsi:type="xsd:string">00G1O000005AVsw</value>
    </values>
    <values>
        <field>SurveyCommunityID__c</field>
        <value xsi:type="xsd:string">0DB1O000000XZVy</value>
    </values>
    <values>
        <field>TaskCSMRecordType__c</field>
        <value xsi:type="xsd:string">0121O000001FxHo</value>
    </values>
    <values>
        <field>TransactionalSurveyId__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>WOFulfillmentQueueId__c</field>
        <value xsi:type="xsd:string">00G1O000005AVsP</value>
    </values>
    <values>
        <field>WOFulfillmentQueueName__c</field>
        <value xsi:type="xsd:string">FulfillmentWO</value>
    </values>
    <values>
        <field>WebfleetSLAId__c</field>
        <value xsi:type="xsd:string">5501O0000017SlnQAE</value>
    </values>
</CustomMetadata>
