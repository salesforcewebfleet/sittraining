<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>webfleet</label>
    <protected>false</protected>
    <values>
        <field>AccessTokenUrl__c</field>
        <value xsi:type="xsd:string">https://auth.webfleet.com/uaa/oauth/token</value>
    </values>
    <values>
        <field>AuthorizeURL__c</field>
        <value xsi:type="xsd:string">https://auth.webfleet.com/uaa/oauth/authorize</value>
    </values>
    <values>
        <field>CallbackURL__c</field>
        <value xsi:type="xsd:string">https://portals.webfleet.com/services/authcallback/webfleet</value>
    </values>
    <values>
        <field>ClientId__c</field>
        <value xsi:type="xsd:string">salesforce-sso</value>
    </values>
    <values>
        <field>ClientSecret__c</field>
        <value xsi:type="xsd:string">hfvbfd6ps74ddhdvfj.hf6_hd8vcrde</value>
    </values>
    <values>
        <field>Scope__c</field>
        <value xsi:type="xsd:string">profile-read</value>
    </values>
    <values>
        <field>UserInfoURL__c</field>
        <value xsi:type="xsd:string">https://auth.webfleet.com/uaa/api/user</value>
    </values>
</CustomMetadata>
