<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Inicial Setup</label>
    <protected>false</protected>
    <values>
        <field>BlacklistedProviders__c</field>
        <value xsi:type="xsd:string">&quot;DE02:DE03:NL02:NL03:ES04&quot;</value>
    </values>
    <values>
        <field>ContactMainAccountStatus__c</field>
        <value xsi:type="xsd:string">Active</value>
    </values>
    <values>
        <field>ContactMainAccountType__c</field>
        <value xsi:type="xsd:string">Customer</value>
    </values>
    <values>
        <field>CustomerNumbersToExclude__c</field>
        <value xsi:type="xsd:string">&quot;830534:830536:830537:831889:438438:438437:830538:436895&quot;</value>
    </values>
    <values>
        <field>EmailsToExclude__c</field>
        <value xsi:type="xsd:string">account@,accounts@,sales@,enquiries@,info@,purchaseledger@,accountspayable@,Billing@,Accounting@,Purchasing@,Finance@,Creditors@,contabilidade@,administracao@,financeiro@,compras@,adm@,admin@,contabilidad@,administracion@,finanzas@,comptabilite@,administration@,factures@,achats@,achat@,compta@,inkoop@,facturen@,crediteuren@,administratie@,invoice@,invoicing@rechnungen@,einkauf@,kontakt@,ksiegowosc@,biuro@,faktury@,faktura@,rozliczenia@,sekretariat@,finanse@,kadry@,hr@,e-faktura@,efaktury@,efaktura@,efaktury@,serwis@,fatturazione@,amministrazione@,creditori@,fattura@,finanza@,finanze@,contabilita@,acquisti@,fatture@,segreteria@,commerciale@,supporto@,vendite@,marketing@,noreply@,@billing,@account,@purchasing,mail@,contact@,financieel@,informatica@,commercial@,comercial@,amministrazione1@,clients@,client@,contact@,contacto@,control@,coordinacion,mail@,service@,office@,officina@compta-cph,compta.sefr,compta,client,post@,postbox@,postbox.work@,postmaster,reclamaciones@,receptionist@,trafico@,transport2@,transport@,wagenpark@,unknown@,verkauf@,boekhouding@,boekhouding02@,tranvouez@,direction.ahf56@,accueil@,exploitation@,accueil@,exploitation@,werkstatt@,rechnungen@,werkstatt@,rechnungseingang@,verkauf@,1234@,crediteur@,boekhouding@,transfrigo@,trans@,transport.lenalogistics@,direccion@,ventas@,dgf.gb.payablequeries@,no@no.no,factuur@,financien@,invoicing@,flandre-sol@,facture@,secretariat@,info4@,technik@,@bridgestone,@tomtom,management@</value>
    </values>
    <values>
        <field>ListofCountries__c</field>
        <value xsi:type="xsd:string">&quot;AU:AT:BE:CA:CL:CZ:DK:FI:FR:DE:IE:IT:LU:MX:MC:NL:NZ:NO:PL:PT:SM:ZA:ES:SE:CH:GB:US&quot;</value>
    </values>
    <values>
        <field>MinimumActiveWebfleetUnits__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>NPSSendDateOlderThanxMonths__c</field>
        <value xsi:type="xsd:double">12.0</value>
    </values>
    <values>
        <field>ProviderDoesNotStartsWith__c</field>
        <value xsi:type="xsd:string">&quot;UBI:CUR:CC:CZ&quot;</value>
    </values>
</CustomMetadata>
