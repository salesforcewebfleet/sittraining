<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PRO7350</label>
    <protected>false</protected>
    <values>
        <field>ACRField__c</field>
        <value xsi:type="xsd:string">PRO7350__c</value>
    </values>
    <values>
        <field>CDOField__c</field>
        <value xsi:type="xsd:string">PRO7350__c</value>
    </values>
</CustomMetadata>
