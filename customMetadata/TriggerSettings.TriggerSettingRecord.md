<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Trigger Setting Record</label>
    <protected>false</protected>
    <values>
        <field>LeadCreateCampaignMembersonChange__c</field>
        <value xsi:type="xsd:string">&apos;Eloqua&apos;,&apos;B2BMA Integration&apos;,&apos;Gema Agueras Gomez&apos;,&apos;Victor Cazorla&apos;,&apos;Cristina Andiñach&apos;,&apos;Peter Rodríguez&apos;</value>
    </values>
</CustomMetadata>
