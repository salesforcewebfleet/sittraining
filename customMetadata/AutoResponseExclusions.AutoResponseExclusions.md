<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Auto Response Exclusions</label>
    <protected>false</protected>
    <values>
        <field>DomainAddress__c</field>
        <value xsi:type="xsd:string">@webfleet.com</value>
    </values>
    <values>
        <field>EmailAddress__c</field>
        <value xsi:type="xsd:string">secondlevel@dako.de,ticket@dako.de,support@dako.de,roland.merkel@dako.de,sirko.lippold@dako.de,noreply@formstack.com</value>
    </values>
</CustomMetadata>
