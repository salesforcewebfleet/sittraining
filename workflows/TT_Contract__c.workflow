<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Automatic_End_Date_Autorenewal</fullName>
        <description>Automatic End Date Autorenewal</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/Automatic_End_Date_Autorenewal</template>
    </alerts>
    <alerts>
        <fullName>Automatic_End_Date_No_Autorenewal</fullName>
        <description>Automatic End Date No Autorenewal</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/Automatic_End_Date_No_Autorenewal</template>
    </alerts>
    <alerts>
        <fullName>Autorenew_Contract_Expiration_Reminder</fullName>
        <description>Autorenew Contract Expiration Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/Autorenew_Contract_Expiration_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Autorenewal_Contract_Expiration</fullName>
        <description>Autorenewal Contract Expiration</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/Autorenew_Contract_Expiration</template>
    </alerts>
    <alerts>
        <fullName>High_Value_Contract_Expiration</fullName>
        <description>High Value Contract Expiration</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/High_Value_Contract_Expiration</template>
    </alerts>
    <alerts>
        <fullName>High_Value_Contract_Expiration_Reminder</fullName>
        <description>High Value Contract Expiration Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/High_Value_Contract_Expiration_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Low_Value_Contract_Expiration</fullName>
        <description>Low Value Contract Expiration</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/Low_Value_Contract_Expiration</template>
    </alerts>
    <alerts>
        <fullName>Low_Value_Contract_Expiration_Reminder</fullName>
        <description>Low Value Contract Expiration Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/Low_Value_Contract_Expiration_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Tel_Legal_High_Value_Extension_Requested</fullName>
        <ccEmails>legal.telematics@tomtom.com</ccEmails>
        <description>Telematics Legal: High Value Extension Requested</description>
        <protected>false</protected>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/Legal_High_Value_Extension_Requested</template>
    </alerts>
    <alerts>
        <fullName>Tel_Legal_Terminate_auto_renewal_contract</fullName>
        <ccEmails>legal.telematics@tomtom.com</ccEmails>
        <description>Telematics Legal Terminate auto-renewal contract</description>
        <protected>false</protected>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/Legal_Terminate_auto_renewal_contract</template>
    </alerts>
    <alerts>
        <fullName>Termination_completed</fullName>
        <description>Termination completed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/Termination_Completed</template>
    </alerts>
    <alerts>
        <fullName>Termination_not_possible</fullName>
        <description>Termination not possible</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_contractmanager@webfleet.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CLM_Templates/Termination_Not_Possible</template>
    </alerts>
</Workflow>