<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copy_Product_Family_To_Line_Item</fullName>
        <field>Product_Family__c</field>
        <formula>TEXT(Product2.Family)</formula>
        <name>Copy Product Family To Line Item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Copy Product Family Information</fullName>
        <actions>
            <name>Copy_Product_Family_To_Line_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Family</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Copies the product family onto the product line item of an opportunity so it can be used by roll up summary fields (since they can&apos;t step through to the product record directly)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>