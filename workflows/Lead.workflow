<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Eloqua_Contact_Not_Found</fullName>
        <ccEmails>matthew.wise@webfleet.com</ccEmails>
        <ccEmails>stephan.wiebigke@webfleet.com</ccEmails>
        <ccEmails>axel.peters@webfleet.com</ccEmails>
        <description>Eloqua Contact Not Found</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Eloqua_Contact_Not_Found</template>
    </alerts>
    <alerts>
        <fullName>Eloqua_Existing_Record_Not_Updated_Lead</fullName>
        <ccEmails>matthew.wise@webfleet.com</ccEmails>
        <ccEmails>stephan.wiebigke@webfleet.com</ccEmails>
        <ccEmails>axel.peters@webfleet.com</ccEmails>
        <description>Eloqua Existing Record Not Updated Lead</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Eloqua_Existing_Record_Not_Updated</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_Laurence_Odendaal_when_a_new_connect_partner_lead_is_created</fullName>
        <description>Email alert to Laurence Odendaal when a new .connect partner lead is created</description>
        <protected>false</protected>
        <recipients>
            <recipient>laurence.odendaal@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>All_templates/LeadsWebtoLeademailresponseSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>LeadUpdatebyPardot</fullName>
        <description>Lead Update by Pardot</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>axel.peters@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>celine.provinitaze-bernard@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Workflow_Templates/Lead_Update_Act_On</template>
    </alerts>
    <alerts>
        <fullName>Lead_Update_by_Act_On_Internal</fullName>
        <description>Lead Update by Act-On (Internal - No FR)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Lead_Update_Act_On</template>
    </alerts>
    <alerts>
        <fullName>Lead_Update_by_Act_On_Internal_FR</fullName>
        <description>Lead Update by Act-On (Internal FR)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>celine.provinitaze-bernard@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Lead_Update_Act_On</template>
    </alerts>
    <alerts>
        <fullName>Lead_Update_by_Act_On_Partner</fullName>
        <description>Lead Update by Act-On (Partner - No FR)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Lead_Update_Eloqua_Partner</template>
    </alerts>
    <alerts>
        <fullName>Lead_Update_by_Act_On_Partner_FR</fullName>
        <description>Lead Update by Act-On (Partner FR)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>celine.provinitaze-bernard@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Lead_Update_Eloqua_Partner</template>
    </alerts>
    <alerts>
        <fullName>Lead_approved</fullName>
        <description>Lead approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_process/Approved_request_Lead</template>
    </alerts>
    <alerts>
        <fullName>Lead_rejected</fullName>
        <description>Lead rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_process/Rejected_approval_Lead</template>
    </alerts>
    <alerts>
        <fullName>Lead_update_by_Partner</fullName>
        <description>Lead update by Partner</description>
        <protected>false</protected>
        <recipients>
            <field>Reseller_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>France/Lead_update_by_Partner</template>
    </alerts>
    <alerts>
        <fullName>Notify_owner_manager_of_neglected_lead</fullName>
        <description>Notify owner &amp; manager of neglected lead</description>
        <protected>false</protected>
        <recipients>
            <field>Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Workflow_Templates/Lead_Neglected_Lead</template>
    </alerts>
    <alerts>
        <fullName>Send_out_email_to_reseller_with_lead_info</fullName>
        <description>Send out email to reseller with lead info</description>
        <protected>false</protected>
        <recipients>
            <field>Reseller_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Partner_Lead_Assignment</template>
    </alerts>
    <fieldUpdates>
        <fullName>Campaign_Opt_in_vs_Opt_out</fullName>
        <field>Campaign_Opt_out__c</field>
        <literalValue>0</literalValue>
        <name>Campaign Opt-in vs Opt-out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campaign_Opt_out_vs_Opt_In</fullName>
        <field>Campaign_Opt_in__c</field>
        <literalValue>0</literalValue>
        <name>Campaign Opt-out vs Opt-In</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_RecType_to_Inside_Sales</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Pre_Sales_Lead_Complete</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change RecType to Inside Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_RecType_to_complete</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Pre_Sales_Lead_Complete</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change RecType to complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_RecType_to_connect_Partner</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Connect_Partner_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change RecType to .connect Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Rectype_to_Pre_Sales_Lead</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Pre_Sales_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Rectype to Pre Sales Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Rectype_to_Pre_Sales_Lead_Compl</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Pre_Sales_Lead_Complete</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Rectype to Pre Sales Lead (Compl)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Rectype_to_Reseller_Complete</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Reseller_Complete_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Rectype to Reseller Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Rectype_to_Reseller_Incomplete</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Reseller_Incomplete_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Rectype to Reseller Incomplete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_MKT_Nurture</fullName>
        <field>Status</field>
        <literalValue>MKT Nurture</literalValue>
        <name>Change Status=MKT Nurture</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Lead_Description</fullName>
        <description>Copies the &apos;original&apos; Lead Description to the Original Description field</description>
        <field>Original_Description__c</field>
        <formula>Description</formula>
        <name>Copy Lead Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_set_to_Ready_to_Convert</fullName>
        <description>Timestamps the date a lead is set to ready to convert. Used to calculate the time since a lead is set to ready to convert.</description>
        <field>Date_set_to_Ready_to_Convert__c</field>
        <formula>TODAY()</formula>
        <name>Date set to Ready to Convert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dead_lead_is_cold</fullName>
        <field>Rating</field>
        <literalValue>cold</literalValue>
        <name>Dead lead is cold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_mail_Opt_In_vs_Opt_Out</fullName>
        <description>If Email Opt In is selected, then Email Opt-Out will be de-selected</description>
        <field>HasOptedOutOfEmail</field>
        <literalValue>0</literalValue>
        <name>E-mail Opt-In vs Opt-Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_mail_Opt_Out_vs_Opt_In</fullName>
        <description>If Email Opt Out is selected, then Email Opt-In will be de-selected</description>
        <field>Email_Opt_In__c</field>
        <literalValue>0</literalValue>
        <name>E-mail Opt-Out vs Opt-In</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FR_Opt_In</fullName>
        <description>If  a new Lead is created for France and no Opt-In or Opt-Out is selected, the Lead will automatically be Opt-In</description>
        <field>Email_Opt_In__c</field>
        <literalValue>1</literalValue>
        <name>FR Opt-In</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inform_Campign_Id</fullName>
        <field>Campaign_Id__c</field>
        <formula>Campaign.Id</formula>
        <name>Inform Campign Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_status_update</fullName>
        <description>Lead status update when lead source =Corporama</description>
        <field>Status</field>
        <literalValue>Raw Data</literalValue>
        <name>Lead status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRO_5150_web_leads_Status_update</fullName>
        <description>If a Web lead comes in from this campaign: 701a0000000dwPe - 70114000002MlD1[Activation_data]  then the Lead Status needs to be changed from Web (which is default for all leads) into Raw Data</description>
        <field>Status</field>
        <literalValue>Raw Data</literalValue>
        <name>PRO 5150 web leads Status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ready_to_Covert_True</fullName>
        <field>Ready_to_convert__c</field>
        <literalValue>1</literalValue>
        <name>Ready to Covert = True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ready_to_Covert_True1</fullName>
        <field>Ready_to_convert__c</field>
        <literalValue>1</literalValue>
        <name>Ready to Covert = True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ready_to_Covert_True2</fullName>
        <field>Ready_to_convert__c</field>
        <literalValue>1</literalValue>
        <name>Ready to Covert = True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ready_to_convert_False</fullName>
        <field>Ready_to_convert__c</field>
        <literalValue>0</literalValue>
        <name>Ready to convert = False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ready_to_convert_True</fullName>
        <field>Ready_to_convert__c</field>
        <literalValue>1</literalValue>
        <name>Ready to convert = True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_to_MKT_Nurture_BNL_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>MKT_Nurture_Queue_BNL</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Send to MKT Nurture BNL queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_to_MKT_Nurture_DACH_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>MKT_Nurture_Queue_DACH</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Send to MKT Nurture DACH queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_to_MKT_Nurture_ES_queue</fullName>
        <description>Send the lead to MKT queue if the status is MKT, owner isn&apos;t telesales and status isn&apos;t SelfGen</description>
        <field>OwnerId</field>
        <lookupValue>MKT_Nurture_Queue_ES</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Send to MKT Nurture ES queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_to_MKT_Nurture_FR_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>MKT_Nurture_Queue_FR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Send to MKT Nurture FR queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_to_MKT_Nurture_IT_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>MKT_Nurture_Queue_IT</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Send to MKT Nurture IT queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_to_MKT_Nurture_PT_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>MKT_Nurture_Queue_PT</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Send to MKT Nurture PT queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_to_MKT_Nurture_UKI_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>MKT_Nurture_Queue_UK</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Send to MKT Nurture UKI queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Status_Update_By</fullName>
        <field>Lead_Status_Last_Modified_By__c</field>
        <formula>$User.LastName  &amp;  &quot;, &quot; &amp;  $User.FirstName</formula>
        <name>Set Last Status Update By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Status_Update_Date</fullName>
        <field>Lead_Status_Last_Modified__c</field>
        <formula>TODAY()</formula>
        <name>Set Last Status Update Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Approved</fullName>
        <field>Pt_Qty_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status= Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_No_requested</fullName>
        <field>Pt_Qty_Approval_Status__c</field>
        <literalValue>Not requested</literalValue>
        <name>Status= No requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Pending</fullName>
        <field>Pt_Qty_Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Status=Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Rejected</fullName>
        <field>Pt_Qty_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status= Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Telesales_Assignment_BNL</fullName>
        <field>OwnerId</field>
        <lookupValue>BeNeLuxLeadQueue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Telesales Assignment BNL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Telesales_nurture_update</fullName>
        <field>Status</field>
        <literalValue>Nurture</literalValue>
        <name>Telesales nurture update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timestamp_when_Converted</fullName>
        <field>Date_set_to_Converted__c</field>
        <formula>now()</formula>
        <name>Timestamp when Converted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timestamp_when_set_Dead</fullName>
        <field>Date_set_to_Dead__c</field>
        <formula>now()</formula>
        <name>Timestamp when set Dead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_set_to_New</fullName>
        <description>Updates every time a Lead Status is Set to New</description>
        <field>Lead_Status_set_to_New__c</field>
        <formula>NOW()</formula>
        <name>Update Date set to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Eloqua_Lead_Score</fullName>
        <field>Eloqua_Lead_Score__c</field>
        <formula>TEXT (35)</formula>
        <name>Update Eloqua Lead Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_First_Campaign</fullName>
        <description>Upddate First Campaign field with the name and the ID of the first campaign that a Lead is coming from</description>
        <field>First_Campaign__c</field>
        <formula>IF ( ISBLANK(Campaign.Name),&apos;No working&apos;, 
IF(ISBLANK( Campaign_Id__c ), 
Campaign.Name &amp; &apos; (&apos; &amp; CampaignId &amp; &apos;)&apos;,
Campaign.Name &amp; &apos; (&apos; &amp; Campaign_Id__c &amp; &apos;)&apos;
))</formula>
        <name>Update First Campaign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_GDRP_Opt_In_Stamp_Date</fullName>
        <description>Update the field with the current value</description>
        <field>GDPR_Opt_In_Stamp_Date__c</field>
        <formula>now()</formula>
        <name>Update GDRP Opt In Stamp Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_GDRP_Opt_Out_Stamp_Date</fullName>
        <field>GDPR_Opt_Out_Stamp_Date__c</field>
        <formula>now()</formula>
        <name>Update GDRP Opt Out Stamp Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Reseller_Email</fullName>
        <field>Reseller_Email__c</field>
        <formula>Assigned_to_Reseller__r.Reseller_Lead_Email_Address__c</formula>
        <name>Update Lead Reseller Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Reseller_Owner_Email</fullName>
        <description>Email address of the owner of the Reseller account.</description>
        <field>Reseller_Owner_Email__c</field>
        <formula>Assigned_to_Reseller__r.Owner_Link__r.Email</formula>
        <name>Update Lead Reseller Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source</fullName>
        <description>Update Lead Source= Marketing Partners</description>
        <field>LeadSource</field>
        <literalValue>Marketing Partners</literalValue>
        <name>Update Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Status_to_NEW</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Update Lead Status to NEW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managers_Email_Lead</fullName>
        <description>Updates the Managers Email address so it can be used by other workflows</description>
        <field>Managers_Email__c</field>
        <formula>Owner_Link__r.Manager.Email</formula>
        <name>Update Managers Email (Lead)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Type_Form</fullName>
        <field>Form__c</field>
        <formula>&apos;SQL_Form&apos;</formula>
        <name>Update Type Form</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_connect_partner_lead</fullName>
        <description>Form updates to .connect partner lead</description>
        <field>RecordTypeId</field>
        <lookupValue>Connect_Partner_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to .connect partner lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>change_ready_to_convert_telesales</fullName>
        <description>Ready to convert = True</description>
        <field>Ready_to_convert__c</field>
        <literalValue>1</literalValue>
        <name>change ready to convert telesales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>created_by_email_update</fullName>
        <field>created_by_email_SFDC__c</field>
        <formula>CreatedBy.Email</formula>
        <name>created by email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>lead_owner_change</fullName>
        <field>OwnerId</field>
        <lookupValue>BeNeLuxLeadQueue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>lead owner change</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>telesales_not_ready_to_convert</fullName>
        <description>Ready to convert = False</description>
        <field>Ready_to_convert__c</field>
        <literalValue>0</literalValue>
        <name>telesales not ready to convert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>TO_ELOQUA_LEADS_V1</fullName>
        <apiVersion>42.0</apiVersion>
        <endpointUrl>https://tttics-a485859.integration.em2.oraclecloud.com/integration/flowsvc/salesforce/PROD_SALESF_TO_ELOQUA_LEADS/v01/</endpointUrl>
        <fields>Accepted_API_SDK_license__c</fields>
        <fields>Accepted_MDM_Agreement__c</fields>
        <fields>Accepted_NDA__c</fields>
        <fields>Accepted_dcPP_Agreement__c</fields>
        <fields>Act_On_Lead_Score_del_del__c</fields>
        <fields>AnnualRevenue</fields>
        <fields>App_API_key__c</fields>
        <fields>Appointment_date__c</fields>
        <fields>ApptSetToLeadConversion__c</fields>
        <fields>ApptSetToLeadTreatment__c</fields>
        <fields>Assigned_to_Reseller__c</fields>
        <fields>Backoffice_software__c</fields>
        <fields>BizDev_Lead_or_Contact__c</fields>
        <fields>Budget__c</fields>
        <fields>Business_Need__c</fields>
        <fields>CC_Advertising_Display__c</fields>
        <fields>CC_Advertising_Raw__c</fields>
        <fields>Campaign_Id__c</fields>
        <fields>Campaign_Opt_in__c</fields>
        <fields>Campaign_Opt_out__c</fields>
        <fields>City</fields>
        <fields>Company</fields>
        <fields>Company_Registration_Number__c</fields>
        <fields>Competitive_Contract_End_Date__c</fields>
        <fields>Competitor__c</fields>
        <fields>Connect_Partner__c</fields>
        <fields>ConnectionReceivedId</fields>
        <fields>ConnectionSentId</fields>
        <fields>ConvertedAccountId</fields>
        <fields>ConvertedContactId</fields>
        <fields>ConvertedDate</fields>
        <fields>ConvertedOpportunityId</fields>
        <fields>Country</fields>
        <fields>CreatedById</fields>
        <fields>CreatedDate</fields>
        <fields>Created_DateTime__c</fields>
        <fields>Createdbefore_3_wks__c</fields>
        <fields>CurrencyIsoCode</fields>
        <fields>Current_Environment__c</fields>
        <fields>Customer_Sales_Support_Description__c</fields>
        <fields>DB_Created_Date_without_Time__c</fields>
        <fields>DB_Lead_Age__c</fields>
        <fields>Data_Quality_Description__c</fields>
        <fields>Data_Quality_Score__c</fields>
        <fields>Date_set_to_Converted__c</fields>
        <fields>Date_set_to_Dead__c</fields>
        <fields>Decision_Making_Process__c</fields>
        <fields>Description</fields>
        <fields>DoNotCall</fields>
        <fields>Duplicate_flag__c</fields>
        <fields>Eloqua_EngagementScore__c</fields>
        <fields>Eloqua_GDPR_Opt_In_Date__c</fields>
        <fields>Eloqua_GDPR_Opt_Out_Date__c</fields>
        <fields>Eloqua_Lead_Score__c</fields>
        <fields>Eloqua_ProfileScore__c</fields>
        <fields>Eloqua_Rating__c</fields>
        <fields>Email</fields>
        <fields>EmailBouncedDate</fields>
        <fields>EmailBouncedReason</fields>
        <fields>Email_Opt_In__c</fields>
        <fields>Email_Subscribe__c</fields>
        <fields>Email_Unsubscribe__c</fields>
        <fields>Events__c</fields>
        <fields>External_Account_Id__c</fields>
        <fields>External_Guid__c</fields>
        <fields>Fax</fields>
        <fields>FirstName</fields>
        <fields>First_Campaign__c</fields>
        <fields>Form__c</fields>
        <fields>Former_Lead_Owner_Id__c</fields>
        <fields>GACLIENTID__c</fields>
        <fields>GATRACKID__c</fields>
        <fields>GAUSERID__c</fields>
        <fields>GCLID__c</fields>
        <fields>GDPR_Evidence__c</fields>
        <fields>GDPR_Opt_In_Stamp_Date__c</fields>
        <fields>GDPR_Opt_In__c</fields>
        <fields>GDPR_Opt_Out_Stamp_Date__c</fields>
        <fields>GDPR_Opt_Source__c</fields>
        <fields>GDPR_Score__c</fields>
        <fields>GDPR_Submitted_on_behalf__c</fields>
        <fields>GeocodeAccuracy</fields>
        <fields>HasOptedOutOfEmail</fields>
        <fields>HasOptedOutOfFax</fields>
        <fields>Hash__c</fields>
        <fields>Hours_Open__c</fields>
        <fields>Hours_Since_Creation__c</fields>
        <fields>Id</fields>
        <fields>Inadequate_contact_information__c</fields>
        <fields>Independent_Hardware_Vendor__c</fields>
        <fields>Independent_Software_Vendor__c</fields>
        <fields>Industry</fields>
        <fields>Industry_Code__c</fields>
        <fields>Industry_Insights__c</fields>
        <fields>Inside_Sales_Notes__c</fields>
        <fields>Inside_Sales_Status_Old__c</fields>
        <fields>Inside_Sales_Status__c</fields>
        <fields>Interested_in_upgrade__c</fields>
        <fields>International_Account__c</fields>
        <fields>IsConverted</fields>
        <fields>IsDeleted</fields>
        <fields>IsUnreadByOwner</fields>
        <fields>Jigsaw</fields>
        <fields>JigsawContactId</fields>
        <fields>Job_Role__c</fields>
        <fields>Key_Account__c</fields>
        <fields>LID__LinkedIn_Company_Id__c</fields>
        <fields>LID__LinkedIn_Member_Token__c</fields>
        <fields>Language__c</fields>
        <fields>LastActivityDate</fields>
        <fields>LastModifiedById</fields>
        <fields>LastModifiedDate</fields>
        <fields>LastName</fields>
        <fields>LastReferencedDate</fields>
        <fields>LastTransferDate</fields>
        <fields>LastViewedDate</fields>
        <fields>Latitude</fields>
        <fields>LeadSource</fields>
        <fields>Lead_Owner_Company_Name__c</fields>
        <fields>Lead_Owner_Email_Id__c</fields>
        <fields>Lead_Owner_Email__c</fields>
        <fields>Lead_Owner_Profile1__c</fields>
        <fields>Lead_Source_Original__c</fields>
        <fields>Lead_Source_and_Owner_Role__c</fields>
        <fields>Lead_Status_Last_Modified_By__c</fields>
        <fields>Lead_Status_Last_Modified__c</fields>
        <fields>Lead_Status_Update__c</fields>
        <fields>Lead_Status_and_Country__c</fields>
        <fields>Lead_duplicate_check__c</fields>
        <fields>Learning__c</fields>
        <fields>Longitude</fields>
        <fields>Managers_Email__c</fields>
        <fields>Marketing_ID__c</fields>
        <fields>MasterRecordId</fields>
        <fields>MobilePhone</fields>
        <fields>Newsletter__c</fields>
        <fields>Next_Step__c</fields>
        <fields>NumberOfEmployees</fields>
        <fields>Office_Softwa__c</fields>
        <fields>Office_Software__c</fields>
        <fields>Old_Inside_Sales_Notes__c</fields>
        <fields>Original_Description__c</fields>
        <fields>Other_Opt_out_reason__c</fields>
        <fields>OwnerId</fields>
        <fields>Owner_ID_del__c</fields>
        <fields>Owner_Link__c</fields>
        <fields>Owner_Type__c</fields>
        <fields>PartnerAccountId</fields>
        <fields>Partner_Manager_Alias__c</fields>
        <fields>Phone</fields>
        <fields>PhotoUrl</fields>
        <fields>PostalCode</fields>
        <fields>Potential_Sub_Qty_Checked__c</fields>
        <fields>Potential_Sub_Qty_Restricted__c</fields>
        <fields>Potential_Sub_Qty__c</fields>
        <fields>Primary_Fleet_Purpose__c</fields>
        <fields>Primary_Fleet_Size__c</fields>
        <fields>Primary_Vehicle_Type__c</fields>
        <fields>Pt_Qty_Approval_Status__c</fields>
        <fields>Rating</fields>
        <fields>Ready_to_convert__c</fields>
        <fields>Reasons_not_Interested__c</fields>
        <fields>RecordTypeId</fields>
        <fields>Referral_Account__c</fields>
        <fields>Referral_provided_by__c</fields>
        <fields>Referrer_Company_Name__c</fields>
        <fields>Referrer_Customer_Number__c</fields>
        <fields>Referrer_E_mail_Address__c</fields>
        <fields>Referrer_Name__c</fields>
        <fields>Referrer_Phone__c</fields>
        <fields>Referrer_Reseller__c</fields>
        <fields>Region_Combined__c</fields>
        <fields>Region__c</fields>
        <fields>Reseller_Email__c</fields>
        <fields>Reseller_Owner_Email__c</fields>
        <fields>Salutation</fields>
        <fields>Secondary_Fleet_Purpose__c</fields>
        <fields>Secondary_Fleet_Size__c</fields>
        <fields>Secondary_Vehicle_Type__c</fields>
        <fields>Solution_Provider__c</fields>
        <fields>Special_offers_Promotions__c</fields>
        <fields>State</fields>
        <fields>Status</fields>
        <fields>Street</fields>
        <fields>Surveys__c</fields>
        <fields>SystemModstamp</fields>
        <fields>System_Integrator_Consultant__c</fields>
        <fields>Target_Achievement__c</fields>
        <fields>Telesales_ID__c</fields>
        <fields>Telesales_Reference__c</fields>
        <fields>Timeframe__c</fields>
        <fields>Title</fields>
        <fields>Total_Fleet_Size_Number__c</fields>
        <fields>Total_Fleet_Size_del__c</fields>
        <fields>Traffic_Source_Detailed__c</fields>
        <fields>Traffic_Source__c</fields>
        <fields>Type__c</fields>
        <fields>Type_of_application__c</fields>
        <fields>Unsubscribe_reason__c</fields>
        <fields>User__c</fields>
        <fields>VAT_number__c</fields>
        <fields>WF_Account_Name__c</fields>
        <fields>Warm_up_date__c</fields>
        <fields>Website</fields>
        <fields>app_partner_email__c</fields>
        <fields>app_partner_name__c</fields>
        <fields>app_solution_name__c</fields>
        <fields>created_by_email_SFDC__c</fields>
        <fields>cta__c</fields>
        <fields>pw_cc__AddressStatus__c</fields>
        <fields>pw_cc__CountryLookup__c</fields>
        <fields>pw_cc__CurrentGenerators__c</fields>
        <fields>pw_cc__NumberofLocations__c</fields>
        <fields>pw_cc__Primary__c</fields>
        <fields>pw_cc__ProductInterest__c</fields>
        <fields>pw_cc__SICCode__c</fields>
        <fields>pw_cc__StateLookup__c</fields>
        <fields>pw_cc__ZipCodeLookup__c</fields>
        <fields>qbdialer__CloseDate__c</fields>
        <fields>qbdialer__CloseScore__c</fields>
        <fields>qbdialer__ContactDate__c</fields>
        <fields>qbdialer__ContactScoreId__c</fields>
        <fields>qbdialer__ContactScore__c</fields>
        <fields>qbdialer__Dials__c</fields>
        <fields>qbdialer__LastCallTime__c</fields>
        <fields>qbdialer__ResponseTime__c</fields>
        <fields>sfLma__Subscriber_Org_Type__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>tomtom_support@engagementfactory.com</integrationUser>
        <name>TO_ELOQUA_LEADS_V1</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>%2EConnect Partner Lead</fullName>
        <actions>
            <name>Update_to_connect_partner_lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Connect_Partner__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Field Sales Lead</value>
        </criteriaItems>
        <description>If in a Field Sales form .connect partner checkbox is enable, the form changes to .connect partner one instead of the Complete form</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Campaign Opt-in vs Opt-out</fullName>
        <actions>
            <name>Campaign_Opt_in_vs_Opt_out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Campaign is Opt-In, then Opt-Out will be de-selected</description>
        <formula>( 
ISCHANGED(  Campaign_Opt_in__c  ) &amp;&amp; 
 Campaign_Opt_in__c  = TRUE 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Campaign Opt-out vs Opt-In</fullName>
        <actions>
            <name>Campaign_Opt_out_vs_Opt_In</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Campaign is Opt-Out, then Opt-In will be de-selected</description>
        <formula>( 
ISCHANGED(  Campaign_Opt_out__c  ) &amp;&amp; 
 Campaign_Opt_out__c  = TRUE 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Lead Status%3D MKT Nurture</fullName>
        <actions>
            <name>Change_Status_MKT_Nurture</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Id__c</field>
            <operation>contains</operation>
            <value>7011O000002858B,7011O000002bXyc</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>eloqua</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Competitive Contract Ends</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Competitive_Contract_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Dead,Ready to convert</value>
        </criteriaItems>
        <description>Reminder generated 3 month before the Competitive Contract End Date arrive to the end.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Competitive_Contract_arrives_to_the_end</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.Competitive_Contract_End_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Completed Lead Assigned To Internal User</fullName>
        <actions>
            <name>Change_Rectype_to_Pre_Sales_Lead_Compl</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Reseller Complete Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Owner_Type__c</field>
            <operation>equals</operation>
            <value>TomTom</value>
        </criteriaItems>
        <description>Changes the record type to Internal if owner changed to an internal user</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Completed Lead Assigned To Reseller</fullName>
        <actions>
            <name>Change_Rectype_to_Reseller_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pre Sales Lead (Complete)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Owner_Type__c</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>Changes the record type to Reseller if owner changed to a Partner user. Was previously triggered whenever status changed to &quot;Assigned to reseller&quot;, but change of ownership is more logical.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Copy Lead Description</fullName>
        <actions>
            <name>Copy_Lead_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the &apos;original&apos; Lead Description to the Original Description field</description>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Copy Managers Email Address %28Lead%29</fullName>
        <actions>
            <name>Update_Managers_Email_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Inserts the owner&apos;s manager&apos;s email address into a special email field so it can be used by other workflows</description>
        <formula>ISCHANGED( OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Corporama Integration</fullName>
        <actions>
            <name>Lead_status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Corporama</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Id__c</field>
            <operation>equals</operation>
            <value>70114000002BnZY</value>
        </criteriaItems>
        <description>Lead status update when lead source = corporama or Campaign Id=70114000002BnZY</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Dead Lead is cold</fullName>
        <actions>
            <name>Dead_lead_is_cold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Dead</value>
        </criteriaItems>
        <description>Upon saving the lead with status dead the rating will be converted to cold</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt-In vs Opt-Out</fullName>
        <actions>
            <name>E_mail_Opt_In_vs_Opt_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Lead changes to Opt-In, then Opt-Out will be de-selected</description>
        <formula>( 
ISCHANGED(  Email_Opt_In__c  ) &amp;&amp; 
 Email_Opt_In__c  = TRUE 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt-out vs Opt-In</fullName>
        <actions>
            <name>E_mail_Opt_Out_vs_Opt_In</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Lead is Opt-Out, then Opt-In will be de-selected</description>
        <formula>( 
ISCHANGED( HasOptedOutOfEmail ) &amp;&amp; 
HasOptedOutOfEmail = TRUE 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email to reseller</fullName>
        <actions>
            <name>Update_Lead_Reseller_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Reseller_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send an email to the reseller, whenever the reseller field has been filled in. Also fill the email address before sending the email</description>
        <formula>Assigned_to_Reseller__c &lt;&gt; &quot;&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_out_email_to_reseller_with_lead_info</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>From Field to Pre Sales Incomplete</fullName>
        <actions>
            <name>Change_Rectype_to_Pre_Sales_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change record type to Pre Sales lead form when a lead create by Field change to an Inside sales owner.</description>
        <formula>RecordType.Name= &apos;Field Sales Lead&apos; &amp;&amp; 
Owner:User.Profile.Name &lt;&gt; &apos;TTT Sales User&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>From Internal Complete to Internal Incomplete</fullName>
        <actions>
            <name>Change_Rectype_to_Pre_Sales_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Ready_to_convert_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Ready to convert,Converted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pre Sales Lead (Complete)</value>
        </criteriaItems>
        <description>If ths status isn&apos;t Ready to Convert, the record type changes from PreSales Complete to PreSales Incomplete and the convert button disappears</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>From Internal Incomplete to Internal Complete</fullName>
        <actions>
            <name>Change_RecType_to_complete</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Ready_to_Covert_True2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Ready to convert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pre Sales Lead,Field Sales Lead,BizDev Record Type</value>
        </criteriaItems>
        <description>If all relevant lead fields have been filled in, the record type changes from PreSales Incomplete to PreSales Complete, which shows the convert button</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>From field sales to %2EConnect Partner</fullName>
        <actions>
            <name>Change_RecType_to_connect_Partner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Ready_to_Covert_True1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Ready to convert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Field Sales Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Connect_Partner__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If .connect Partner check box is clicked, the record type changes from field sales to .connect partner, which shows the convert button</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>From field sales to Inside Sales</fullName>
        <actions>
            <name>Change_RecType_to_Inside_Sales</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Ready_to_Covert_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Ready to convert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Field Sales Lead,BizDev Record Type</value>
        </criteriaItems>
        <description>If all relevant lead fields have been filled in, the record type changes from field sales to inside sales, which shows the convert button</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Incomplete Lead Assigned To Internal User</fullName>
        <actions>
            <name>Change_Rectype_to_Pre_Sales_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Reseller Incomplete Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Owner_Type__c</field>
            <operation>equals</operation>
            <value>TomTom</value>
        </criteriaItems>
        <description>Changes the record type to Internal if owner changed to an internal user</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Incomplete Lead Assigned To Reseller</fullName>
        <actions>
            <name>Change_Rectype_to_Reseller_Incomplete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Field Sales Lead,Pre Sales Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Owner_Profile1__c</field>
            <operation>equals</operation>
            <value>TTT Gold Partner User</value>
        </criteriaItems>
        <description>Changes the record type to Reseller if owner changed to a Partner user. Was previously triggered whenever status changed to &quot;Assigned to reseller&quot;, but change of ownership is more logical.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inform Campign Id</fullName>
        <actions>
            <name>Inform_Campign_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>notEqual</operation>
            <value>Eloqua,Act-On,Gema Agueras</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Campaign_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Inform the Campaign Id field when the lead is created by a user (Not Eloqua or Act-On)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Internal Lead Complete</fullName>
        <actions>
            <name>Ready_to_convert_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Ready to convert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Internal Incomplete Lead</value>
        </criteriaItems>
        <description>If all relevant lead fields have been filled in, the record type changes from incomplete to complete, which shows the convert button</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Internal Lead Incomplete</fullName>
        <actions>
            <name>Ready_to_convert_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Ready to convert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Internal Complete Lead</value>
        </criteriaItems>
        <description>If some relevant lead fields have not been filled in, the record type changes from complete to incomplete, which hides the convert button</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Last Update Status</fullName>
        <actions>
            <name>Set_Last_Status_Update_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Last_Status_Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Whenever Lead Status is updated, date and user name are stored.</description>
        <formula>ISNEW() || ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead from Appexchange</fullName>
        <actions>
            <name>Update_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>startsWith</operation>
            <value>SFDC</value>
        </criteriaItems>
        <description>Update Lead Source= Marketing Partners when a lead is coming Appexchange</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead update by Partner</fullName>
        <actions>
            <name>Lead_update_by_Partner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Alert mail to account owner when Lead Status changed by the partner user of the assigned reseller (UserType is blank for partner users).</description>
        <formula>CONTAINS ( $Profile.Name , &apos;Partner&apos; ) &amp;&amp;    Country   = &apos;France&apos; &amp;&amp; ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead updated by Act-On</fullName>
        <actions>
            <name>Lead_Update_by_Act_On_Internal</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Follow_up_reminder_Internal</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Inform the Lead Owner when the Act-On Lead Score is updated by Act-On. Emails should not be sent to Queue&apos;s, only to users. UserID&apos;s start with &quot;005&quot;</description>
        <formula>AND 
(
Hours_Since_Creation__c &gt; 9,
ISCHANGED( Act_On_Lead_Score_del_del__c ),
LastModifiedById = &apos;005a0000009BDiW&apos;,
LEFT(OwnerId,3)=&quot;005&quot;, 
NOT (CONTAINS(Email,&quot;@tomtom.com&quot;) || CONTAINS(Email,&quot;@webfleet.com&quot;)),(Act_On_Lead_Score_del_del__c - PRIORVALUE(Act_On_Lead_Score_del_del__c)) &gt;= 35
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead updated by Act-On %28Internal FR%29</fullName>
        <actions>
            <name>Lead_Update_by_Act_On_Internal_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Follow_up_reminder_Internal</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Inform the Lead Owner when the Act-On Lead Score is updated by Act-On. Emails should not be sent to Queue&apos;s, only to users. UserID&apos;s start with &quot;005&quot; and Internal user</description>
        <formula>AND  (NOT(ISNEW()),(ISCHANGED( Act_On_Lead_Score_del_del__c )  || ISCHANGED( Eloqua_Lead_Score__c )), 
(LastModifiedById = &apos;005a0000009BDiW&apos;  ||  LastModifiedBy.Username =&apos;tomtom_support@engagementfactory.com&apos;), 
LEFT(OwnerId,3)=&quot;005&quot;,  
NOT (CONTAINS(Email,&quot;@tomtom.com&quot;) || CONTAINS(Email,&quot;@webfleet.com&quot;)), 
((Act_On_Lead_Score_del_del__c - PRIORVALUE(Act_On_Lead_Score_del_del__c)) &gt;= 35  || ( VALUE(Eloqua_Lead_Score__c) - VALUE(PRIORVALUE(Eloqua_Lead_Score__c))) &gt;= 35  ),  
(Country=&apos;France&apos; ||  Country=&apos;FR&apos;),
CONTAINS(Form__c,&apos;SQL&apos;) || Campaign_Id__c=&apos;7011O00000285gm&apos;,
Owner:User.Profile.Name&lt;&gt;&quot;TTT Gold Partner User&quot;,
/* CBM 2017 LINK410510 */
Campaign_Id__c  &lt;&gt; &apos;70114000002Cdva&apos;,
/* CBM 2017 NAV2 */ 
Campaign_Id__c  &lt;&gt;&apos;70114000002CduD&apos;,
/* DOYA Campaign FM form submit 2017 */
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4N&apos;,
/* DOYA Campaign Driver form submit 2017 */
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4S&apos;,
/* DOYA Campaign Driver OPT IN form Submit 2017 */
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDk&apos;,
/* DOYA Campaign Driver OPT OUT form submit 2017 */
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDp&apos;,
/* DOYA Campaign FM form submit OPT OUT 2017 */
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDu&apos;,
/* UKI 2017 LCY Fleet Event Registrees UK */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYZ&apos;,
/* Contact_request_Callback_LCY Fleet Event Page */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYe&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027qvz&apos;,
/* CBM Campaigns */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021JTD&apos;,
/* Others */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZG&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La9&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La4&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZz&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZu&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZk&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027pY3&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000002bXwM&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead updated by Act-On %28Internal No FR%29</fullName>
        <actions>
            <name>Lead_Update_by_Act_On_Internal</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Follow_up_reminder_Internal</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Inform the Lead Owner when the Act-On Lead Score is updated by Act-On. Emails should not be sent to Queue&apos;s, only to users. UserID&apos;s start with &quot;005&quot;</description>
        <formula>AND  ( NOT(ISNEW()),(ISCHANGED( Act_On_Lead_Score_del_del__c )  || ISCHANGED( Eloqua_Lead_Score__c )), 
( LastModifiedById = &apos;005a0000009BDiW&apos;  ||  LastModifiedBy.Username =&apos;tomtom_support@engagementfactory.com&apos;), 
LEFT(OwnerId,3)=&quot;005&quot;,  
NOT (CONTAINS(Email,&quot;@tomtom.com&quot;) || CONTAINS(Email,&quot;@webfleet.com&quot;)), 
( (Act_On_Lead_Score_del_del__c - PRIORVALUE(Act_On_Lead_Score_del_del__c)) &gt;= 35  || ( VALUE(Eloqua_Lead_Score__c) - VALUE(PRIORVALUE(Eloqua_Lead_Score__c))) &gt;= 35  ),  
Country  &lt;&gt;  &apos;France&apos; , 
Country  &lt;&gt;  &apos;FR&apos; ,
CONTAINS(Form__c,&apos;SQL&apos;),
Owner:User.Profile.Name&lt;&gt;&quot;TTT Gold Partner User&quot;,
/* CBM 2017 LINK410510 */
Campaign_Id__c  &lt;&gt; &apos;70114000002Cdva&apos;,
/* CBM 2017 NAV2 */ 
Campaign_Id__c  &lt;&gt;&apos;70114000002CduD&apos;,
/* DOYA Campaign*/
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4N&apos;,
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4S&apos;,
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDk&apos;,
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDp&apos;,
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDu&apos;,
/* UKI 2017 LCY Fleet Event Registrees UK */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYZ&apos;,
/* Contact_request_Callback_LCY Fleet Event Page */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYe&apos;,
/* Prisma ES */
Campaign_Id__c  &lt;&gt;&apos;701140000027qec&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027qeh&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027qem&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027qvz&apos;,
/* CBM Campaigns */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021JTD&apos;,
/* Others */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZG&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La9&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La4&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZz&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZu&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZk&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027pY3&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000003COtV&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000002bXwM&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000003JZma&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000003JZn4&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000003JZn9&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000003JeEN&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000003JeEX&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000003JeEh&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000003JeEc&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000002dpWh&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead updated by Act-On %28Partner No FR%29</fullName>
        <actions>
            <name>Lead_Update_by_Act_On_Partner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Follow_up_reminder_Portal</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Inform the Lead Owner when the Act-On Lead Score is updated by Act-On. Emails should not be sent to Queue&apos;s, only to users. UserID&apos;s start with &quot;005&quot; and partner users</description>
        <formula>AND  ( NOT(ISNEW()),(ISCHANGED( Act_On_Lead_Score_del_del__c )  || ISCHANGED( Eloqua_Lead_Score__c )), ( LastModifiedById = &apos;005a0000009BDiW&apos;  ||  LastModifiedBy.Username =&apos;tomtom_support@engagementfactory.com&apos;), LEFT(OwnerId,3)=&quot;005&quot;,  NOT (CONTAINS(Email,&quot;@tomtom.com&quot;) || CONTAINS(Email,&quot;@webfleet.com&quot;)), ( (Act_On_Lead_Score_del_del__c - PRIORVALUE(Act_On_Lead_Score_del_del__c)) &gt;= 35  || ( VALUE(Eloqua_Lead_Score__c) - VALUE(PRIORVALUE(Eloqua_Lead_Score__c))) &gt;= 35  ),  Country  &lt;&gt;  &apos;France&apos; , Country  &lt;&gt;  &apos;FR&apos; ,
CONTAINS(Form__c,&apos;SQL&apos;),
Owner:User.Profile.Name=&quot;TTT Gold Partner User&quot;,
/* CBM 2017 LINK410510 */
Campaign_Id__c  &lt;&gt; &apos;70114000002Cdva&apos;,
/* CBM 2017 NAV2 */ 
Campaign_Id__c  &lt;&gt;&apos;70114000002CduD&apos;,
/* DOYA Campaign*/
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4N&apos;,
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4S&apos;,
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDk&apos;,
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDp&apos;,
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDu&apos;,
/* UKI 2017 LCY Fleet Event Registrees UK */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYZ&apos;,
/* Contact_request_Callback_LCY Fleet Event Page */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYe&apos;,
/* Prisma ES */
Campaign_Id__c  &lt;&gt;&apos;701140000027qec&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027qeh&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027qem&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027qvz&apos;,
/* CBM Campaigns */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021JTD&apos;,
/* Others */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZG&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La9&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La4&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZz&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZu&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZk&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027pY3&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000002bXwM&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead updated by Eloqua %28Partner and FR%29</fullName>
        <actions>
            <name>Lead_Update_by_Act_On_Partner_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Follow_up_reminder_Portal</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Inform the Lead Owner when the Eloqua Lead Score is updated by Eloqua. Emails should not be sent to Queue&apos;s, only to users. UserID&apos;s start with &quot;005&quot; and portal users</description>
        <formula>AND (NOT(ISNEW()),(ISCHANGED( Act_On_Lead_Score_del_del__c )  || ISCHANGED( Eloqua_Lead_Score__c )), ( LastModifiedById = &apos;005a0000009BDiW&apos;  ||  LastModifiedBy.Username =&apos;tomtom_support@engagementfactory.com&apos;), LEFT(OwnerId,3)=&quot;005&quot;,  NOT (CONTAINS(Email,&quot;@tomtom.com&quot;) || CONTAINS(Email,&quot;@webfleet.com&quot;)), ( (Act_On_Lead_Score_del_del__c - PRIORVALUE(Act_On_Lead_Score_del_del__c)) &gt;= 35  || ( VALUE(Eloqua_Lead_Score__c) - VALUE(PRIORVALUE(Eloqua_Lead_Score__c))) &gt;= 35  ),  (Country=&apos;France&apos; ||  Country=&apos;FR&apos;)  ,
CONTAINS(Form__c,&apos;SQL&apos;)|| Campaign_Id__c=&apos;7011O00000285gm&apos;,
 Owner:User.Profile.Name=&quot;TTT Gold Partner User&quot;,
/* CBM 2017 LINK410510 */
Campaign_Id__c  &lt;&gt; &apos;70114000002Cdva&apos;,
/* CBM 2017 NAV2 */ 
Campaign_Id__c  &lt;&gt;&apos;70114000002CduD&apos;,
/* DOYA Campaign FM form submit 2017 */
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4N&apos;,
/* DOYA Campaign Driver form submit 2017 */
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4S&apos;,
/* DOYA Campaign Driver OPT IN form Submit 2017 */
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDk&apos;,
/* DOYA Campaign Driver OPT OUT form submit 2017 */
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDp&apos;,
/* DOYA Campaign FM form submit OPT OUT 2017 */
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDu&apos;,
/* UKI 2017 LCY Fleet Event Registrees UK */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYZ&apos;,
/* Contact_request_Callback_LCY Fleet Event Page */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYe&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027qvz&apos;,
/* CBM Campaigns */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021JTD&apos;,
/* Others */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZG&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La9&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La4&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZz&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZu&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZk&apos;,
Campaign_Id__c  &lt;&gt;&apos;701140000027pY3&apos;,
Campaign_Id__c  &lt;&gt;&apos;7011O000002bXwM&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>No Opt-out or Opt-In for FR</fullName>
        <actions>
            <name>FR_Opt_In</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Email_Opt_In__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Email_Opt_In__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>France</value>
        </criteriaItems>
        <description>If a new Lead is created for France and no Opt-In or Opt-Out is selected, the Lead will automatically be Opt-In</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRO 5150 web leads Status update</fullName>
        <actions>
            <name>PRO_5150_web_leads_Status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Id__c</field>
            <operation>equals</operation>
            <value>701a0000000dwPe,70114000002MlD1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Marketing web</value>
        </criteriaItems>
        <description>If a Web lead comes in from this campaign: 701a0000000dwPe - 70114000002MlD1 [activation-data] and the Lead Source is Marketing web, then the Lead Status needs to be changed from New (which is default for all leads) needs to into Raw Data</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Reseller Lead Complete</fullName>
        <actions>
            <name>Change_Rectype_to_Reseller_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Ready_to_convert_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Ready to convert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Reseller Incomplete Lead</value>
        </criteriaItems>
        <description>If all relevant lead fields have been filled in, the record type changes from incomplete to complete, which shows the convert button</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reseller Lead Incomplete</fullName>
        <actions>
            <name>Change_Rectype_to_Reseller_Incomplete</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Ready_to_convert_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Ready to convert,Converted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Reseller Complete Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Owner_Profile1__c</field>
            <operation>equals</operation>
            <value>TTT Gold Partner User</value>
        </criteriaItems>
        <description>If some relevant lead fields have not been filled in, the record type changes from complete to incomplete, which hides the convert button</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SA %2Econnect partners</fullName>
        <actions>
            <name>Email_alert_to_Laurence_Odendaal_when_a_new_connect_partner_lead_is_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 6 AND (2 OR 3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Independent_Hardware_Vendor__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Independent_Software_Vendor__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Solution_Provider__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.System_Integrator_Consultant__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>South Africa,SA</value>
        </criteriaItems>
        <description>An email is sent to Laurence Odendaal when a new .connect partner lead is created in South Africa</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send to MKT BNL queue</fullName>
        <actions>
            <name>Send_to_MKT_Nurture_BNL_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Send the lead to MKT queue if the status is MKT, owner isn&apos;t telesales and status isn&apos;t SelfGen</description>
        <formula>LEFT(OwnerId, 3)=&apos;005&apos;  &amp;&amp; 
ISPICKVAL(Status, &apos;MKT Nurture&apos;)  &amp;&amp;   
NOT(CONTAINS(Region__c, &apos;Inside Sales&apos;))  &amp;&amp; 
NOT(CONTAINS(TEXT( LeadSource), &apos;SelfGen&apos;))  &amp;&amp; 
(CONTAINS( Country , &apos;Belgium&apos;)  || CONTAINS( Country , &apos;Luxembourg&apos;) || CONTAINS( Country , &apos;Netherlands&apos;)) &amp;&amp; 
Independent_Hardware_Vendor__c=false  &amp;&amp; 
Independent_Software_Vendor__c =false  &amp;&amp; 
System_Integrator_Consultant__c=false  &amp;&amp; 
Solution_Provider__c =false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send to MKT DACH queue</fullName>
        <actions>
            <name>Send_to_MKT_Nurture_DACH_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Send the lead to MKT queue if the status is MKT, owner isn&apos;t telesales and status isn&apos;t SelfGen</description>
        <formula>LEFT(OwnerId, 3)=&apos;005&apos; &amp;&amp; 
ISPICKVAL(Status, &apos;MKT Nurture&apos;) &amp;&amp; 
NOT(CONTAINS(Region__c, &apos;Inside Sales&apos;)) &amp;&amp; 
NOT(CONTAINS(TEXT( LeadSource), &apos;SelfGen&apos;)) &amp;&amp; 
(CONTAINS( Country , &apos;Germany&apos;) || CONTAINS( Country , &apos;Switzerland&apos;)  || CONTAINS( Country , &apos;Austria&apos;)) &amp;&amp; 
Independent_Hardware_Vendor__c=false &amp;&amp; 
Independent_Software_Vendor__c =false &amp;&amp; 
System_Integrator_Consultant__c=false &amp;&amp; 
Solution_Provider__c =false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send to MKT ES queue</fullName>
        <actions>
            <name>Send_to_MKT_Nurture_ES_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Send the lead to MKT queue if the status is MKT, owner isn&apos;t telesales and status isn&apos;t SelfGen</description>
        <formula>LEFT(OwnerId, 3)=&apos;005&apos; &amp;&amp; 
ISPICKVAL(Status, &apos;MKT Nurture&apos;) &amp;&amp; 
NOT(CONTAINS(Region__c, &apos;Inside Sales&apos;)) &amp;&amp; 
NOT(CONTAINS(TEXT( LeadSource), &apos;SelfGen&apos;)) &amp;&amp; 
CONTAINS(Region__c, &apos;SPAIN&apos;)  &amp;&amp; 
Independent_Hardware_Vendor__c=false &amp;&amp; 
Independent_Software_Vendor__c =false &amp;&amp; 
System_Integrator_Consultant__c=false &amp;&amp; 
Solution_Provider__c =false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send to MKT FR queue</fullName>
        <actions>
            <name>Send_to_MKT_Nurture_FR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Send the lead to MKT queue if the status is MKT, owner isn&apos;t telesales and status isn&apos;t SelfGen</description>
        <formula>LEFT(OwnerId, 3)=&apos;005&apos; &amp;&amp; 
ISPICKVAL(Status, &apos;MKT Nurture&apos;) &amp;&amp; 
NOT(CONTAINS(Region__c, &apos;Inside Sales&apos;)) &amp;&amp; 
NOT(CONTAINS(TEXT( LeadSource), &apos;SelfGen&apos;)) &amp;&amp; 
CONTAINS( Country , &apos;France&apos;) &amp;&amp; 
Independent_Hardware_Vendor__c=false &amp;&amp; 
Independent_Software_Vendor__c =false &amp;&amp; 
System_Integrator_Consultant__c=false &amp;&amp; 
Solution_Provider__c =false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send to MKT IT queue</fullName>
        <actions>
            <name>Send_to_MKT_Nurture_IT_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Send the lead to MKT queue if the status is MKT, owner isn&apos;t telesales and status isn&apos;t SelfGen</description>
        <formula>LEFT(OwnerId, 3)=&apos;005&apos; &amp;&amp; 
ISPICKVAL(Status, &apos;MKT Nurture&apos;) &amp;&amp; 
NOT(CONTAINS(Region__c, &apos;Inside Sales&apos;)) &amp;&amp; 
NOT(CONTAINS(TEXT( LeadSource), &apos;SelfGen&apos;)) &amp;&amp; 
CONTAINS( Country , &apos;Italy&apos;) &amp;&amp; 
Independent_Hardware_Vendor__c=false &amp;&amp; 
Independent_Software_Vendor__c =false &amp;&amp; 
System_Integrator_Consultant__c=false &amp;&amp; 
Solution_Provider__c =false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send to MKT PT queue</fullName>
        <actions>
            <name>Send_to_MKT_Nurture_PT_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Send the lead to MKT queue if the status is MKT, owner isn&apos;t telesales and status isn&apos;t SelfGen</description>
        <formula>LEFT(OwnerId, 3)=&apos;005&apos; &amp;&amp; 
ISPICKVAL(Status, &apos;MKT Nurture&apos;) &amp;&amp; 
NOT(CONTAINS(Region__c, &apos;Inside Sales&apos;)) &amp;&amp; 
NOT(CONTAINS(TEXT( LeadSource), &apos;SelfGen&apos;)) &amp;&amp; 
CONTAINS( Country , &apos;Portugal&apos;) &amp;&amp; 
Independent_Hardware_Vendor__c=false &amp;&amp; 
Independent_Software_Vendor__c =false &amp;&amp; 
System_Integrator_Consultant__c=false &amp;&amp; 
Solution_Provider__c =false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send to MKT UKI queue</fullName>
        <actions>
            <name>Send_to_MKT_Nurture_UKI_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Send the lead to MKT queue if the status is MKT, owner isn&apos;t telesales and status isn&apos;t SelfGen</description>
        <formula>LEFT(OwnerId, 3)=&apos;005&apos; &amp;&amp; 
ISPICKVAL(Status, &apos;MKT Nurture&apos;) &amp;&amp; 
NOT(CONTAINS(Region__c, &apos;Inside Sales&apos;)) &amp;&amp; 
NOT(CONTAINS(TEXT( LeadSource), &apos;SelfGen&apos;)) &amp;&amp; 
(CONTAINS( Country , &apos;United Kingdom&apos;)  || CONTAINS( Country , &apos;Ireland&apos;)) &amp;&amp; 
Independent_Hardware_Vendor__c=false &amp;&amp; 
Independent_Software_Vendor__c =false &amp;&amp; 
System_Integrator_Consultant__c=false &amp;&amp; 
Solution_Provider__c =false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Created By Email Field</fullName>
        <actions>
            <name>created_by_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set custom field &quot;Created By Email&quot; to the email address of the creator of the lead.</description>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TO_ELOQUA_LEADS_V1</fullName>
        <actions>
            <name>TO_ELOQUA_LEADS_V1</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 5) AND 4</booleanFilter>
        <criteriaItems>
            <field>Lead.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>eloqua</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.exclude_eloqua_integration__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.web2LeadSqlFormSubmission__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Telesales Lead Complete</fullName>
        <actions>
            <name>change_ready_to_convert_telesales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Ready to convert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>contains</operation>
            <value>Telemarketing (Enterprise),Telesales Incomplete Lead,Telesales Complete Lead,Telemarketing (Enterprise) Incomplete Lead</value>
        </criteriaItems>
        <description>If all relevant lead fields have been filled in, the record type changes from incomplete to complete, which shows the convert button</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Telesales Lead Incomplete</fullName>
        <actions>
            <name>telesales_not_ready_to_convert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Ready to convert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>contains</operation>
            <value>Telemarketing (Enterprise) Incomplete Lead,Telesales Incomplete Lead</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Telesales status change</fullName>
        <actions>
            <name>Telesales_nurture_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Inside_Sales_Status__c</field>
            <operation>equals</operation>
            <value>No Interest,No Contact</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Test - Lead Status</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Ready to convert</value>
        </criteriaItems>
        <description>Inform FR Account Managers when Lead Status changes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Test Lead update ActOn</fullName>
        <actions>
            <name>Lead_Update_by_Act_On_Internal</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>NOT( ISNEW()) &amp;&amp; ISCHANGED(  Act_On_Lead_Score_del_del__c ) &amp;&amp;(OwnerId = &quot;00Ga0000003fZRe&quot; || Owner:User.Alias = &quot;skoks&quot; ) &amp;&amp; (Act_On_Lead_Score_del_del__c - PRIORVALUE(Act_On_Lead_Score_del_del__c)) &gt;= 35</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Timestamp date set to New</fullName>
        <actions>
            <name>Update_Date_set_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Timestamp date set to Ready to Convert</fullName>
        <actions>
            <name>Date_set_to_Ready_to_Convert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Status ) &amp;&amp;  ISPICKVAL( Status , &quot;Ready to Convert&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Timestamp when Converted</fullName>
        <actions>
            <name>Timestamp_when_Converted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Because the standard field only records DATE converted, this workflow will allow tracking date AND time converted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Timestamp when set Dead</fullName>
        <actions>
            <name>Timestamp_when_set_Dead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Records the date/time when lead is updated to status of Dead. This date is then used in a formula to calculate how long a lead is open.</description>
        <formula>ISCHANGED(Status) &amp;&amp; ISPICKVAL(Status,&quot;Dead&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Untouched lead</fullName>
        <actions>
            <name>lead_owner_change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Dead,Converted,Nurture</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LastModifiedById</field>
            <operation>lessThan</operation>
            <value>7 DAYS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Campaign Name SelfGen</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>startsWith</operation>
            <value>SelfGen</value>
        </criteriaItems>
        <description>When Lead Source = Self Gen*, campaign name = lead source</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Eloqua Lead Score</fullName>
        <actions>
            <name>Update_Eloqua_Lead_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>eloqua</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Eloqua_Lead_Score__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update Eloqua Lead Score = 35 if he lead is created by Eloqua and the inicial value is blank (normaly leads coming from Web-to-lead)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update First Campaign</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.First_Campaign__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update First Campaign field in Lead when a lead is coming from a campaign for first time</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update GDPR Opt OutStampDate</fullName>
        <actions>
            <name>Update_GDRP_Opt_Out_Stamp_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>PRIORVALUE( GDPR_Opt_In__c ) =true  &amp;&amp; 
GDPR_Opt_In__c= false  &amp;&amp; 
ISBLANK( GDPR_Opt_Out_Stamp_Date__c )  &amp;&amp; 
$User.Alias =&apos;eloqua&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update GDPR Opt in StampDate</fullName>
        <actions>
            <name>Update_GDRP_Opt_In_Stamp_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>PRIORVALUE( GDPR_Opt_In__c ) =false  &amp;&amp; 
GDPR_Opt_In__c= true  &amp;&amp; 
ISBLANK( GDPR_Opt_In_Stamp_Date__c )  &amp;&amp; 
$User.Alias =&apos;eloqua&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Status Eloqua</fullName>
        <actions>
            <name>Update_Lead_Status_to_NEW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Lead Status from UNQ Nurture to New if conditions are met</description>
        <formula>NOT(ISNEW())
&amp;&amp;
(CONTAINS(Form__c , &apos;SQL&apos;) ||  Campaign_Id__c=&apos;7011O00000285gm&apos;)
&amp;&amp;
(ISPICKVAL(Status,&apos;UNQ Nurture&apos;) || ISPICKVAL(Status,&apos;Raw Data&apos;) )  
&amp;&amp;  
$User.LastName == &apos;Eloqua&apos;
&amp;&amp; 
CampaignId  &lt;&gt; &apos;701a0000000dwPe&apos;
 &amp;&amp; 
CampaignId  &lt;&gt; &apos;70114000002MlD1&apos;
 &amp;&amp; 
CampaignId  &lt;&gt; &apos;7011O000003COtV&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Type Form</fullName>
        <actions>
            <name>Update_Type_Form</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>eloqua</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update TypeForm = &apos;SQL&apos; if he lead is created by Eloqua and the inicial value is blank (normaly leads coming from Web-to-lead)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>created by email Field Update</fullName>
        <actions>
            <name>created_by_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISNULL(Owner_Link__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Call Lead</fullName>
        <active>false</active>
        <description>Call Lead - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Email Lead</fullName>
        <active>false</active>
        <description>Email Lead - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Letter Lead</fullName>
        <active>false</active>
        <description>Send Letter Lead - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Competitive_Contract_arrives_to_the_end</fullName>
        <assignedToType>owner</assignedToType>
        <description>In less than three month the date of the Competitive Contract will arrive to its end. Please get in touch with the lead.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Competitive Contract arrives to the end</subject>
    </tasks>
    <tasks>
        <fullName>Follow_up_reminder_Internal</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your Lead/Contact has submitted a contact request on our website. Please review the Contact History and follow up accordingly.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow_up reminder (Internal)</subject>
    </tasks>
    <tasks>
        <fullName>Follow_up_reminder_Portal</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your Lead/Contact has submitted a contact request on our website. Please review the Contact History and follow up accordingly.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow_up reminder (Portal)</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Call_Lead</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Call</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Email_Lead</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Email</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Send_Letter_Lead</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Letter</subject>
    </tasks>
</Workflow>