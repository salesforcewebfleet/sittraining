<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_new_connect_Partner_has_achieved_Registered_dynamic</fullName>
        <description>A new .connect Partner has achieved Registered status - Dynamic Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email_2_DCCP_status_Registered__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email_DCCP_Status_Registered__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Registered_Partner_Notification</template>
    </alerts>
    <alerts>
        <fullName>A_new_connect_Partner_has_achieved_Registered_status</fullName>
        <description>A new .connect Partner has achieved Registered status</description>
        <protected>false</protected>
        <recipients>
            <recipient>noelia.grande@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stefano.millucci@tomtom.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Registered_Partner_Notification</template>
    </alerts>
    <alerts>
        <fullName>A_new_connect_Partner_has_achieved_START_status_Dynamic_Email</fullName>
        <description>A new .connect Partner has achieved START status - Dynamic Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email_2_DCCP_status_Start__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email_3_DCCP_status_Start__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email_4_DCCP_status_Start__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email_5_DCCP_status_Start__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email_DCCP_Status_Start__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email_Submitter__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_START_Partner_Approved</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_Competitive_Contract_arrives_to_the_end</fullName>
        <description>Email alert: Competitive Contract arrives to the end</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Competitive_Contracts_End_for_Accounts</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_Approver</fullName>
        <description>Email alert to Approver</description>
        <protected>false</protected>
        <recipients>
            <recipient>axel.peters@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/dcPP_Approval_process_approver_email</template>
    </alerts>
    <alerts>
        <fullName>connect_REGISTERED_partner_confirmation</fullName>
        <description>.connect REGISTERED partner confirmation</description>
        <protected>false</protected>
        <recipients>
            <recipient>michel.vanpetten@tomtom.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stefano.millucci@tomtom.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Registered_Partner_Notification</template>
    </alerts>
    <alerts>
        <fullName>connect_Registered_Notification_DACH</fullName>
        <ccEmails>Nicole.Schueller@tomtom.com</ccEmails>
        <description>.connect Registered Partner Notification DACH</description>
        <protected>false</protected>
        <recipients>
            <recipient>stefano.millucci@tomtom.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ulrike.glass@tomtom.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Registered_Partner_Notification</template>
    </alerts>
    <alerts>
        <fullName>connect_Registered_Partner</fullName>
        <description>.connect Registered Partner</description>
        <protected>false</protected>
        <recipients>
            <recipient>daniel.farragher@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Registered_Partner_Notification</template>
    </alerts>
    <alerts>
        <fullName>connect_Registered_Partner_Notification_France</fullName>
        <description>.connect Registered Partner Notification France</description>
        <protected>false</protected>
        <recipients>
            <recipient>eric.deschiens@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jerome.germain@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>julien.gernot@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stefano.millucci@tomtom.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Registered_Partner_Notification</template>
    </alerts>
    <alerts>
        <fullName>connect_Registered_Partner_Notification_South_Africa</fullName>
        <ccEmails>stefano.millucci@tomtom.com</ccEmails>
        <description>.connect Registered Partner Notification South Africa</description>
        <protected>false</protected>
        <recipients>
            <recipient>laurence.odendaal@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Registered_Partner_Notification</template>
    </alerts>
    <alerts>
        <fullName>connect_Registered_Partner_Notification_UK</fullName>
        <description>.connect_Registered_Partner_Notification_UK</description>
        <protected>false</protected>
        <recipients>
            <recipient>karolina.tymkiewicz@tomtom.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>linda.dean@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Registered_Partner_Notification</template>
    </alerts>
    <alerts>
        <fullName>connect_Registered_Partner_Notification_United_States</fullName>
        <description>.connect Registered Partner Notification United States</description>
        <protected>false</protected>
        <recipients>
            <recipient>sara.koks@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Registered_Partner_Notification</template>
    </alerts>
    <alerts>
        <fullName>connect_Registered_Partner_Notification_to_the_Sales_Assistant_Italy</fullName>
        <description>.connect Registered Partner Notification to the Sales Assistant Italy</description>
        <protected>false</protected>
        <recipients>
            <recipient>eleonora.scilanga@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stefano.millucci@tomtom.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Registered_Partner_Notification</template>
    </alerts>
    <alerts>
        <fullName>connect_Start_partner_Notification_API_Key_Request</fullName>
        <ccEmails>2ndlinesupport@telematics.tomtom.com</ccEmails>
        <description>connect Start partner Notification API Key Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>daniel.farragher@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/connect_Start_Partner_Notification_API_Key_Request</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Type_Prospect</fullName>
        <field>Type</field>
        <literalValue>Prospect</literalValue>
        <name>Account Type = Prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Account_Rectype_to_Customer</fullName>
        <description>Change account record type to Customer (when type picklist is changed to Customer)</description>
        <field>RecordTypeId</field>
        <lookupValue>Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Account Rectype to Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Account_Rectype_to_Reseller</fullName>
        <description>Change account record type to Reseller (when type picklist is changed to Reseller)</description>
        <field>RecordTypeId</field>
        <lookupValue>Reseller</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Account Rectype to Reseller</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DCCP_Status_to_Prospecting</fullName>
        <description>OTS-75953
Add Approval Flow to George de Boer on accounts</description>
        <field>DCPP_Status__c</field>
        <literalValue>Prospecting</literalValue>
        <name>DCCP Status to Prospecting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DCCP_Status_to_Reg</fullName>
        <description>OTS-75953
Add Approval Flow to George de Boer on accounts</description>
        <field>DCPP_Status__c</field>
        <literalValue>Registered</literalValue>
        <name>DCCP Status to Registered</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DCCP_Status_to_Registered</fullName>
        <description>OTS-75953
Add Approval Flow to George de Boer on accounts</description>
        <field>DCPP_Status__c</field>
        <literalValue>Registered</literalValue>
        <name>DCCP Status to Registered</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DCCP_Status_to_Start</fullName>
        <description>OTS-75953
Add Approval Flow to George de Boer on accounts</description>
        <field>DCPP_Status__c</field>
        <literalValue>Start</literalValue>
        <name>DCCP Status to Start</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DCPP_Status_to_Prospecting</fullName>
        <description>OTS-75953
Add Approval Flow to George de Boer on accounts
Update DCCP status back to previous status when rejected</description>
        <field>DCPP_Status__c</field>
        <literalValue>Prospecting</literalValue>
        <name>DCPP Status to Prospecting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Submitter</fullName>
        <description>OTS-75953
Add Approval Flow to George de Boer on accounts</description>
        <field>Email_Submitter__c</field>
        <formula>$User.Email</formula>
        <name>Email Submitter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inform_State</fullName>
        <description>Inform state depending on the Postal Code</description>
        <field>BillingState</field>
        <formula>CASE(LEFT( BillingPostalCode , 2), 
&apos;01&apos;, &apos;Álava&apos;, 
&apos;02&apos;, &apos;Albacete&apos;,
&apos;03&apos;, &apos;Alicante&apos;,
&apos;04&apos;, &apos;Almería&apos;,
&apos;05&apos;, &apos;Ávila&apos;,
&apos;06&apos;, &apos;Badajoz&apos;,
&apos;07&apos;, &apos;Illes Balears&apos;,
&apos;08&apos;, &apos;Barcelona&apos;,
&apos;09&apos;, &apos;Burgos&apos;,
&apos;10&apos;, &apos;Cáceres&apos;,
&apos;11&apos;, &apos;Cádiz&apos;,
&apos;12&apos;, &apos;Castellón&apos;,
&apos;13&apos;, &apos;Ciudad Real&apos;,
&apos;14&apos;, &apos;Córdoba&apos;,
&apos;15&apos;, &apos;Coruña&apos;,
&apos;16&apos;, &apos;Cuenca&apos;,
&apos;17&apos;, &apos;Girona&apos;,
&apos;18&apos;, &apos;Granada&apos;,
&apos;19&apos;, &apos;Guadalajara&apos;,
&apos;20&apos;, &apos;Guipuzkoa&apos;,
&apos;21&apos;, &apos;Huelva&apos;,
&apos;22&apos;, &apos;Huesca&apos;,
&apos;23&apos;, &apos;Jaén&apos;,
&apos;24&apos;, &apos;León&apos;,
&apos;25&apos;, &apos;Lleida&apos;,
&apos;26&apos;, &apos;La Rioja&apos;,
&apos;27&apos;, &apos;Lugo&apos;,
&apos;28&apos;, &apos;Madrid&apos;,
&apos;29&apos;, &apos;Málaga&apos;,
&apos;30&apos;, &apos;Murcia&apos;,
&apos;31&apos;, &apos;Navarra&apos;,
&apos;32&apos;, &apos;Ourense&apos;,
&apos;32&apos;, &apos;Asturias&apos;,
&apos;34&apos;, &apos;Palencia&apos;,
&apos;35&apos;, &apos;Las Palmas&apos;,
&apos;36&apos;, &apos;Pontevedra&apos;,
&apos;37&apos;, &apos;Salamanca&apos;,
&apos;38&apos;, &apos;S.C. Tenerife&apos;,
&apos;39&apos;, &apos;Cantabria&apos;,
&apos;40&apos;, &apos;Segovia&apos;,
&apos;41&apos;, &apos;Sevilla&apos;,
&apos;42&apos;, &apos;Soria&apos;,
&apos;43&apos;, &apos;Tarragona&apos;,
&apos;44&apos;, &apos;Teruel&apos;,
&apos;45&apos;, &apos;Toledo&apos;,
&apos;46&apos;, &apos;Valencia&apos;,
&apos;47&apos;, &apos;Valladolid&apos;,
&apos;48&apos;, &apos;Bizkaia&apos;,
&apos;49&apos;, &apos;Zamora&apos;,
&apos;50&apos;, &apos;Zaragoza&apos;,
&apos;51&apos;, &apos;Ceuta&apos;,
&apos;52&apos;, &apos;Melilla&apos;,

&apos;Error&apos;)</formula>
        <name>Inform State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_type_update_to_competitor</fullName>
        <description>Record type update to competitor</description>
        <field>RecordTypeId</field>
        <lookupValue>Competitor</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record type update to competitor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_type_update_to_connect_partner</fullName>
        <description>Record type update to .connect partner</description>
        <field>RecordTypeId</field>
        <lookupValue>connect_Partner</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record type update to .connect partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Type</fullName>
        <description>Update Account Type to .connect Partner</description>
        <field>Type</field>
        <literalValue>.connect Partner</literalValue>
        <name>Update Account Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Import_Duplicate_Check</fullName>
        <field>Import_Duplicate_Check__c</field>
        <formula>Account_Duplicate_Check__c</formula>
        <name>Update Import Duplicate Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>wf_c_software_partner_update</fullName>
        <field>wf_c_Software_Partner__c</field>
        <literalValue>1</literalValue>
        <name>wf.c software partner update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>wf_c_tick_box_update</fullName>
        <description>wf.c integrator tick box update</description>
        <field>wf_c_Integrator__c</field>
        <literalValue>1</literalValue>
        <name>wf.c integrator tick box update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>TO_ELOQUA_ACCOUNTS_V1</fullName>
        <apiVersion>42.0</apiVersion>
        <description>Send Accounts to Eloqua via ICS</description>
        <endpointUrl>https://tttics-a485859.integration.em2.oraclecloud.com/integration/flowsvc/salesforce/PROD_SALESF_TO_ELOQUA_ACCOUN/v01/</endpointUrl>
        <fields>Accepted_API_SDK_license__c</fields>
        <fields>Accepted_CoCar_WF_Connect_API_license__c</fields>
        <fields>Accepted_MDM_Agreement__c</fields>
        <fields>Accepted_NDA__c</fields>
        <fields>Accepted_Partner_Agreement_dcPP__c</fields>
        <fields>AccountNumber</fields>
        <fields>AccountSource</fields>
        <fields>Account_Duplicate_Check__c</fields>
        <fields>Account_ID_18__c</fields>
        <fields>Account_Owner_Active__c</fields>
        <fields>AnnualRevenue</fields>
        <fields>App_API_key__c</fields>
        <fields>Average_Costs_per_Day__c</fields>
        <fields>BillingCity</fields>
        <fields>BillingCountry</fields>
        <fields>BillingGeocodeAccuracy</fields>
        <fields>BillingLatitude</fields>
        <fields>BillingLongitude</fields>
        <fields>BillingPostalCode</fields>
        <fields>BillingState</fields>
        <fields>BillingStreet</fields>
        <fields>BizDev_Project_Type__c</fields>
        <fields>CANbus__c</fields>
        <fields>CSPP_Status__c</fields>
        <fields>Cars_Fleet_Purpose__c</fields>
        <fields>Cars_Fleet_Size__c</fields>
        <fields>Churned_date__c</fields>
        <fields>Commercial_Registration__c</fields>
        <fields>Company_Registration_Number__c</fields>
        <fields>Competitive_Contract_End_Date__c</fields>
        <fields>Competitor__c</fields>
        <fields>ConnectionReceivedId</fields>
        <fields>ConnectionSentId</fields>
        <fields>CreatedById</fields>
        <fields>CreatedDate</fields>
        <fields>CurrencyIsoCode</fields>
        <fields>Customer_Approach__c</fields>
        <fields>DCPP_Status__c</fields>
        <fields>DMU__c</fields>
        <fields>Data_Quality_Description__c</fields>
        <fields>Data_Quality_Score__c</fields>
        <fields>Date_Price_Information__c</fields>
        <fields>Description</fields>
        <fields>Digital_Tacho__c</fields>
        <fields>Distributor__c</fields>
        <fields>Driver_ID__c</fields>
        <fields>Driving_Behavior__c</fields>
        <fields>Duplicate_id__c</fields>
        <fields>Email_2_DCCP_status_Registered__c</fields>
        <fields>Email_2_DCCP_status_Start__c</fields>
        <fields>Email_3_DCCP_status_Start__c</fields>
        <fields>Email_4_DCCP_status_Start__c</fields>
        <fields>Email_5_DCCP_status_Start__c</fields>
        <fields>Email_DCCP_Status_Registered__c</fields>
        <fields>Email_DCCP_Status_Start__c</fields>
        <fields>Email_Submitter__c</fields>
        <fields>End_Of_Lease__c</fields>
        <fields>External_Account_Id__c</fields>
        <fields>External_Guid__c</fields>
        <fields>Fax</fields>
        <fields>Fleet_Contact__c</fields>
        <fields>GPRS_Costs_per_month__c</fields>
        <fields>HGV_Fleet_Purpose__c</fields>
        <fields>HGV_Fleet_Size__c</fields>
        <fields>Hardware_initial_costs__c</fields>
        <fields>Id</fields>
        <fields>Import_Duplicate_Check__c</fields>
        <fields>Independent_Hardware_Vendor__c</fields>
        <fields>Industry</fields>
        <fields>Industry_Code__c</fields>
        <fields>Installation_Tariff__c</fields>
        <fields>Insurance_Provider__c</fields>
        <fields>Integration_Possibilities__c</fields>
        <fields>International_Account__c</fields>
        <fields>IsDeleted</fields>
        <fields>IsPartner</fields>
        <fields>Jigsaw</fields>
        <fields>JigsawCompanyId</fields>
        <fields>Key_Account__c</fields>
        <fields>LCV_Fleet_Purpose__c</fields>
        <fields>LCV_Fleet_Size__c</fields>
        <fields>LID__LinkedIn_Company_Id__c</fields>
        <fields>LastActivityDate</fields>
        <fields>LastModifiedById</fields>
        <fields>LastModifiedDate</fields>
        <fields>LastReferencedDate</fields>
        <fields>LastViewedDate</fields>
        <fields>Leasing_Provider__c</fields>
        <fields>Management_insight__c</fields>
        <fields>MasterRecordId</fields>
        <fields>Master_Agreement__c</fields>
        <fields>Minimum_contract_term_in_months__c</fields>
        <fields>Name</fields>
        <fields>Navigation__c</fields>
        <fields>No_of_Open_Opportunities__c</fields>
        <fields>NumberOfEmployees</fields>
        <fields>OEM__c</fields>
        <fields>Office_Software_PL__c</fields>
        <fields>Office_Software__c</fields>
        <fields>Order_Communication__c</fields>
        <fields>OwnerId</fields>
        <fields>Owner_Link__c</fields>
        <fields>Owner_Type__c</fields>
        <fields>Ownership</fields>
        <fields>ParentId</fields>
        <fields>Parent_Account_Clone__c</fields>
        <fields>Partner_Manager_Alias__c</fields>
        <fields>Phone</fields>
        <fields>PhotoUrl</fields>
        <fields>Primary_Fleet_Purpose__c</fields>
        <fields>Primary_Fleet_Size__c</fields>
        <fields>Primary_Vehicle_Type__c</fields>
        <fields>Provider_Name__c</fields>
        <fields>Rating</fields>
        <fields>RecordTypeId</fields>
        <fields>Region__c</fields>
        <fields>Reseller_Account_ID__c</fields>
        <fields>Reseller_ID__c</fields>
        <fields>Reseller_Lead_Email_Address__c</fields>
        <fields>Reseller_Name__c</fields>
        <fields>Reseller_U_RID__c</fields>
        <fields>Reseller__c</fields>
        <fields>SIM__c</fields>
        <fields>Secondary_Fleet_Purpose__c</fields>
        <fields>Secondary_Fleet_Size__c</fields>
        <fields>Secondary_Vehicle_Type__c</fields>
        <fields>ShippingCity</fields>
        <fields>ShippingCountry</fields>
        <fields>ShippingGeocodeAccuracy</fields>
        <fields>ShippingLatitude</fields>
        <fields>ShippingLongitude</fields>
        <fields>ShippingPostalCode</fields>
        <fields>ShippingState</fields>
        <fields>ShippingStreet</fields>
        <fields>Sic</fields>
        <fields>SicDesc</fields>
        <fields>Site</fields>
        <fields>Solution_Provider__c</fields>
        <fields>Solution_level__c</fields>
        <fields>Status__c</fields>
        <fields>Subscription_per_month__c</fields>
        <fields>SystemModstamp</fields>
        <fields>TCO_after_5_years__c</fields>
        <fields>TickerSymbol</fields>
        <fields>Total_Fleet_Size__c</fields>
        <fields>Total_Initial_Costs__c</fields>
        <fields>Totally_monthly_costs__c</fields>
        <fields>Tracking_Tracing__c</fields>
        <fields>Traffic_Information__c</fields>
        <fields>Truck_Navigation__c</fields>
        <fields>Type</fields>
        <fields>U_CID__c</fields>
        <fields>U_RID__c</fields>
        <fields>VAT_ID__c</fields>
        <fields>Vehicle_Data_Access__c</fields>
        <fields>WEBFLEET_Churned_Customer__c</fields>
        <fields>WEBFLEET_Customer_Number__c</fields>
        <fields>WEBFLEET_Terminated_Units__c</fields>
        <fields>WEBFLEET_Units_Active__c</fields>
        <fields>WEBFLEET_Units__c</fields>
        <fields>WF_Fleetcontact_email__c</fields>
        <fields>Website</fields>
        <fields>Worktime_Logbook__c</fields>
        <fields>pw_cc__BillingAddressStatus__c</fields>
        <fields>pw_cc__BillingCountryLookup__c</fields>
        <fields>pw_cc__BillingStateLookup__c</fields>
        <fields>pw_cc__BillingZipCodeLookup__c</fields>
        <fields>pw_cc__ShippingAddressStatus__c</fields>
        <fields>pw_cc__ShippingCountryLookup__c</fields>
        <fields>pw_cc__ShippingStateLookup__c</fields>
        <fields>pw_cc__ShippingZipCodeLookup__c</fields>
        <fields>qbdialer__CloseDate__c</fields>
        <fields>qbdialer__CloseScore__c</fields>
        <fields>qbdialer__Dials__c</fields>
        <fields>qbdialer__LastCallTime__c</fields>
        <fields>qbdialer__Related_Contact_Dials__c</fields>
        <fields>qbdialer__Related_Contact_LastCallTime__c</fields>
        <fields>qbdialer__ResponseTime__c</fields>
        <fields>wf_c_Integrator__c</fields>
        <fields>wf_c_Software_Partner__c</fields>
        <fields>zaapit__Hierarchy_Level__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>tomtom_support@engagementfactory.com</integrationUser>
        <name>TO_ELOQUA_ACCOUNTS_V1</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Account Changed To Customer</fullName>
        <actions>
            <name>Change_Account_Rectype_to_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Customer</value>
        </criteriaItems>
        <description>If the type picklist is changed to Customer, the record type will automatically change to &quot;Customer&quot; (most likely from &quot;Other&quot;) and this new record type makes some fields read only.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account Changed To Reseller</fullName>
        <actions>
            <name>Change_Account_Rectype_to_Reseller</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Reseller</value>
        </criteriaItems>
        <description>If the type picklist is changed to Reseller, the record type will automatically change to &quot;Reseller&quot; (most likely from &quot;Other&quot;) and this new record type uses a different page layout for Resellers</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account change to %2Econnect Partner</fullName>
        <actions>
            <name>Record_type_update_to_connect_partner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>.connect Partner</value>
        </criteriaItems>
        <description>If the type picklist is changed to .connect Partner, the record type will automatically change to &quot;.connect Partner&quot;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account change to Competitor</fullName>
        <actions>
            <name>Record_type_update_to_competitor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Competitor</value>
        </criteriaItems>
        <description>If the type picklist is changed to Competitor, the record type will automatically change to &quot;Competitor&quot; (most likely from &quot;Other&quot;).</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Competitive Contract Ends</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Competitive_Contract_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Customer</value>
        </criteriaItems>
        <description>Reminder generated 3 month before the Competitive Contract End Date arrive to the end.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_alert_Competitive_Contract_arrives_to_the_end</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Competitive_Contract_arrives_to_the_end</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.Competitive_Contract_End_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Inform State</fullName>
        <actions>
            <name>Inform_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Inform State only for Spain when the Postal code exist</description>
        <formula>(BillingCountry=&apos;Spain&apos;  || BillingCountry=&apos;ES&apos; ) &amp;&amp;   LEN( BillingPostalCode  ) = 5</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set account%2Etype to prospect</fullName>
        <actions>
            <name>Account_Type_Prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the account type to prospect if the account type is empty upon creation</description>
        <formula>Ispickval(Type, &quot;&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Software Partner update</fullName>
        <actions>
            <name>wf_c_software_partner_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Software Partner</value>
        </criteriaItems>
        <description>If an Account of type Software partner is created, the Software Partner tick box will be filled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>System Integrator</fullName>
        <actions>
            <name>wf_c_tick_box_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>System Integrator</value>
        </criteriaItems>
        <description>If account type Integrator is selected, automatically the wf.c tick box will be filled.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Account to Eloqua</fullName>
        <actions>
            <name>TO_ELOQUA_ACCOUNTS_V1</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>eloqua</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Import Duplicate Check Field</fullName>
        <actions>
            <name>Update_Import_Duplicate_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Saves the import duplicate check formula field into a field which is marked &quot;external ID&quot;, so that it can be used or importing if no Webfleet Customer ID is present.</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>connect Registered Partner BNLX%2C Nordics</fullName>
        <actions>
            <name>connect_REGISTERED_partner_confirmation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2) or (1 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Account.DCPP_Status__c</field>
            <operation>equals</operation>
            <value>Registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>Benelux,Nordics</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>Richard Hoomans</value>
        </criteriaItems>
        <description>Sends an email notification to the Sales Assistant whenever the .connect Partner status = Registered
VR: workflow rule has been disabled for  &apos;OTS-75953
Add Approval Flow to Partner Manager on accounts&apos; Email is now sent as a result of approval action.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>connect Registered Partner DACH</fullName>
        <actions>
            <name>connect_Registered_Notification_DACH</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.DCPP_Status__c</field>
            <operation>equals</operation>
            <value>Registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>DACH</value>
        </criteriaItems>
        <description>Sends an email notification to the Sales Assistant whenever the .connect Partner status = Registered
VR: workflow rule has been disabled for  &apos;OTS-75953
Add Approval Flow to Partner Manager on accounts&apos; Email is now sent as a result of approval action.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>connect Registered Partner France</fullName>
        <actions>
            <name>connect_Registered_Partner_Notification_France</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.DCPP_Status__c</field>
            <operation>equals</operation>
            <value>Registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>France</value>
        </criteriaItems>
        <description>Sends an email notification to the Sales Assistant whenever the .connect Partner status = Registered
VR: workflow rule has been disabled for  &apos;OTS-75953
Add Approval Flow to Partner Manager on accounts&apos; Email is now sent as a result of approval action.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>connect Registered Partner Iberia</fullName>
        <actions>
            <name>A_new_connect_Partner_has_achieved_Registered_status</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.DCPP_Status__c</field>
            <operation>equals</operation>
            <value>Registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>Iberia</value>
        </criteriaItems>
        <description>Sends an email notification to the Sales Assistant whenever the .connect Partner status = Registered
VR: workflow rule has been disabled for  &apos;OTS-75953
Add Approval Flow to Partner Manager on accounts&apos; Email is now sent as a result of approval action.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>connect Registered Partner Italy</fullName>
        <actions>
            <name>connect_Registered_Partner_Notification_to_the_Sales_Assistant_Italy</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.DCPP_Status__c</field>
            <operation>equals</operation>
            <value>Registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>Italy</value>
        </criteriaItems>
        <description>Sends an email notification to the Sales Assistant whenever the .connect Partner status = Registered
VR: workflow rule has been disabled for  &apos;OTS-75953
Add Approval Flow to Partner Manager on accounts&apos; Email is now sent as a result of approval action.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>connect Registered Partner South Africa</fullName>
        <actions>
            <name>connect_Registered_Partner_Notification_South_Africa</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.DCPP_Status__c</field>
            <operation>equals</operation>
            <value>Registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>South Africa</value>
        </criteriaItems>
        <description>Sends an email notification to the Sales Assistant whenever the .connect Partner status = Registered
VR: workflow rule has been disabled for  &apos;OTS-75953
Add Approval Flow to Partner Manager on accounts&apos; Email is now sent as a result of approval action.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>connect Registered Partner UKI</fullName>
        <actions>
            <name>connect_Registered_Partner_Notification_UK</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.DCPP_Status__c</field>
            <operation>equals</operation>
            <value>Registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>UK</value>
        </criteriaItems>
        <description>Sends an email notification to the Sales Assistant whenever the .connect Partner status = Registered
VR: workflow rule has been disabled for  &apos;OTS-75953
Add Approval Flow to Partner Manager on accounts&apos; Email is now sent as a result of approval action.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>connect Registered Partner United States</fullName>
        <actions>
            <name>connect_Registered_Partner_Notification_United_States</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.DCPP_Status__c</field>
            <operation>equals</operation>
            <value>Registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>Richard Hoomans</value>
        </criteriaItems>
        <description>Sends an email notification to the Sales Assistant whenever the .connect Partner status = Registered
VR: workflow rule has been disabled for  &apos;OTS-75953
Add Approval Flow to Partner Manager on accounts&apos; Email is now sent as a result of approval action.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>connect Start Partner API Key Request</fullName>
        <actions>
            <name>connect_Start_partner_Notification_API_Key_Request</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.DCPP_Status__c</field>
            <operation>equals</operation>
            <value>Start</value>
        </criteriaItems>
        <description>Sends an email notification whenever the .connect Partner status = Start
Rule desactivated as this email will not be triggered from the approval process (whenever status &apos;Start&apos; is approved (OTS-75953
Add Approval Flow to Partner Manager on accounts).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Call  - Account %28Exmaple by ZaapIT%29</fullName>
        <active>false</active>
        <description>Call Account - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Call %28Exmaple by ZaapIT%29</fullName>
        <active>false</active>
        <description>Call Account Example by ZaapIT (don&apos;t activated - an example)</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Email - Account %28Exmaple by ZaapIT%29</fullName>
        <actions>
            <name>zaapit__Email</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Email Account - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Letter  - Account %28Exmaple by ZaapIT%29</fullName>
        <actions>
            <name>zaapit__SendLetterAccount</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Send Letter Account - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Quote - Account %28Exmaple by ZaapIT%29</fullName>
        <actions>
            <name>zaapit__SendQuoteAccount</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Send Quote Account - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Competitive_Contract_arrives_to_the_end</fullName>
        <assignedToType>owner</assignedToType>
        <description>In less than three month the date of the Competitive Contract will arrive to its end. Please get in touch with the main contact of this account.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Competitive Contract arrives to the end</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Call</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Email</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__SendLetterAccount</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Letter</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__SendQuoteAccount</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Quote</subject>
    </tasks>
</Workflow>