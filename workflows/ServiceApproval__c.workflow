<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ApprovedEmailAlert</fullName>
        <description>Approved Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_process/ServiceApprovalApproved2</template>
    </alerts>
    <alerts>
        <fullName>RejectedEmailAlert</fullName>
        <description>Rejected Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_process/ServiceApprovalRejected2</template>
    </alerts>
    <alerts>
        <fullName>SubmittedEmailAlert</fullName>
        <description>Submitted Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_process/ServiceApprovalSubmitted2</template>
    </alerts>
    <fieldUpdates>
        <fullName>StatusApproved</fullName>
        <description>When an Approval is Approved, the Approval Status field is updated to Approved.</description>
        <field>ApprovalStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusPending</fullName>
        <description>When an Approval is first submitted, the Approval Status field is updated to Pending.</description>
        <field>ApprovalStatus__c</field>
        <literalValue>Pending</literalValue>
        <name>Status Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusRecalled</fullName>
        <description>When an Approval is Recalled, the Approval Status field is updated to Recalled.</description>
        <field>ApprovalStatus__c</field>
        <literalValue>Recalled</literalValue>
        <name>Status Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusRejected</fullName>
        <description>When an Approval is Rejected, the Approval Status field is updated to Rejected.</description>
        <field>ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>