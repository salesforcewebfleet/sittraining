<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_region</fullName>
        <description>Update the field &quot;Region 2&quot; in the user page when the user is created</description>
        <field>Region_2__c</field>
        <formula>IF(TEXT(UserType) = &quot;Standard&quot;, UserRole.RollupDescription, Manager.UserRole.RollupDescription)</formula>
        <name>Update region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Inform Region</fullName>
        <actions>
            <name>Update_region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Inform the  Region 2 field when a user is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>