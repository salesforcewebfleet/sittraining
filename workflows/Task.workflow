<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Type_field_to_Email</fullName>
        <description>Field Update Edit Updates Task &apos;Type&apos; field to &apos;Email&apos;</description>
        <field>Type</field>
        <literalValue>Email</literalValue>
        <name>Update Type field to Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Callaction</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.CallType</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Type to Email for Emails</fullName>
        <actions>
            <name>Update_Type_field_to_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Email:</value>
        </criteriaItems>
        <description>Rules Changes Type to Email for automatically added Emails</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updates on tasks assigned</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>contains</operation>
            <value>Terri,Audrie</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Task.CreatedDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>