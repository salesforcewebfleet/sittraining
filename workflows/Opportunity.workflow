<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_Special_Price_Opportunity_Stage_Closed</fullName>
        <description>Email Alert: Special Price Opportunity Stage &apos;Closed&apos;</description>
        <protected>false</protected>
        <recipients>
            <recipient>adam.yu@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/SpecPrice_Opp_Closed</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_when_a_new_opportunity_form_a_UKI_reseller_has_been_created</fullName>
        <description>Email notification when a new opportunity form a UKI reseller has been created</description>
        <protected>false</protected>
        <recipients>
            <field>Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Opty_create_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_manager_of_opportunity_stage_change</fullName>
        <description>Notify manager of opportunity stage change</description>
        <protected>false</protected>
        <recipients>
            <field>Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Opty_Negotiation_Closure_notification</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Proposal_sent</fullName>
        <description>Opportunity Proposal sent</description>
        <protected>false</protected>
        <recipients>
            <recipient>matt.gunzenhaeuser@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Workflow_Templates/Opportunity_proposal_sent</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_has_been_approved</fullName>
        <description>Opportunity has been approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_process/Approved_request_2</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_in_negotiation</fullName>
        <description>Opportunity: in negotiation</description>
        <protected>false</protected>
        <recipients>
            <recipient>matt.gunzenhaeuser@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Opportunity_negotiation</template>
    </alerts>
    <alerts>
        <fullName>SASU_email_with_opp_details</fullName>
        <description>SASU email with opp details</description>
        <protected>false</protected>
        <recipients>
            <field>SASU_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_process/Approved_request_SASU_2</template>
    </alerts>
    <alerts>
        <fullName>The_opportunity_has_been_rejected</fullName>
        <description>The opportunity has been rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_process/Rejected_approval_2</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_Details_in_Approval_History</fullName>
        <field>Step_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Add Details in Approval History</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Industry</fullName>
        <description>Industry is mandatory when creating an Opportunity on a Prospect account</description>
        <field>Industry</field>
        <name>Industry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Approval</fullName>
        <field>Requested_Discount_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status= Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_No_value</fullName>
        <field>Requested_Discount_Status__c</field>
        <name>Status= No value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Pending</fullName>
        <field>Requested_Discount_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Status= Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Rejected</fullName>
        <field>Requested_Discount_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status= Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_History_Flag</fullName>
        <field>Step_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Update Approval History Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managers_Email_Opty</fullName>
        <description>Updates the Managers Email address so it can be used by other workflows</description>
        <field>Managers_Email__c</field>
        <formula>Owner_Link__r.Manager.Email</formula>
        <name>Update Managers Email (Opty)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SASU_email</fullName>
        <description>Update the Customer email field depending on the Opportunity region</description>
        <field>SASU_Email__c</field>
        <formula>CASE(  Account.BillingCountry , 
&apos;Austria&apos;, &apos;Sales.at@webfleet.com&apos;,
&apos;AT&apos;, &apos;Sales.at@webfleet.com&apos;, 
&apos;Switzerland&apos;, &apos;Sales.se@webfleet.com&apos;,
&apos;SE&apos;, &apos;Sales.se@webfleet.com&apos;,
&apos;Chile&apos;, &apos;Sales.ch@webfleet.com&apos;,
&apos;CH&apos;, &apos;Sales.ch@webfleet.com&apos;,
&apos;Czech Republic&apos;, &apos;Sales.cz@webfleet.com&apos;,
&apos;CZ&apos;, &apos;Sales.cz@webfleet.com&apos;,
&apos;Germany&apos;, &apos;Sales.de@webfleet.com&apos;,
&apos;DE&apos;, &apos;Sales.de@webfleet.com&apos;,
&apos;Poland&apos;, &apos;Sales.pl@webfleet.com&apos;,
&apos;PL&apos;, &apos;Sales.pl@webfleet.com&apos;,
&apos;Belgium&apos;, &apos;Sales.be@webfleet.com&apos;,
&apos;BE&apos;, &apos;Sales.be@webfleet.com&apos;,
&apos;Netherlands&apos;, &apos;Sales.nl@webfleet.com&apos;,
&apos;NL&apos;, &apos;Sales.nl@webfleet.com&apos;,
&apos;France&apos;, &apos;Sales.fr@webfleet.com&apos;,
&apos;FR&apos;, &apos;Sales.fr@webfleet.com&apos;,
&apos;Spain&apos;, &apos;Sales.es@webfleet.com&apos;,
&apos;ES&apos;, &apos;Sales.es@webfleet.com&apos;,
&apos;Italy&apos;, &apos;Sales.it@webfleet.com&apos;,
&apos;IT&apos;, &apos;Sales.it@webfleet.com&apos;,
&apos;Denmark&apos;, &apos;Sales.dk@webfleet.com&apos;,
&apos;DK&apos;, &apos;Sales.dk@webfleet.com&apos;,
&apos;Portugal&apos;, &apos;Sales.pt@webfleet.com&apos;,
&apos;PT&apos;, &apos;Sales.pt@webfleet.com&apos;,
&apos;Sweden&apos;, &apos;Sales.se@webfleet.com&apos;,
&apos;SE&apos;, &apos;Sales.se@webfleet.com&apos;,
&apos;United Kingdom&apos;, &apos;Sales.uk@webfleet.com&apos;,
&apos;UK&apos;, &apos;Sales.uk@webfleet.com&apos;,
&apos;Ireland&apos;, &apos;Sales.ie@webfleet.com&apos;,
&apos;IE&apos;, &apos;Sales.ie@webfleet.com&apos;,
&apos;South Africa&apos;, &apos;Sales.za@webfleet.com&apos;,
&apos;ZA&apos;, &apos;Sales.za@webfleet.com&apos;,


&apos;Sales.int@webfleet.com&apos;)</formula>
        <name>Update SASU email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>90 days Follow up task on Closed%2FWon</fullName>
        <actions>
            <name>Customer_Care_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>This rule is created to make sure Inside Sales will make a follow up call 90 days after the opportunity is set to Closed / won</description>
        <formula>ISPICKVAL(StageName, &apos;Closed Won&apos;) 
&amp;&amp;
  Owner.ProfileId   = (&apos;00e14000001duju&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Add Opps Created by CRM to Campaign</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Create_by_Role__c</field>
            <operation>equals</operation>
            <value>CRM Team</value>
        </criteriaItems>
        <description>Adds all opportunities created by the CRM team into the CRM Opportunities Campaign</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Managers Email Address %28Opty%29</fullName>
        <actions>
            <name>Update_Managers_Email_Opty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Inserts the owner&apos;s manager&apos;s email address into a special email field so it can be used by other workflows</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Follow-up close lost opp</fullName>
        <actions>
            <name>Follow_up_reminder_Opportunity_lost_12_months_ago</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Create a remainder task 11 month after an opportunity is close lost</description>
        <formula>ISPICKVAL( StageName , &apos;Closed Lost&apos;) &amp;&amp; NOT(Owner.UserRole.Id  = &apos;00E1O000002gVsR&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity in Demo %26 Propose %28US%29</fullName>
        <actions>
            <name>Opportunity_Proposal_sent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Demo &amp; Propose</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>This workflow notifies management that a US based opportunity is in Demo &amp; Propose stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity in negotiation %28US%29</fullName>
        <actions>
            <name>Opportunity_in_negotiation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>This workflow notifies management that a US based opportunity is in Closing  stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opty created by UKI Reseller</fullName>
        <actions>
            <name>Email_notification_when_a_new_opportunity_form_a_UKI_reseller_has_been_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>United Kingdom,Ireland</value>
        </criteriaItems>
        <description>When an opportunity is created by an UKI reseller, an email notification will be sent to the manager of the owner (manager is specified in the owner&apos;s user record).</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opty reached Closed Lost stage</fullName>
        <actions>
            <name>Notify_manager_of_opportunity_stage_change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>Whenever an opportunity changes stage to Closed Lost, an email notification will be sent to the manager of the owner (manager is specified in the owner&apos;s user record).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opty reached Closed Won stage</fullName>
        <actions>
            <name>Notify_manager_of_opportunity_stage_change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Whenever an opportunity changes stage to Closed Won, an email notification will be sent to the manager of the owner (manager is specified in the owner&apos;s user record).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opty reached Closing stage</fullName>
        <actions>
            <name>Notify_manager_of_opportunity_stage_change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closing</value>
        </criteriaItems>
        <description>Whenever an opportunity changes stage to any of these: Closing an email notification will be sent to the manager of the owner (manager is specified in the owner&apos;s user record).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Special Price Opportunity Closed</fullName>
        <actions>
            <name>Email_Alert_Special_Price_Opportunity_Stage_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Special_price__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>Email Alert to Sales Project Coordinator when a Special Price Opportunity changes to status Closed (both Lost and Won)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Call Opportunity</fullName>
        <actions>
            <name>zaapit__Call_oppts</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Call Opportunity - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Email Opportunity</fullName>
        <actions>
            <name>zaapit__Email_oppts</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Email Opportunity - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Letter Opportunity</fullName>
        <actions>
            <name>zaapit__Send_Letter_oppts</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Send Letter Opportunity - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Quote Opportunity</fullName>
        <actions>
            <name>zaapit__Send_Quote_oppts</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Send Quote Opportunity - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Customer_Care_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please call back customer 3 months after the opp has been changed to closed/won</description>
        <dueDateOffset>90</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Customer Care Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Follow_up_reminder_Opportunity_lost_12_months_ago</fullName>
        <assignedToType>owner</assignedToType>
        <description>This opportunity was lost 12 months ago, please review this account and reach out to your contact if applicable</description>
        <dueDateOffset>365</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow up reminder Opportunity lost 12 months ago</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Call_oppts</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Call</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Email_oppts</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Email</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Send_Letter_oppts</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Letter</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Send_Quote_oppts</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Quote</subject>
    </tasks>
</Workflow>