<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Activity_Type</fullName>
        <field>Activity_Types__c</field>
        <formula>Text(Type)</formula>
        <name>Update Activity Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Activity Type</fullName>
        <actions>
            <name>Update_Activity_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the hidden activity type field, so that it can be used for reporting on the custom report type.</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>