<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contact_Updates_by_Act_On_Internal</fullName>
        <description>Contact Updates by Act-On (Internal - No FR)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Contact_Update_Eloqua_Internal</template>
    </alerts>
    <alerts>
        <fullName>Contact_Updates_by_Act_On_Internal_FR</fullName>
        <description>Contact Updates by Act-On (Internal FR)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>celine.provinitaze-bernard@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Contact_Update_Eloqua_Internal</template>
    </alerts>
    <alerts>
        <fullName>Contact_Updates_by_Act_On_Partner</fullName>
        <description>Contact Updates by Act-On (Partner - No FR)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>sara.koks@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Contact_Update_Eloqua_Partner</template>
    </alerts>
    <alerts>
        <fullName>Contact_Updates_by_Act_On_Partner_FR</fullName>
        <description>Contact Updates by Act-On (Partner FR)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>celine.provinitaze-bernard@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.koks@webfleet.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Contact_Update_Eloqua_Partner</template>
    </alerts>
    <alerts>
        <fullName>Eloqua_Existing_Record_Not_Updated_Contact</fullName>
        <ccEmails>matthew.wise@webfleet.com</ccEmails>
        <ccEmails>stephan.wiebigke@webfleet.com</ccEmails>
        <ccEmails>axel.peters@webfleet.com</ccEmails>
        <description>Eloqua Existing Record Not Updated Contact</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Eloqua_Existing_Record_Not_Updated</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_Austria</fullName>
        <ccEmails>Crm.at@telematics.tomtom.com</ccEmails>
        <description>Email to OSC Austria</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_Belgium</fullName>
        <ccEmails>Crm.be@telematics.tomtom.com</ccEmails>
        <description>Email to OSC Belgium</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_France</fullName>
        <ccEmails>Crm.fr@telematics.tomtom.com</ccEmails>
        <description>Email to OSC France</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_Germany</fullName>
        <ccEmails>Crm.de@telematics.tomtom.com</ccEmails>
        <description>Email to OSC Germany</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_Ireland</fullName>
        <ccEmails>Crm.ie@telematics.tomtom.com</ccEmails>
        <description>Email to OSC Ireland</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_Italy</fullName>
        <ccEmails>Crm.it@telematics.tomtom.com</ccEmails>
        <description>Email to OSC Italy</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_Netherlands</fullName>
        <ccEmails>Crm.nl@telematics.tomtom.com</ccEmails>
        <description>Email to OSC Netherlands</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_Portugal</fullName>
        <ccEmails>Crm.pt@telematics.tomtom.com</ccEmails>
        <description>Email to OSC Portugal</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_Spain</fullName>
        <ccEmails>Crm.es@telematics.tomtom.com</ccEmails>
        <description>Email to OSC Spain</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_Switzerland</fullName>
        <ccEmails>Crm.ch@telematics.tomtom.com</ccEmails>
        <description>Email to OSC Switzerland</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Email_to_OSC_UK</fullName>
        <ccEmails>Crm.uk@telematics.tomtom.com</ccEmails>
        <description>Email to OSC UK</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Follow_up_in_OSC</template>
    </alerts>
    <alerts>
        <fullName>Notify</fullName>
        <description>Notify</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Lead_Update_Act_On</template>
    </alerts>
    <fieldUpdates>
        <fullName>BizDev_Lead_or_Contact</fullName>
        <description>the field BizDev Lead or Contact will be ticked when the current user&apos;s role is Business Development upon Lead or Contact creation</description>
        <field>BizDev_Lead_or_Contact__c</field>
        <literalValue>1</literalValue>
        <name>BizDev Lead or Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campaign_Opt_In_vs_Campaign_Opt_Out</fullName>
        <field>Campaign_Opt_out__c</field>
        <literalValue>0</literalValue>
        <name>Campaign Opt-In vs Campaign Opt-Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campaign_Opt_Out_vs_Campaign_Opt_In</fullName>
        <field>Campaign_Opt_in__c</field>
        <literalValue>0</literalValue>
        <name>Campaign Opt-Out vs Campaign Opt-In</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Contact_Description</fullName>
        <description>Copies the &apos;original&apos; Lead Description to the Original Description field</description>
        <field>Original_Description__c</field>
        <formula>Description</formula>
        <name>Copy Contact Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_mail_Opt_Out_vs_Opt_In</fullName>
        <description>If Email Opt Out is selected, then Email Opt-In will be de-selected</description>
        <field>Email_Opt_In__c</field>
        <literalValue>0</literalValue>
        <name>E-mail Opt-Out vs Opt-In</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FR_Opt_in</fullName>
        <description>If no Opt-In or Opt-Out is selected for France, then the Opt-In will automatically be selected</description>
        <field>Email_Opt_In__c</field>
        <literalValue>1</literalValue>
        <name>FR Opt-in</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opt_In_vs_Opt_Out</fullName>
        <description>When Email Opt In is selected, Email Opt-Out will be de-selected</description>
        <field>HasOptedOutOfEmail</field>
        <literalValue>0</literalValue>
        <name>Opt-In vs Opt-Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Active</fullName>
        <field>Active__c</field>
        <literalValue>Active</literalValue>
        <name>Status = Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Type_Form</fullName>
        <field>Form_Type__c</field>
        <formula>&apos;SQL_Form&apos;</formula>
        <name>Update Type Form</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>TO_ELOQUA_CONTACTS_V1</fullName>
        <apiVersion>42.0</apiVersion>
        <description>Send Contacts via ICS to Eloqua</description>
        <endpointUrl>https://tttics-a485859.integration.em2.oraclecloud.com/integration/flowsvc/salesforce/PROD_SALESF_TO_ELOQUA_CONTAC/v01/</endpointUrl>
        <fields>AccountId</fields>
        <fields>Act_On_Lead_Score_del_del__c</fields>
        <fields>Active__c</fields>
        <fields>AssistantName</fields>
        <fields>AssistantPhone</fields>
        <fields>Birthdate</fields>
        <fields>CC_Advertising_Display__c</fields>
        <fields>CC_Advertising_Raw__c</fields>
        <fields>Campaign_Id__c</fields>
        <fields>Campaign_Opt_in__c</fields>
        <fields>Campaign_Opt_out__c</fields>
        <fields>Commercial_Newsletter__c</fields>
        <fields>Company_Name__c</fields>
        <fields>ConnectionReceivedId</fields>
        <fields>ConnectionSentId</fields>
        <fields>CreatedById</fields>
        <fields>CreatedDate</fields>
        <fields>CurrencyIsoCode</fields>
        <fields>Data_Quality_Description__c</fields>
        <fields>Data_Quality_Score__c</fields>
        <fields>Department</fields>
        <fields>Description</fields>
        <fields>DoNotCall</fields>
        <fields>Duplicate_flag__c</fields>
        <fields>Eloqua_EngagementScore__c</fields>
        <fields>Eloqua_GDPR_Opt_In_Date__c</fields>
        <fields>Eloqua_GDPR_Opt_Out_Date__c</fields>
        <fields>Eloqua_Lead_Score__c</fields>
        <fields>Eloqua_ProfileScore__c</fields>
        <fields>Eloqua_Rating__c</fields>
        <fields>Email</fields>
        <fields>EmailBouncedDate</fields>
        <fields>EmailBouncedReason</fields>
        <fields>Email_Opt_In__c</fields>
        <fields>Email_Subscribe__c</fields>
        <fields>Email_Unsubscribe__c</fields>
        <fields>Events__c</fields>
        <fields>External_Account_Id__c</fields>
        <fields>External_Guid__c</fields>
        <fields>Fax</fields>
        <fields>FirstName</fields>
        <fields>Form_Type__c</fields>
        <fields>Former_Contact_Owner_Name__c</fields>
        <fields>GDPR_Evidence__c</fields>
        <fields>GDPR_Opt_In_Stamp_Date__c</fields>
        <fields>GDPR_Opt_In__c</fields>
        <fields>GDPR_Opt_Out_Stamp_Date__c</fields>
        <fields>GDPR_Opt_Source__c</fields>
        <fields>GDPR_Score__c</fields>
        <fields>GDPR_Submitted_on_behalf__c</fields>
        <fields>HasOptedOutOfEmail</fields>
        <fields>HasOptedOutOfFax</fields>
        <fields>Hash__c</fields>
        <fields>HomePhone</fields>
        <fields>Id</fields>
        <fields>Inactive_owner__c</fields>
        <fields>Industry_Code__c</fields>
        <fields>Industry_Insights__c</fields>
        <fields>Inside_Sales_Notes__c</fields>
        <fields>IsDeleted</fields>
        <fields>IsEmailBounced</fields>
        <fields>Jigsaw</fields>
        <fields>JigsawContactId</fields>
        <fields>Job_Role__c</fields>
        <fields>LID__LinkedIn_Company_Id__c</fields>
        <fields>LID__LinkedIn_Member_Token__c</fields>
        <fields>Language__c</fields>
        <fields>LastActivityDate</fields>
        <fields>LastCURequestDate</fields>
        <fields>LastCUUpdateDate</fields>
        <fields>LastModifiedById</fields>
        <fields>LastModifiedDate</fields>
        <fields>LastName</fields>
        <fields>LastReferencedDate</fields>
        <fields>LastViewedDate</fields>
        <fields>LeadSource</fields>
        <fields>Lead_Date_Created__c</fields>
        <fields>Learning__c</fields>
        <fields>MailingCity</fields>
        <fields>MailingCountry</fields>
        <fields>MailingGeocodeAccuracy</fields>
        <fields>MailingLatitude</fields>
        <fields>MailingLongitude</fields>
        <fields>MailingPostalCode</fields>
        <fields>MailingState</fields>
        <fields>MailingStreet</fields>
        <fields>MasterRecordId</fields>
        <fields>MobilePhone</fields>
        <fields>Newsletter__c</fields>
        <fields>OSC_Contact_ID__c</fields>
        <fields>Original_Description__c</fields>
        <fields>OtherCity</fields>
        <fields>OtherCountry</fields>
        <fields>OtherGeocodeAccuracy</fields>
        <fields>OtherLatitude</fields>
        <fields>OtherLongitude</fields>
        <fields>OtherPhone</fields>
        <fields>OtherPostalCode</fields>
        <fields>OtherState</fields>
        <fields>OtherStreet</fields>
        <fields>Other_Opt_out_reason__c</fields>
        <fields>OwnerId</fields>
        <fields>Owner_ID_del__c</fields>
        <fields>Phone</fields>
        <fields>PhotoUrl</fields>
        <fields>Primary_Fleet_Purpose__c</fields>
        <fields>Primary_Fleet_Size__c</fields>
        <fields>Primary_Vehicle_Type__c</fields>
        <fields>RecordTypeId</fields>
        <fields>ReportsToId</fields>
        <fields>Salutation</fields>
        <fields>Special_offers_Promotions__c</fields>
        <fields>Surveys__c</fields>
        <fields>SystemModstamp</fields>
        <fields>Technical_Newsletter__c</fields>
        <fields>Title</fields>
        <fields>Type__c</fields>
        <fields>Unsubscribe_reason__c</fields>
        <fields>WEBFLEET_Contact_Id__c</fields>
        <fields>WF_Account_Name__c</fields>
        <fields>Warm_up_date__c</fields>
        <fields>WebPage__c</fields>
        <fields>pw_cc__CountryText__c</fields>
        <fields>pw_cc__MailingAddressStatus__c</fields>
        <fields>pw_cc__MailingCountryLookup__c</fields>
        <fields>pw_cc__MailingStateLookup__c</fields>
        <fields>pw_cc__MailingZipCodeLookup__c</fields>
        <fields>pw_cc__OtherAddressStatus__c</fields>
        <fields>pw_cc__StateText__c</fields>
        <fields>qbdialer__CloseDate__c</fields>
        <fields>qbdialer__CloseScore__c</fields>
        <fields>qbdialer__ContactDate__c</fields>
        <fields>qbdialer__ContactScoreId__c</fields>
        <fields>qbdialer__ContactScore__c</fields>
        <fields>qbdialer__Dials__c</fields>
        <fields>qbdialer__LastCallTime__c</fields>
        <fields>qbdialer__ResponseTime__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>tomtom_support@engagementfactory.com</integrationUser>
        <name>TO_ELOQUA_CONTACTS_V1</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Campaign Opt-In vs Campaign Opt-Out</fullName>
        <actions>
            <name>Campaign_Opt_Out_vs_Campaign_Opt_In</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Campaign is Opt-In, then Campaign Opt-Out will be de-selected</description>
        <formula>( 
ISCHANGED(Campaign_Opt_out__c) &amp;&amp; 
Campaign_Opt_out__c = TRUE 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Campaign Opt-Out vs Campaign Opt-In</fullName>
        <actions>
            <name>Campaign_Opt_In_vs_Campaign_Opt_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Campaign is Opt-Out, then Campaign Opt-In will be de-selected</description>
        <formula>( 
ISCHANGED(Campaign_Opt_in__c) &amp;&amp; 
Campaign_Opt_in__c = TRUE 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact updated %28FR%29</fullName>
        <actions>
            <name>Contact_Updates_by_Act_On_Internal_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Follow_up_reminder_Internal</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Inform the Contact Owner when the old Act-On Lead Score is updated due to a form submit (France)</description>
        <formula>NOT( ISNEW()) &amp;&amp;  (ISCHANGED( Act_On_Lead_Score_del_del__c ) || ISCHANGED( Eloqua_Lead_Score__c ))  &amp;&amp;  (LastModifiedById = &apos;005a0000009BDiW&apos; ||  LastModifiedBy.Username =&apos;tomtom_support@engagementfactory.com&apos;) &amp;&amp;  NOT (CONTAINS(Email,&quot;@tomtom.com&quot;) || CONTAINS(Email,&quot;@webfleet.com&quot;)) &amp;&amp;  ( (Act_On_Lead_Score_del_del__c - PRIORVALUE(Act_On_Lead_Score_del_del__c)) &gt;= 35  ||  (VALUE(Eloqua_Lead_Score__c) - VALUE(PRIORVALUE(Eloqua_Lead_Score__c))) &gt;= 35 )  &amp;&amp;   (MailingCountry =&apos;France&apos;|| MailingCountry =&apos;FR&apos;)&amp;&amp; 
(CONTAINS( Form_Type__c ,&apos;SQL&apos;) || Campaign_Id__c=&apos;7011O00000285gm&apos;)&amp;&amp; 
/* CBM 2017 LINK410510 */
Campaign_Id__c  &lt;&gt; &apos;70114000002Cdva&apos;&amp;&amp;
/* CBM 2017 NAV2 */ 
Campaign_Id__c  &lt;&gt;&apos;70114000002CduD&apos;&amp;&amp;
/* DOYA Campaign*/
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4N&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4S&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDk&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDp&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDu&apos;&amp;&amp;
/* UKI 2017 LCY Fleet Event Registrees UK */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYZ&apos;&amp;&amp;
/* Contact_request_Callback_LCY Fleet Event Page */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYe&apos;&amp;&amp;
/* Prisma ES */
Campaign_Id__c  &lt;&gt;&apos;701140000027qec&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;701140000027qeh&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;701140000027qem&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;701140000027qvz&apos; &amp;&amp; 
/* CBM Campaigns */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021JTD&apos; &amp;&amp;
/* Others */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZG&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La9&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La4&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZz&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZu&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZk&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;701140000027pY3&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000002bXwM&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000003COat&apos;&amp;&amp;
Campaign_Id__c &lt;&gt;&apos;7011O000003JZn4&apos;&amp;&amp;
Campaign_Id__c &lt;&gt;&apos;7011O000003JZn9&apos;&amp;&amp;
Campaign_Id__c &lt;&gt;&apos;7011O000003JeEN&apos;&amp;&amp;
Campaign_Id__c &lt;&gt;&apos;7011O000003JeEX&apos;&amp;&amp;
Campaign_Id__c &lt;&gt;&apos;7011O000003JeEh&apos;&amp;&amp;
Campaign_Id__c &lt;&gt;&apos;7011O000003JeEc&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact updated %28No FR%29</fullName>
        <actions>
            <name>Contact_Updates_by_Act_On_Internal</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Follow_up_reminder_Internal</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Inform the Contact Owner when the old Act-On Lead Score is updated due to a new form submit - Country different to France</description>
        <formula>NOT( ISNEW()) &amp;&amp;  (ISCHANGED( Act_On_Lead_Score_del_del__c ) || ISCHANGED( Eloqua_Lead_Score__c ))  &amp;&amp;  (LastModifiedById = &apos;005a0000009BDiW&apos; ||  LastModifiedBy.Username =&apos;tomtom_support@engagementfactory.com&apos;) &amp;&amp;  NOT (CONTAINS(Email,&quot;@tomtom.com&quot;) || CONTAINS(Email,&quot;@webfleet.com&quot;)) &amp;&amp;  ( (Act_On_Lead_Score_del_del__c - PRIORVALUE(Act_On_Lead_Score_del_del__c)) &gt;= 35  ||  (VALUE(Eloqua_Lead_Score__c) - VALUE(PRIORVALUE(Eloqua_Lead_Score__c))) &gt;= 35 )  &amp;&amp;   (MailingCountry &lt;&gt; &apos;France&apos; &amp;&amp;  MailingCountry  &lt;&gt; &apos;FR&apos;) &amp;&amp;
CONTAINS( Form_Type__c ,&apos;SQL&apos;) &amp;&amp; 
Campaign_Id__c  &lt;&gt; &apos;70114000002Cdva&apos;&amp;&amp;
/* CBM 2017 NAV2 */ 
Campaign_Id__c  &lt;&gt;&apos;70114000002CduD&apos;&amp;&amp;
/* DOYA Campaign*/
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4N&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;70114000002Ce4S&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDk&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDp&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;70114000002CeDu&apos;&amp;&amp;
/* UKI 2017 LCY Fleet Event Registrees UK */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYZ&apos;&amp;&amp;
/* Contact_request_Callback_LCY Fleet Event Page */
Campaign_Id__c  &lt;&gt;&apos;70114000002CiYe&apos;&amp;&amp;
/* Prisma ES */
Campaign_Id__c  &lt;&gt;&apos;701140000027qec&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;701140000027qeh&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;701140000027qem&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;701140000027qvz&apos; &amp;&amp; 
/* CBM Campaigns */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021JTD&apos;&amp;&amp;
/* Others */
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZG&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La9&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O0000021La4&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZz&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZu&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O0000021LZk&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;701140000027pY3&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000002bXwM&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000002bcuN&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000003COtV&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000003COat&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000003JZma&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000003JZn4&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000003JZn9&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000003JeEN&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000003JeEX&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000003JeEh&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000003JeEc&apos;&amp;&amp;
Campaign_Id__c  &lt;&gt;&apos;7011O000002dpWh&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact updated Pardot</fullName>
        <actions>
            <name>Notify</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform the Contact Owner when Lead  is updated due to a SQL form submit</description>
        <formula>NOT(ISNEW())  
&amp;&amp;
LastModifiedById = &apos;0051O00000DhGLI&apos;   
&amp;&amp;
CONTAINS(Form_Type__c ,&apos;SQL&apos;)
&amp;&amp;
ISCHANGED(MarketingID__c)
&amp;&amp;
(pi__score__c - PRIORVALUE(pi__score__c)&gt; 49)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact updated by Act-On</fullName>
        <actions>
            <name>Contact_Updates_by_Act_On_Internal</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Follow_up_reminder_Internal</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Inform the Contact Owner when the Act-On Lead Score is updated by Act-On.</description>
        <formula>NOT( ISNEW()) &amp;&amp; ISCHANGED( Act_On_Lead_Score_del_del__c ) &amp;&amp; LastModifiedById = &apos;005a0000009BDiW&apos; &amp;&amp; NOT (CONTAINS(Email,&quot;@tomtom.com&quot;) || CONTAINS(Email,&quot;@webfleet.com&quot;)) &amp;&amp; 
(Act_On_Lead_Score_del_del__c - PRIORVALUE(Act_On_Lead_Score_del_del__c)) &gt;= 35</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Copy Contact Description</fullName>
        <actions>
            <name>Copy_Contact_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the &apos;original&apos; Contact Description to the Original Description field</description>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Default status %3D Active</fullName>
        <actions>
            <name>Status_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Active__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set the default status for newly created contacts, if it has been left blank by the creator of the record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>E-mail Opt-Out vs Opt-In</fullName>
        <actions>
            <name>E_mail_Opt_Out_vs_Opt_In</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Contact is Opt-Out, then Opt-In will be de-selected</description>
        <formula>( 
ISCHANGED( HasOptedOutOfEmail ) &amp;&amp; 
HasOptedOutOfEmail = TRUE 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt-In vs Opt-Out</fullName>
        <actions>
            <name>Opt_In_vs_Opt_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Contact is Opt-In, then Opt-Out will be de-selected</description>
        <formula>( 
ISCHANGED( Email_Opt_In__c ) &amp;&amp; 
Email_Opt_In__c = TRUE 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>No Opt-out or Opt-In for FR</fullName>
        <actions>
            <name>FR_Opt_in</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email_Opt_In__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
            <value>France</value>
        </criteriaItems>
        <description>If a new contact is created for France and no Opt-In or Opt-Out is selected, the Contact will automatically be Opt-In</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Contact to Eloqua via ICS</fullName>
        <actions>
            <name>TO_ELOQUA_CONTACTS_V1</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>eloqua</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.exclude_eloqua_integration__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Type Form</fullName>
        <actions>
            <name>Update_Type_Form</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>eloqua</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Form_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update Type Form for Contacts if it&apos;s blank</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>test</fullName>
        <active>false</active>
        <formula>ISCHANGED( Email_Opt_In__c ) &amp;&amp;  Email_Opt_In__c  = true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Call Contact</fullName>
        <actions>
            <name>zaapit__Call_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Call Contact  - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Email Contact</fullName>
        <actions>
            <name>zaapit__Email_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Email Contact - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Letter Contact</fullName>
        <actions>
            <name>zaapit__Send_Letter_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Send Letter Contact - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Quote Contact</fullName>
        <actions>
            <name>zaapit__Send_Quote_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Send Quote Contact - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Follow_up_reminder_Internal</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your Lead/Contact has submitted a contact request on our website. Please review the Contact History and follow up accordingly.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow up reminder (Internal)</subject>
    </tasks>
    <tasks>
        <fullName>Follow_up_reminder_Portal</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your Lead/Contact has submitted a contact request on our website. Please review the Contact History and follow up accordingly.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow up reminder (Portal)</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Call_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Call</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Email_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Email</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Send_Letter_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Letter</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Send_Quote_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Quote</subject>
    </tasks>
</Workflow>