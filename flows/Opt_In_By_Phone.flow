<?xml version="1.0" encoding="UTF-8"?>
<Flow xmlns="http://soap.sforce.com/2006/04/metadata">
    <apiVersion>49.0</apiVersion>
    <decisions>
        <name>Does_External_User_Have_Opt_In_Permission</name>
        <label>Does External User Have Opt-In Permission</label>
        <locationX>182</locationX>
        <locationY>278</locationY>
        <defaultConnector>
            <targetReference>Insufficient_Permissions_External</targetReference>
        </defaultConnector>
        <defaultConnectorLabel>Default Outcome</defaultConnectorLabel>
        <rules>
            <name>Has_Permission_External</name>
            <conditionLogic>and</conditionLogic>
            <conditions>
                <leftValueReference>Opt_In_By_Phone_3rd_Party_Permission</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <booleanValue>true</booleanValue>
                </rightValue>
            </conditions>
            <connector>
                <targetReference>Opt_In_By_Phone_3rd_Party</targetReference>
            </connector>
            <label>Has Permission External</label>
        </rules>
    </decisions>
    <decisions>
        <name>Does_User_Have_Permission_To_Opt_In</name>
        <label>Does User Have Permission To Opt-In</label>
        <locationX>1106</locationX>
        <locationY>518</locationY>
        <defaultConnector>
            <targetReference>Insufficient_Permissions</targetReference>
        </defaultConnector>
        <defaultConnectorLabel>Does Not Have Permission</defaultConnectorLabel>
        <rules>
            <name>Has_Permission</name>
            <conditionLogic>and</conditionLogic>
            <conditions>
                <leftValueReference>Opt_In_By_Phone_Permission</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <booleanValue>true</booleanValue>
                </rightValue>
            </conditions>
            <connector>
                <targetReference>ININ_or_FrontSpin</targetReference>
            </connector>
            <label>Has Permission</label>
        </rules>
    </decisions>
    <decisions>
        <description>Check if the call task is from ININ or FrontSpin</description>
        <name>ININ_or_FrontSpin</name>
        <label>ININ or FrontSpin</label>
        <locationX>842</locationX>
        <locationY>638</locationY>
        <defaultConnector>
            <targetReference>Invalid_Task_Type</targetReference>
        </defaultConnector>
        <defaultConnectorLabel>Is Not Valid Call Task</defaultConnectorLabel>
        <rules>
            <name>Is_ININ</name>
            <conditionLogic>and</conditionLogic>
            <conditions>
                <leftValueReference>Opt_In_Task_Record.Type</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>Call</stringValue>
                </rightValue>
            </conditions>
            <connector>
                <targetReference>Opt_In_By_Phone_ININ</targetReference>
            </connector>
            <label>Is ININ</label>
        </rules>
        <rules>
            <name>Is_FrontSpin</name>
            <conditionLogic>and</conditionLogic>
            <conditions>
                <leftValueReference>Opt_In_Task_Record.Type</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>Phone Call</stringValue>
                </rightValue>
            </conditions>
            <conditions>
                <leftValueReference>Opt_In_Task_Record.CallObject</leftValueReference>
                <operator>IsNull</operator>
                <rightValue>
                    <booleanValue>false</booleanValue>
                </rightValue>
            </conditions>
            <connector>
                <targetReference>Opt_In_By_Phone_FrontSpin</targetReference>
            </connector>
            <label>Is FrontSpin</label>
        </rules>
    </decisions>
    <decisions>
        <name>Is_External_User_Decision</name>
        <label>Is External User</label>
        <locationX>776</locationX>
        <locationY>158</locationY>
        <defaultConnector>
            <targetReference>Get_Task_Record</targetReference>
        </defaultConnector>
        <defaultConnectorLabel>Default Outcome</defaultConnectorLabel>
        <rules>
            <name>External_User</name>
            <conditionLogic>and</conditionLogic>
            <conditions>
                <leftValueReference>$Profile.Name</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>TTT Lead Qualification Partner</stringValue>
                </rightValue>
            </conditions>
            <connector>
                <targetReference>Does_External_User_Have_Opt_In_Permission</targetReference>
            </connector>
            <label>External User</label>
        </rules>
    </decisions>
    <decisions>
        <description>Checks if the running user is the same as the task creator (person who made phone call).</description>
        <name>Is_Running_User_Same_As_Task_Creator</name>
        <label>Is Running User Same As Task Creator</label>
        <locationX>1370</locationX>
        <locationY>398</locationY>
        <defaultConnector>
            <targetReference>Incorrect_User_Running</targetReference>
        </defaultConnector>
        <defaultConnectorLabel>Incorrect User</defaultConnectorLabel>
        <rules>
            <name>Correct_User</name>
            <conditionLogic>and</conditionLogic>
            <conditions>
                <leftValueReference>Running_User_Id</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <elementReference>Opt_In_Task_Record.CreatedById</elementReference>
                </rightValue>
            </conditions>
            <connector>
                <targetReference>Does_User_Have_Permission_To_Opt_In</targetReference>
            </connector>
            <label>Correct User</label>
        </rules>
    </decisions>
    <description>MWISE Updates 20210409</description>
    <formulas>
        <name>Opt_In_By_Phone_3rd_Party_Permission</name>
        <dataType>Boolean</dataType>
        <expression>{!$Permission.Opt_In_By_Phone_3rd_Party}</expression>
    </formulas>
    <formulas>
        <name>Opt_In_By_Phone_Permission</name>
        <dataType>Boolean</dataType>
        <expression>{!$Permission.Opt_In_By_Phone}</expression>
    </formulas>
    <formulas>
        <name>Running_User_Id</name>
        <dataType>String</dataType>
        <expression>CASESAFEID({!$User.Id})</expression>
    </formulas>
    <interviewLabel>Opt-In By Phone {!$Flow.CurrentDateTime}</interviewLabel>
    <label>Opt-In By Phone</label>
    <processMetadataValues>
        <name>BuilderType</name>
        <value>
            <stringValue>LightningFlowBuilder</stringValue>
        </value>
    </processMetadataValues>
    <processMetadataValues>
        <name>CanvasMode</name>
        <value>
            <stringValue>AUTO_LAYOUT_CANVAS</stringValue>
        </value>
    </processMetadataValues>
    <processMetadataValues>
        <name>OriginBuilderType</name>
        <value>
            <stringValue>LightningFlowBuilder</stringValue>
        </value>
    </processMetadataValues>
    <processType>Flow</processType>
    <recordLookups>
        <name>Get_Task_Record</name>
        <label>Get Task Record</label>
        <locationX>1370</locationX>
        <locationY>278</locationY>
        <assignNullValuesIfNoRecordsFound>true</assignNullValuesIfNoRecordsFound>
        <connector>
            <targetReference>Is_Running_User_Same_As_Task_Creator</targetReference>
        </connector>
        <filterLogic>and</filterLogic>
        <filters>
            <field>Id</field>
            <operator>EqualTo</operator>
            <value>
                <elementReference>recordId</elementReference>
            </value>
        </filters>
        <object>Task</object>
        <outputReference>Opt_In_Task_Record</outputReference>
        <queriedFields>Id</queriedFields>
        <queriedFields>Type</queriedFields>
        <queriedFields>WhoId</queriedFields>
        <queriedFields>CreatedById</queriedFields>
        <queriedFields>CallObject</queriedFields>
    </recordLookups>
    <runInMode>DefaultMode</runInMode>
    <screens>
        <description>Screen to show when the flow is launched by someone other than the user that created the task (the caller).</description>
        <name>Incorrect_User_Running</name>
        <label>Incorrect User Running</label>
        <locationX>1634</locationX>
        <locationY>518</locationY>
        <allowBack>false</allowBack>
        <allowFinish>true</allowFinish>
        <allowPause>false</allowPause>
        <fields>
            <name>Invalid_Running_User_Error_Text</name>
            <fieldText>&lt;p&gt;You can only perform an opt-in if you made the phone call.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;The opt-in will not be completed.&lt;/p&gt;</fieldText>
            <fieldType>DisplayText</fieldType>
        </fields>
        <showFooter>true</showFooter>
        <showHeader>true</showHeader>
    </screens>
    <screens>
        <name>Insufficient_Permissions</name>
        <label>Insufficient Permissions</label>
        <locationX>1370</locationX>
        <locationY>638</locationY>
        <allowBack>false</allowBack>
        <allowFinish>true</allowFinish>
        <allowPause>false</allowPause>
        <fields>
            <name>Insufficient_Permissions_Display_Text</name>
            <fieldText>&lt;p&gt;You require the Opt-In By Phone custom permission to perform this action.&lt;/p&gt;&lt;p&gt;Please get in contact with a Salesforce administrator if you believe this is an error.&lt;/p&gt;</fieldText>
            <fieldType>DisplayText</fieldType>
        </fields>
        <showFooter>true</showFooter>
        <showHeader>true</showHeader>
    </screens>
    <screens>
        <name>Insufficient_Permissions_External</name>
        <label>Insufficient Permissions</label>
        <locationX>314</locationX>
        <locationY>398</locationY>
        <allowBack>false</allowBack>
        <allowFinish>true</allowFinish>
        <allowPause>false</allowPause>
        <fields>
            <name>Insufficient_Permissions_External_Display_Text</name>
            <fieldText>&lt;p&gt;You require the Opt-In By Phone 3rd Party custom permission to perform this action.&lt;/p&gt;&lt;p&gt;Please get in contact with a Salesforce administrator if you believe this is an error.&lt;/p&gt;</fieldText>
            <fieldType>DisplayText</fieldType>
        </fields>
        <showFooter>true</showFooter>
        <showHeader>true</showHeader>
    </screens>
    <screens>
        <name>Invalid_Task_Type</name>
        <label>Invalid Task Type</label>
        <locationX>1106</locationX>
        <locationY>758</locationY>
        <allowBack>false</allowBack>
        <allowFinish>true</allowFinish>
        <allowPause>false</allowPause>
        <fields>
            <name>Invalid_Task_Type_Error_Text</name>
            <fieldText>&lt;p&gt;Error: An opt-in can only be performed from a task generated by a phone call with the FrontSpin phone system with a valid call ID.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;The opt-in will not be completed.&lt;/p&gt;</fieldText>
            <fieldType>DisplayText</fieldType>
        </fields>
        <showFooter>true</showFooter>
        <showHeader>true</showHeader>
    </screens>
    <start>
        <locationX>650</locationX>
        <locationY>0</locationY>
        <connector>
            <targetReference>Is_External_User_Decision</targetReference>
        </connector>
    </start>
    <status>Draft</status>
    <subflows>
        <name>Opt_In_By_Phone_3rd_Party</name>
        <label>Opt-In By Phone 3rd Party</label>
        <locationX>50</locationX>
        <locationY>398</locationY>
        <flowName>Opt_In_By_Phone_3rd_Party</flowName>
        <inputAssignments>
            <name>recordId</name>
            <value>
                <elementReference>recordId</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <name>Running_User_Id</name>
            <value>
                <elementReference>Running_User_Id</elementReference>
            </value>
        </inputAssignments>
    </subflows>
    <subflows>
        <name>Opt_In_By_Phone_FrontSpin</name>
        <label>Opt-In By Phone FrontSpin</label>
        <locationX>842</locationX>
        <locationY>758</locationY>
        <flowName>Opt_In_By_Phone_FrontSpin</flowName>
        <inputAssignments>
            <name>recordId</name>
            <value>
                <elementReference>recordId</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <name>Running_User_Id</name>
            <value>
                <elementReference>Running_User_Id</elementReference>
            </value>
        </inputAssignments>
    </subflows>
    <subflows>
        <name>Opt_In_By_Phone_ININ</name>
        <label>Opt-In By Phone ININ</label>
        <locationX>578</locationX>
        <locationY>758</locationY>
        <flowName>Opt_In_By_Phone_ININ</flowName>
        <inputAssignments>
            <name>recordId</name>
            <value>
                <elementReference>recordId</elementReference>
            </value>
        </inputAssignments>
    </subflows>
    <variables>
        <description>The task record from which the opt-in was started.</description>
        <name>Opt_In_Task_Record</name>
        <dataType>SObject</dataType>
        <isCollection>false</isCollection>
        <isInput>false</isInput>
        <isOutput>false</isOutput>
        <objectType>Task</objectType>
    </variables>
    <variables>
        <description>The ID of the record from which the flow is launched (populated automatically).</description>
        <name>recordId</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>false</isOutput>
    </variables>
</Flow>
