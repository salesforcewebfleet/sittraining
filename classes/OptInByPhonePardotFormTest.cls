@isTest
private class OptInByPhonePardotFormTest {
    @isTest static void testOptInForm() {
        OptInByPhonePardotForm optInForm = new OptInByPhonePardotForm();
        System.assertEquals('https://e.webfleet.com/l/844203/2020-11-16/xxlr8', optInForm.getEndpoint());
        System.assertEquals('application/x-www-form-urlencoded', optInForm.getFormHeaders().search('Content-Type'));
        System.assertEquals(true, optInForm.getFormFields().search('email').getRequired());
        System.assertEquals(true, optInForm.getFormFields().search('country').getRequired());
    }

    @isTest static void testUpdateForm() {

        // Create form
        OptInByPhonePardotForm optInForm = new OptInByPhonePardotForm();

        String testEmail = 'mwisetest@mailinator.com';
        String testCountry = 'Germany';

        // Update form
        optInForm.updateForm(testEmail, testCountry);

        System.assertEquals(testEmail, optInForm.getFormFields().search('email').getValue());
        System.assertEquals(testCountry, optInForm.getFormFields().search('country').getValue());
    }
}