public virtual class PardotForm extends WebForm {
    public PardotForm(String endPoint) {
        super(endPoint);
        formHeaders.add('Content-Type', 'application/x-www-form-urlencoded');
    }
}