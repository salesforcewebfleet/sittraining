@ isTest
public class testCONTRACTSharing {

	@testSetup static void SetUp() {
		list<Profile> cp1 = [select Id, name from Profile where Name = 'Community Price Calculation' limit 1];
		List<Profile> p1 = [Select Id from Profile where Name = 'System Administrator' limit 1];
		List<UserRole> r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'Board' LIMIT 1];

		User m1 = new User();
		m1.FirstName = 'Test';
		m1.LastName = 'Manager';
		m1.Alias = 't111';
		m1.Email = 'abhijit2018@gmail.com';
		m1.Username = 'abhijit2018@gmail.com';
		m1.CommunityNickname = 'mgrabtestnickname1';
		m1.EmailEncodingKey = 'ISO-8859-1';
		m1.TimeZoneSidKey = 'Europe/Amsterdam';
		m1.LocaleSidKey = 'en_US';
		m1.LanguageLocaleKey = 'en_US';
		m1.CurrencyIsoCode = 'EUR';
		m1.ProfileId = p1 [0].Id;
		m1.UserRoleId = r1 [0].Id;
		insert m1;

		//create user record
		User u1 = new User();
		u1.FirstName = 'Test';
		u1.LastName = 'User';
		u1.Alias = 't122';
		u1.Email = 'abhijit2017@gmail.com';
		u1.Username = 'abhijit2017@gmail.com';
		u1.CommunityNickname = 'abtestnickname1';
		u1.EmailEncodingKey = 'ISO-8859-1';
		u1.TimeZoneSidKey = 'Europe/Amsterdam';
		u1.LocaleSidKey = 'en_US';
		u1.LanguageLocaleKey = 'en_US';
		u1.CurrencyIsoCode = 'EUR';
		u1.ManagerId = m1.Id;
		u1.ProfileId = p1 [0].Id;
		insert u1;

		//create controller
		User c1 = new User();
		c1.FirstName = 'Test';
		c1.LastName = 'Controller';
		c1.Alias = 'c333';
		c1.Email = 'abhijit201117@gmail.com';
		c1.Username = 'abhijit201117@gmail.com';
		c1.CommunityNickname = 'contabtestnickname1';
		c1.EmailEncodingKey = 'ISO-8859-1';
		c1.TimeZoneSidKey = 'Europe/Amsterdam';
		c1.LanguageLocaleKey = 'en_US';
		c1.LocaleSidKey = 'en_US';
		c1.CurrencyIsoCode = 'EUR';
		c1.ProfileId = p1 [0].Id;
		c1.UserRoleId = r1 [0].Id;
		insert c1;
	}

	@isTest static void testMethod1() {
		list<User> u12 = [select Id, UserName from User where UserName = 'abhijit2017@gmail.com' LIMIT 1];
		list<User> c12 = [select Id, UserName from User where UserName = 'abhijit201117@gmail.com' LIMIT 1];

		//create org unit
		Org_Unit__c orgunit = new Org_Unit__c();
		orgunit.OwnerId = u12 [0].Id;
		orgunit.Controller__c = c12 [0].Id;
		orgunit.Name = 'MY ORG UNT';
		insert orgunit;
		//end of user data insert

		TT_Contract__c newcon = new TT_Contract__c();
		newcon.Name = 'AB test new contract';
		newcon.OwnerId = u12 [0].Id;
		newcon.Contract_Type__c = 'Sales';
		newcon.Org_Unit__c = orgunit.Id;
		newcon.Start_Date__c = Date.newinstance(2019, 7, 20);
		newcon.End_Date__c = Date.newinstance(2019, 7, 29);
		newcon.Notice_Period_Days__c = 10;
		newcon.Reminder_Period_Days__c = 1;
		newcon.Involved_Ip__c = 'No';
		newcon.Duration_Type__c = 'Fixed';
		newcon.Counter_Party_Name__c = 'Party';
		newcon.Annual_Contract_Value_in_EUR__c = 1;
		newcon.Status__c = 'Active';
		insert newcon;

		TT_Contract__c newcon1 = new TT_Contract__c();
		newcon1.Name = 'AB test new contract1';
		newcon1.OwnerId = u12 [0].Id;
		newcon1.Contract_Type__c = 'Sales';
		newcon1.Org_Unit__c = orgunit.Id;
		newcon1.Start_Date__c = Date.newinstance(2019, 7, 20);
		newcon1.End_Date__c = Date.newinstance(2019, 7, 29);
		newcon1.Notice_Period_Days__c = 10;
		newcon1.Reminder_Period_Days__c = 1;
		newcon1.Involved_Ip__c = 'No';
		newcon1.Duration_Type__c = 'Fixed';
		newcon1.Counter_Party_Name__c = 'Party';
		newcon1.Annual_Contract_Value_in_EUR__c = 1;
		newcon1.Status__c = 'Active';
		insert newcon1;

	}   

	public static testMethod void CreateContract() {
        list<User> u12 = [select Id, UserName from User where UserName = 'abhijit2017@gmail.com' LIMIT 1];
		list<User> c12 = [select Id, UserName from User where UserName = 'abhijit201117@gmail.com' LIMIT 1];

		//create org unit
		Org_Unit__c orgunit = new Org_Unit__c();
		orgunit.OwnerId = u12 [0].Id;
		orgunit.Controller__c = c12 [0].Id;
		orgunit.Name = 'MY ORG UNT';
		insert orgunit;
        
		User u3 = [select Id from user where username = 'abhijit2017@gmail.com' LIMIT 1];
		list<user> u31 = [select Id from user where username = 'abhijit201117@gmail.com' LIMIT 1];

		User thisUser = [select Id from User where Id = :UserInfo.getUserId()];

		System.runAs(thisUser) {
			list<User> u42 = [select Org_Unit_SFDC_ID__c, Id from User where Id = :u3.Id];
			u42[0].Org_Unit_SFDC_ID__c = orgunit.Id;
			update u42;
		}

		TT_Contract__c newcon3 = new TT_Contract__c();
		newcon3.Name = 'AB test new contract';
		newcon3.OwnerId = u3.Id;
		newcon3.Contract_Type__c = 'Sales';
		newcon3.Org_Unit__c = orgunit.Id;
		newcon3.Start_Date__c = Date.newinstance(2019, 7, 20);
		newcon3.End_Date__c = Date.newinstance(2019, 7, 29);
		newcon3.Notice_Period_Days__c = 10;
		newcon3.Reminder_Period_Days__c = 1;
		newcon3.Involved_Ip__c = 'No';
		newcon3.Duration_Type__c = 'Fixed';
		newcon3.Counter_Party_Name__c = 'Party';
		newcon3.Annual_Contract_Value_in_EUR__c = 1;

		insert newcon3;

		list<TT_Contract__c> con4 = [select OwnerId, Id from TT_Contract__c where Id = :newcon3.Id LIMIT 1];
		if(con4.size() > 0) {
			con4[0].OwnerId = u31 [0].Id;
			update con4;
		}
	}


	public static testMethod void InactivateUser() {
		User u4 = [select Id from user where username = 'abhijit2017@gmail.com' LIMIT 1];

		list<User> updUser1 = [select isActive from User where Id = :u4.Id LIMIT 1];
		if(updUser1.size() > 0) {
			updUser1[0].isActive = false;
			update updUser1;
		}
	}

	public static testMethod void BatchSharingRecalc() {
        list<User> u12 = [select Id, UserName from User where UserName = 'abhijit2017@gmail.com' LIMIT 1];
		list<User> c12 = [select Id, UserName from User where UserName = 'abhijit201117@gmail.com' LIMIT 1];

		//create org unit
		Org_Unit__c orgunit = new Org_Unit__c();
		orgunit.OwnerId = u12 [0].Id;
		orgunit.Controller__c = c12 [0].Id;
		orgunit.Name = 'MY ORG UNT';
		insert orgunit;
        
		User u3 = [select Id from user where username = 'abhijit2017@gmail.com' LIMIT 1];

		User thisUser = [select Id from User where Id = :UserInfo.getUserId()];

		System.runAs(thisUser) {
			list<User> u41 = [select Org_Unit_SFDC_ID__c, Id from User where Id = :u3.Id];
			u41[0].Org_Unit_SFDC_ID__c = orgunit.Id;
			update u41;
		}

		TT_Contract__c newcon3 = new TT_Contract__c();
		newcon3.Name = 'AB test new contract';
		newcon3.OwnerId = u3.Id;
		newcon3.Contract_Type__c = 'Sales';
		newcon3.Org_Unit__c = orgunit.Id;
		newcon3.Start_Date__c = Date.newinstance(2019, 7, 20);
		newcon3.End_Date__c = Date.newinstance(2019, 7, 29);
		newcon3.Notice_Period_Days__c = 10;
		newcon3.Reminder_Period_Days__c = 1;
		newcon3.Involved_Ip__c = 'No';
		newcon3.Duration_Type__c = 'Fixed';
		newcon3.Counter_Party_Name__c = 'Party';
		newcon3.Annual_Contract_Value_in_EUR__c = 1;

		insert newcon3;

		ContractSharingRecalculate  recalc = new ContractSharingRecalculate();
		String sch = '0 0 23 * * ?';
		system.schedule('Test check', sch, recalc);

		ContractSharingBatch b1 = new ContractSharingBatch();
		Database.executeBatch(b1);
	}
    
    @isTest static void checkClosedLostOpportunity() {
        
        ContractSharing.SendEmail(new Set<Id> {UserInfo.getUserId()});
    }
    
    @isTest static void testShareRecordForOwnerChangeForUserTermination(){
        list<User> u12 = [select Id, UserName from User where UserName = 'abhijit2017@gmail.com' LIMIT 1];
		list<User> c12 = [select Id, UserName from User where UserName = 'abhijit201117@gmail.com' LIMIT 1];

		//create org unit
		Org_Unit__c orgunit = new Org_Unit__c();
		orgunit.OwnerId = u12 [0].Id;
		orgunit.Controller__c = c12 [0].Id;
		orgunit.Name = 'MY ORG UNT';
		insert orgunit;
        
		User u3 = [select Id from user where username = 'abhijit2017@gmail.com' LIMIT 1];
		list<user> u31 = [select Id from user where username = 'abhijit201117@gmail.com' LIMIT 1];

		User thisUser = [select Id from User where Id = :UserInfo.getUserId()];

		System.runAs(thisUser) {
			list<User> u42 = [select Org_Unit_SFDC_ID__c, Id from User where Id = :u3.Id];
			u42[0].Org_Unit_SFDC_ID__c = orgunit.Id;
			update u42;
		}

		TT_Contract__c newcon3 = new TT_Contract__c();
		newcon3.Name = 'AB test new contract';
		newcon3.OwnerId = u3.Id;
		newcon3.Contract_Type__c = 'Sales';
		newcon3.Org_Unit__c = orgunit.Id;
		newcon3.Start_Date__c = Date.newinstance(2019, 7, 20);
		newcon3.End_Date__c = Date.newinstance(2019, 7, 29);
		newcon3.Notice_Period_Days__c = 10;
		newcon3.Reminder_Period_Days__c = 1;
		newcon3.Involved_Ip__c = 'No';
		newcon3.Duration_Type__c = 'Fixed';
		newcon3.Counter_Party_Name__c = 'Party';
		newcon3.Annual_Contract_Value_in_EUR__c = 1;

		insert newcon3;
        ContractSharing.CalledfromUserTermination = true;

		list<TT_Contract__c> con4 = [select OwnerId, Id from TT_Contract__c where Id = :newcon3.Id LIMIT 1];
		if(con4.size() > 0) {
			con4[0].OwnerId = u31 [0].Id;
			update con4;
		}
    }
}