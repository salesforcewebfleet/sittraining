@isTest
private class TouchPointParticipantHandlerTest extends c_TestFactory{
    
    // Bulkify or not? Look at the settings on the TestFactory to decide how much data to build
    static Integer totalAccounts = c_TestFactory.BULKIFY_TESTS ? 100 : 2;
    
    @IsTest
    static void testInsertNotExcluded(){
        Contact c = [SELECT Id,AccountId,LastName,FirstName FROM Contact LIMIT 1];
        TouchPointParticipant__c newTPP = new TouchPointParticipant__c(Account__c = c.AccountId,contact__c = c.Id, email__c = 'test@test.com', LastName__c= c.LastName, FirstName__c = c.FirstName);
        insert newTPP;
        List<TouchPointParticipant__c> insTPP = [SELECT Id FROM TouchPointParticipant__c where email__c = 'test@test.com' and Status__c != 'Excluded'];
        System.assertEquals(1,insTpp.size());
    }
    
     /*@IsTest
    static void testInsertExcluded(){
        Contact c = [SELECT Id,AccountId,LastName,FirstName FROM Contact LIMIT 1];
        TouchPointParticipant__c newTPP = new TouchPointParticipant__c(Account__c = c.AccountId,contact__c = c.Id, email__c = 'account@test.com', LastName__c= c.LastName, FirstName__c = c.FirstName);
        insert newTPP;
        List<TouchPointParticipant__c> insTPP = [SELECT Id FROM TouchPointParticipant__c where email__c = 'account@test.com' and Status__c = 'Excluded'];
        System.assertEquals(1,insTpp.size());
    }*/
    
    @TestSetup
    static void createData() {
        c_TestFactory.setDefaultContext();

        List<Account> accounts = new List<Account>();
        for (Integer i = 0; i < totalAccounts; i++) {
            Account account = (Account) make(Entity.SERVICE_ACCOUNT, new Account());
            accounts.add(account);
        }

        List<Contact> contacts = new List<Contact>();
        for (Account account : accounts) {
            Contact c = (Contact) make(Entity.SERVICE_CONTACT, new Contact(Account = account));
            contacts.add(c);
        }
        run();
    }

}