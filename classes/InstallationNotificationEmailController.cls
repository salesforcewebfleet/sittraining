/**
 * @description Queries the work orders related to the provided CaseId.
 *
 * @author fheeneman Salesforce
 *
 * @date 17/4/20    fheeneman    Initial setup <br />
 * @date 10/7/20    fheeneman    Added WorkOrderNumber field to query <br />
 */
public without sharing class InstallationNotificationEmailController {

    public String caseId { get; set; }

    public List<WorkOrder> getWorkOrders() {
        return [
                SELECT
                        WorkOrderNumber,
                        InstallationPartner__c,
                        InstallationPartner__r.Name,
                        AlternativeContact__r.Name,
                        AlternativeContact__r.Email,
                        AlternativeContact__r.Phone
                FROM WorkOrder
                WHERE CaseId = :caseId AND IPAssignmentEmailStatus__c IN ('Send Pending', 'Send Failed')
        ];
    }
}