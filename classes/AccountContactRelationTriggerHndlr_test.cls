/**
 * @author      fheeneman
 * @author      Salesforce
 * @description Description
 *              Tests executed:
 *              1 - testPost() - description
 *              2 - testPostWithException() - description
 * @date 14/2/20		fheeneman		Initial setup
 */
@IsTest
private class AccountContactRelationTriggerHndlr_test extends c_TestFactory {

    // Bulkify or not? Look at the settings on the TestFactory to decide how much data to build
    static Integer totalAccounts = c_TestFactory.BULKIFY_TESTS ? 100 : 2;

    @IsTest
    static void testBeforeInsert() {
        // Arrange
        c_TestFactory.setDefaultContext();
        List<AccountContactRelation> accountContactRelations = new List<AccountContactRelation>();

        Account unrelatedAccount = (Account) make(Entity.SERVICE_ACCOUNT, new Account());
        Contact unrelatedContact = (Contact) make(Entity.SERVICE_CONTACT, new Contact(Account = unrelatedAccount));
        run();

        List<Account > accounts = [SELECT Id FROM Account LIMIT :totalAccounts];
        for (Account account : accounts) {
            AccountContactRelation acr = (AccountContactRelation) make(Entity.SERVICE_ACR, new AccountContactRelation(AccountId = account.Id, ContactId = unrelatedContact.Id));
            accountContactRelations.add(acr);
        }

        // Act
        Test.startTest();
        insert accountContactRelations;
        Test.stopTest();

        // Assert
        for (AccountContactRelation accountContactRelation : accountContactRelations) {
            System.assert(String.isNotBlank(accountContactRelation.Id), 'The ACR was not inserted, the Id is null');
        }
    }

    @TestSetup
    static void createData() {
        c_TestFactory.setDefaultContext();

        Account[] accounts = new List<Account>();
        for (Integer i = 0; i < totalAccounts; i++) {
            Account account = (Account) make(Entity.SERVICE_ACCOUNT, new Account());
            accounts.add(account);
        }

        List<Contact> contacts = new List<Contact>();
        // Giving every account 1 contact, which generates an ACR between them by default.
        for (Account account : accounts) {
            contacts.add((Contact) make(Entity.SERVICE_CONTACT, new Contact(Account = account)));
        }

        // We don't want the ACR to be trigger right now, because we need to test it in our methods.
        TriggerHandler.bypass('AccountContactRelationTriggerHandler');
        run();
        TriggerHandler.clearBypass('AccountContactRelationTriggerHandler');
    }
}