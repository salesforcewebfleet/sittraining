/* Author: Abhijit Ghanekar GIL-36
Scheduled processing of records
*/
global class ContractSharingRecalculate implements Schedulable
{

    global void execute(SchedulableContext ctx)
    {
        ContractSharingBatch  batch = new ContractSharingBatch(); 
        Database.executeBatch(batch, 1);   
    }

}