/**
 * @author      fheeneman
 * @author      Salesforce
 * @description Description
 *              Tests executed:
 *              1 - testPost() - description
 *              2 - testPostWithException() - description
 * @date 15/5/20		fheeneman		Initial setup
 */
@IsTest
private class CaseAssignmentRuleInvocableAction_test  extends c_TestFactory {
    static Integer totalCases = c_TestFactory.BULKIFY_TESTS ? 200 : 2;

    @IsTest
    static void runAssigmentRule_Default() {
        // Arrange
        Case newCase = (Case) make(Entity.SERVICE_CASE, new Case());
        run();

        // The invocable method only runs on already inserted cases
        CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest request = new CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest();
        request.aCase = newCase;
        request.assignmentRuleName = '';

        List<CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest> caseAssignmentRuleRequests = new List<CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest>();
        caseAssignmentRuleRequests.add(request);

        // Act
        Test.startTest();
        CaseAssignmentRuleInvocableAction.runAssignmentRule(caseAssignmentRuleRequests);
        Test.stopTest();
        
        // Assert
        System.assert(true);
        // FIXME: Cannot do assert on the outcome of the assignment rule, as we don't know how those will behave in the future.
    }

    @IsTest
    static void runAssigmentRule_NamedRule() {
        // Arrange
        Case newCase = (Case) make(Entity.SERVICE_CASE, new Case());
        run();

        // Don't care which assignment rule we get, we just need a valid name.
        List<AssignmentRule> assignmentRules = [SELECT Name FROM AssignmentRule WHERE SobjectType = 'Case' LIMIT 1];
        AssignmentRule assignmentRule = (assignmentRules.size() == 1) ? assignmentRules.get(0) : null;

        // The invocable method only runs on already inserted cases
        CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest request = new CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest();
        request.aCase = newCase;
        if (assignmentRule != null) {
            request.assignmentRuleName = assignmentRule.Name;
        }

        List<CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest> caseAssignmentRuleRequests = new List<CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest>();
        caseAssignmentRuleRequests.add(request);

        // Act
        Test.startTest();
        CaseAssignmentRuleInvocableAction.runAssignmentRule(caseAssignmentRuleRequests);
        Test.stopTest();

        // Assert
        System.assert(true);
        // FIXME: Cannot do assert on the outcome of the assignment rule, as we don't know how those will behave in the future.
    }

    @IsTest
    static void runAssigmentRule_Default_Bulk() {
        // Arrange
        List<Case> cases = new List<Case>();
        for (Integer i = 0; i < totalCases; i++) {
            Case newCase = (Case) make(Entity.SERVICE_CASE, new Case());
            cases.add(newCase);
        }
        run();

        // The invocable method only runs on already inserted cases
        List<CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest> caseAssignmentRuleRequests = new List<CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest>();

        for (Case newCase : cases) {
            CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest request = new CaseAssignmentRuleInvocableAction.CaseAssignmentRuleRequest();
            request.aCase = newCase;
            request.assignmentRuleName = '';

            caseAssignmentRuleRequests.add(request);
        }

        // Act
        Test.startTest();
        CaseAssignmentRuleInvocableAction.runAssignmentRule(caseAssignmentRuleRequests);
        Test.stopTest();


        // Assert
        System.assert(true);
        // FIXME: Cannot do assert on the outcome of the assignment rule, as we don't know how those will behave in the future.
    }

    @TestSetup
    static void createData() {
        c_TestFactory.setDefaultContext();

        make(Entity.SERVICE_CASE, new Case());
        run();
    }
}