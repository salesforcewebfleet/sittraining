/**
 * @author        	fheeneman
 * @author       	Salesforce
 * @description	    Description
 * @see           	CDOSyncBatch_test Test class
 * @date 4/3/20    fheeneman    Initial setup
 */
public without sharing class CDOSyncBatch implements Database.Batchable<SObject> {

    /**
     * @description Initializes the scope of the batch job
     *
     * @param batchableContext
     *
     * @return Query Locator containing all the ACRs
     */
    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        String queryString = 'SELECT AccountId, ';
        for (String cdoField : CDOSync.getCDOMapping().values()) {
            queryString += cdoField + ',';
        }
        queryString = queryString.removeEnd(',');
        queryString += ' FROM AccountContactRelation';

        return Database.getQueryLocator(queryString);
    }

    /**
     * @description Executes the logic based on chunks of the scope
     *
     * @param batchableContext
     * @param scope Chunks of the ACRs provided by the start method
     */
    public void execute(Database.BatchableContext batchableContext, List<SObject> scope) {
        CDOSync.synchronizeFromACRs((List<AccountContactRelation>)scope);
    }

    /**
     * @description Could be used to clean up, not used for now
     *
     * @param batchableContext
     */
    public void finish(Database.BatchableContext batchableContext) {

    }

}