public class OptInByPhone {
    @InvocableMethod
    public static void OptInByPhone(List<OptInByPhoneRequest> requests) {
        OptInByPhonePardotForm form;

        for (OptInByPhoneRequest request : requests) {
            form = new OptInByPhonePardotForm();
            form.updateForm(request.email, request.country);
            form.submit();
        }
    }

    public class OptInByPhoneRequest {
        @InvocableVariable(label='Email' required=true)
        public String email;

        @InvocableVariable(label='Country' required=true)
        public String country;
    }
}