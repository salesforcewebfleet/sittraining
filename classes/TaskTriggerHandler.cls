public with sharing class TaskTriggerHandler extends TriggerHandler{  
    private List<Task> newTasks;
    private List<Task> oldTasks;
    private Map<Id, Task> newTasksByIds;
    private Map<Id, Task> oldTasksByIds;

    /**
     * @description Constructor. Sets trigger context variables to member variables
     */
    public TaskTriggerHandler() {
        this.newTasks = (List<Task>) Trigger.new;
        this.oldTasks = (List<Task>) Trigger.old;
        this.newTasksByIds = (Map<Id, Task>) Trigger.newMap;
        this.oldTasksByIds = (Map<Id, Task>) Trigger.oldMap;
    }

    /**
     * @description beforeInsert handler
     */
    protected override void beforeInsert() {
        TaskClassHandler.SetDueDateTimeonTask(null,newTasks);
    }
    
    /**
     * @description beforeUpdate handler
     */
    protected override void beforeUpdate(){
        TaskClassHandler.SetDueDateTimeonTask(oldTasksByIds, newTasks);
    }
}