global class DeleteWebfleetProductsBatch implements Database.Batchable<sObject>
{
                global Database.QueryLocator start(Database.BatchableContext BC)
                {
                               String query ='SELECT Id FROM WEBFLEET_Products__c';
                               return Database.getQueryLocator(query);
                               // If you use a QueryLocator object, the governor limit for the total number of records retrieved by SOQL queries is bypassed
                }

                // Without indicating the type of the contents of the List:
                // global void execute(Database.BatchableContext BC, List<sObject> scope)
                global void execute(Database.BatchableContext BC, List<WEBFLEET_Products__c> scope)
                {
                               delete scope;
                               // Alternatively: Database.delete(scope, true);
                }

                global void finish(Database.BatchableContext BC)
                {
                }
}