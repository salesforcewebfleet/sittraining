/**
 * @description Test class for WebfleetRegistrationHandler
 *
 * @author fheeneman Salesforce
 *
 * @date 24/6/20		fheeneman		Initial setup
 */
@IsTest
private class WebfleetRegistrationHandler_test extends c_TestFactory {

    private static final String WEBFLEET_ID = 'uniqueIdentifierFromWebfleet';

    @IsTest
    static void createUser_KnownSub() {
        // Arrange
        Auth.UserData userData = generateAuthUserDate();

        User existingUser = (User) make(Entity.STANDARD_USER, new User(WebfleetId__c = WEBFLEET_ID));
        run();

        // Act
        Test.startTest();
        WebfleetRegistrationHandler handler = new WebfleetRegistrationHandler();
        User result = handler.createUser(null, userData);
        Test.stopTest();

        // Assert
        System.assertEquals(existingUser.Id, result.Id);
    }

    @IsTest
    static void createUser_InactiveKnownSub() {
        // Arrange
        Auth.UserData userData = generateAuthUserDate();

        User existingUser = (User) make(Entity.STANDARD_USER, new User(WebfleetId__c = WEBFLEET_ID, IsActive = false));
        run();

        // Act
        Test.startTest();
        WebfleetRegistrationHandler handler = new WebfleetRegistrationHandler();
        User result = handler.createUser(null, userData);
        Test.stopTest();

        // Assert
        System.assertEquals(existingUser.Id, result.Id);
        System.assertEquals(true, result.IsActive);
    }

    @IsTest
    static void createUser_UnknownSub() {
        // Arrange
        Auth.UserData userData = generateAuthUserDate();

        // Act
        Test.startTest();
        WebfleetRegistrationHandler handler = new WebfleetRegistrationHandler();
        try {
            handler.createUser(null, userData);
            System.assert(false, 'Method did not throw an exception.');
        } catch (Exception e) {
            System.assertEquals(e.getTypeName(), 'WebfleetRegistrationHandler.RegistrationHandlerException', 'Method did not throw the expected exception');
        }
        Test.stopTest();

        // Assert
    }

    @IsTest
    static void updateUser_InactiveUser() {
        // Arrange
        User existingUser = (User) make(Entity.STANDARD_USER, new User(WebfleetId__c = WEBFLEET_ID, IsActive = false));
        run();

        // Act
        Test.startTest();
        WebfleetRegistrationHandler handler = new WebfleetRegistrationHandler();
        handler.updateUser(existingUser.Id, null, null);
        Test.stopTest();

        // Assert
        User result = [SELECT IsActive FROM User WHERE Id = :existingUser.Id LIMIT 1];
        System.assertEquals(true, result.IsActive);
    }

    @TestSetup
    static void createData() {
        c_TestFactory.setDefaultContext();
    }

    private static Auth.UserData generateAuthUserDate() {
        return new Auth.UserData(
                WEBFLEET_ID,
                null,
                null,
                null,
                'email@domain.invalid',
                null,
                null,
                'en',
                'WEBFLEET',
                null,
                new Map<String, String>()
        );
    }
}