public class TaskClassHandler{
	public static void setDueDateTimeonTask(Map<Id, Task> oldTasksMap, List<Task> newTask){
		if (oldTasksMap == null){
			for (Task t : newTask){
				if (t.DueDateTime__c != null){
					t.ActivityDate = t.DueDateTime__c.date();
				} else if (t.ActivityDate != null){
					t.DueDateTime__c = convertDateTimetoDate(t.ActivityDate);
				}
			}
		} else{
			for (Task t : newTask){
				Task oldTask = oldTasksMap.get(t.Id);
				if (t.DueDateTime__c != oldTask.DueDateTime__c){
					t.ActivityDate = t.DueDateTime__c.date();
				} else if (t.ActivityDate != oldTask.ActivityDate){
					t.DueDateTime__c = convertDateTimetoDate(t.ActivityDate);
				}
			}
		}
	}

	private static DateTime convertDateTimetoDate(Date datetoConvert){
		DateTime now = DateTime.now();
		Long timeOffset = DateTime.newInstance(now.date(), now.time()).getTime()-DateTime.newInstance(now.dateGmt(), now.timeGmt()).getTime();
		Integer hoursOffset = (Integer)timeOffset / 3600 / 1000 * -1;
		HoursOffset += 8;
		Datetime newDateTime = datetoConvert;
		newDateTime = newDateTime.addHours(HoursOffset);
		return newDateTime;
	}
}