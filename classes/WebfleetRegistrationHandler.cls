/**
 * @description Registration Handler associated with the WEBFLEET AuthProvider configuration
 *
 * @see WebfleetRegistrationHandler_test Test class
 *
 * @author fheeneman Salesforce
 *
 * @date 23/6/20    fheeneman    Initial setup
 */
public without sharing class WebfleetRegistrationHandler implements Auth.RegistrationHandler {
    /**
     * @description Is invoked when a user goes into the SSO flow and is not known yet (i.e. has no Third-Party Account Link)
     * <br />
     * Will try to find the corresponding Salesforce User based on the sub property from the Auth.UserData, if not found should throw an error.
     * <p>
     * Currently only works with existing users, could be expanded to create new users based on the UserInfo coming in,
      * but this is currently too summary to create this.
     * </p>
     * <br />
     * The only little trick we pull here is to activate the user if it is inactive, to work around the license limitation
     * the number of community login users for the data migration.
     *
     * @param portalId Id of the associated portal. The portalID value may be null or an empty key if there is no portal configured with this provider.
     * @param userData Information about the user retrieved from the User Info endpoint in the AuthProvider,
      * contains things like sub, email and locale of the user.
     *
     * @return the Salesforce User with the same WebfleetId as the provided sub property from the Auth.UserData
     */
    public User createUser(Id portalId, Auth.UserData userData) {
        List<User> users = new List<User>();

        if (User.SObjectType.getDescribe().isAccessible()) {
            users = [SELECT Id, IsActive FROM User WHERE WebfleetId__c = :userData.identifier LIMIT 1];
        }

        if (users.isEmpty()) {
            throw new RegistrationHandlerException(Label.WebfleetRegHandlerUserNotLinkedError);
        }
        if (!users[0].IsActive) {
            users[0].IsActive = true;
            update users[0];
        }
        return users[0];
    }

    /**
     * @description Could be used to update the specified user’s information.
     * This method is called if the user has logged in before with the authorization provider and then logs in again. <br />
     * Currently the UserDate contains too little information to do anything useful. <br />
     * <br />
     * The only little trick we pull here is to activate the user if it is inactive, to work around the license limitation
     * the number of community login users for the data migration.
     *
     * @param userId Salesforce User Id of the user that is logging in
     * @param portalId Id of the associated portal.
     * @param userData Information about the user retrieved from the User Info endpoint in the AuthProvider,
     * contains things like sub, email and locale of the user.
     */
    public void updateUser(Id userId, Id portalId, Auth.UserData userData) {
        // Required by the interface, can be expanded later to update the user.
        List<User> users = new List<User>();

        if (User.SObjectType.getDescribe().isAccessible()) {
            users = [SELECT Id, IsActive FROM User WHERE Id = :userId LIMIT 1];
        }
        User existingUser = (users.size() == 1) ? users.get(0) : null;

        if (existingUser != null && !existingUser.IsActive && User.SObjectType.getDescribe().isUpdateable()) {
            existingUser.IsActive = true;
            update existingUser;
        }
    }

    /**
     * @description Custom exception to throw during the SSO process. Is displayed to the user in the url.
     */
    class RegistrationHandlerException extends Exception {
    }
}