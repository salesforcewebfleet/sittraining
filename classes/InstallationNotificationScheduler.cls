/**
 * @description Class that schedules the InstallationNotificationBatch
 *
 * @see InstallationNotificationScheduler_test Test class
 *
 * @author fheeneman Salesforce
 *
 * @date 21/4/20    fheeneman    Initial setup
 */
public with sharing class InstallationNotificationScheduler implements System.Schedulable {

    /**
     * @description Schedules the InstallationNotificationBatch
     *
     * @param schedulableContext standard parameter
     */
    public void execute(SchedulableContext schedulableContext) {

        List<FulfillmentEmailSettings__mdt> fulfillmentEmailSettings = [SELECT BulkSize__c FROM FulfillmentEmailSettings__mdt WHERE DeveloperName = 'InstallationNotification' LIMIT 1];
        Integer batchSize = fulfillmentEmailSettings[0] != null && fulfillmentEmailSettings[0] != null ? Integer.valueOf(fulfillmentEmailSettings[0].BulkSize__c) : 200;
        Database.executeBatch(new InstallationNotificationBatch(), batchSize);
    }
}