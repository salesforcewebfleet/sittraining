@isTest
private class CreateApprovalHistoryDetailsTest {
    @testSetup
    static void createUserSetup() { 
        List<Profile> p1 = [Select Id from Profile where Name = 'System Administrator' limit 1];
        List<UserRole> r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'Board' LIMIT 1];
        User m1 = new User();
        m1.FirstName = 'Test';
        m1.LastName = 'User';
        m1.Alias = 'testUser';
        m1.Email = 'testUserPeter@gmail.com';
        m1.Username = 'testUserPeter@gmail.com';
        m1.CommunityNickname = 'testUser';
        m1.EmailEncodingKey = 'ISO-8859-1';
        m1.TimeZoneSidKey = 'Europe/Amsterdam';
        m1.LocaleSidKey = 'en_US';
        m1.LanguageLocaleKey = 'en_US';
        m1.CurrencyIsoCode = 'EUR';
        m1.ProfileId = p1 [0].Id;
        m1.UserRoleId = r1 [0].Id;
        insert m1;
        
    }
    @isTest
    static void testOpportunittyTrigger() {
        Opportunity opp = new Opportunity();
        opp.Name='testOpName';
        opp.CloseDate = Date.today();
        opp.StageName='Demo & Propose';
        opp.Requested_Discount__c = 'Grace period up to 2 month';
        opp.Product_Tariff__c = 'Who design this madness?';
        opp.Reason_for_the_request__c = 'A madperson for sure';
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', Family = 'Hardware');
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry pbEntry = new PricebookEntry(Pricebook2Id = pricebookId,Product2Id = prod.Id,UnitPrice = 100.00,IsActive = true);
		insert pbEntry;
		OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID=opp.Id,Product2Id = prod.Id, quantity=4, totalprice=200, PricebookEntryId  = pbEntry.Id);
		insert lineItem1;
        User user1 = [SELECT Id FROM User WHERE Alias='testUser'];
        // Create an approval request for the // Create an approval request for the account
        Approval.ProcessSubmitRequest req1 =
        new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(opp.id);

        // Submit on behalf of a specific submitter
        req1.setSubmitterId(user1.Id);
        
        // Submit the record to specific process and skip the criteria evaluation
        List<ProcessDefinition> processList = [SELECT DeveloperName FROM ProcessDefinition WHERE State = 'Active' AND TableEnumOrId = 'Opportunity'];
        try{
            if(!processList.isEmpty()){
                req1.setProcessDefinitionNameOrId(processList[0].DeveloperName);
                req1.setSkipEntryCriteria(true);
        
                // Submit the approval request for the account
                Approval.ProcessResult result = Approval.process(req1);
        
                // Verify the result
                System.assert(result.isSuccess());
                opp.Step_Approved__c = true;
                update opp;
        	}
        } catch(Exception e) {
            System.debug('The following exception has occurred: ' + e.getMessage());}
	}
}