@isTest
private class NpsTouchpointHandlerTest extends c_TestFactory{
    
    // Bulkify or not? Look at the settings on the TestFactory to decide how much data to build
    static Integer totalAccounts = c_TestFactory.BULKIFY_TESTS ? 100 : 2;
    
    @IsTest
    static void happyFlow(){
        List<TouchPoint__c> tpList = new List<TouchPoint__c>();
        
        tpList.add(new TouchPoint__c(Name='Test Mule Touchpoint EU',
                                            Type__c = 'NPS',
                                            Active__c = true,
                                           ContainsConfig__c = true,
                                           Countries__c = 'AT:BE:CZ:DK:FI:FR:DE:IE:IT:LU:MC:NL:NO:PL:PT:SM:ES:SE:CH:GB',
                                     	   EndDate__c = Date.today(),
                                           ProviderExternalId__c = '6547689878',
                                           StartDate__c = Date.today(),
                                           ScheduledPushTime__c = Time.newInstance(8, 0, 0, 0)));
        
          tpList.add(new TouchPoint__c(Name='Test Mule Touchpoint INT',
                                     Type__c = 'NPS',
                                     Active__c = true,
                                     Countries__c = 'AU:CA:CL:NZ:MX:US:ZA',
                                     EndDate__c = Date.today(),
                                     ProviderExternalId__c = '56',
                                     StartDate__c = Date.today(),
                                     ScheduledPushTime__c = Time.newInstance(15, 0, 0, 0)));
        
        insert tpList;
        List<Account> resellerAccounts = [SELECT Id FROM Account WHERE Type = 'Reseller'];
        List<Account> customerAccounts = [Select Reseller_Name__c FROM Account WHERE Type ='Customer'];
        for(Integer i = 0; i < resellerAccounts.size(); i++){
            customerAccounts[i].Reseller_Name__c = resellerAccounts[i].Id;
        }
        update customerAccounts;
        Database.executeBatch(new NpsTouchpointBatch(), 50);
    }
    
    @TestSetup
    static void createData() {
        c_TestFactory.setDefaultContext();

        List<Account> accounts = new List<Account>();
        for (Integer i = 0; i < totalAccounts; i++) {
            Account account = (Account) make(Entity.SERVICE_ACCOUNT, new Account());
            account.Type = 'Reseller';
            account.U_RID__c = '524546'+i;
            accounts.add(account);
        }
        
        for(Integer i = 0; i < totalAccounts; i++) {
            Account account = (Account) make(Entity.SERVICE_ACCOUNT, new Account(Reseller_Name__c = accounts.get(i).Id));
            System.debug('account createData :'+account);
            account.Type = 'Customer';
            account.Status__c = 'Active';
            account.WEBFLEET_Units_Active__c  = 10;
            account.WEBFLEET_Customer_Number__c = '342'+i;
            account.LastNPSCalculation__c = null;
            accounts.add(account);
        }

        List<Contact> contacts = new List<Contact>();
        for (Account account : accounts) {
            Contact c = (Contact) make(Entity.SERVICE_CONTACT, new Contact(Account = account));
            c.IsActive__c = true;
            c.Language__c = 'de_de';
            c.NPSSurveyOptOut__c = false;
            c.Country__c = 'DE';
            c.IsSurveyBounced__c = false;
            c.DateNPSSent__c = null;
            contacts.add(c);
        }
        run();
    }
}