public without sharing class NpsTouchpointHandler{
    static List<TouchPoint__c> toucpList = new List<TouchPoint__c>();

    public static Decimal calculatePercentageAudience(){
        Decimal selectionPercentage = 0;
        toucpList = [SELECT Id, AlreadyContacted__c, CurrentAudience__c, LastExecutionDate__c, MonthlyDesiredAudience__c, SelectionPercentage__c, ScheduledPushTime__c, Countries__c, ContainsConfig__c
                     FROM TouchPoint__c
                     WHERE Active__c = true and (StartDate__c <= Today or StartDate__c = null) and (EndDate__c >= Today or EndDate__c = null) and ProviderExternalId__c != null and Type__c = 'NPS'
                     ORDER BY ContainsConfig__c DESC];
        if(!toucpList.isEmpty()){
            TouchPoint__c toucpConfig = toucpList[0];
            Date lastEx = toucpConfig.LastExecutionDate__c;
            selectionPercentage = toucpConfig.SelectionPercentage__c;
            // If more than 21 days have been passed between executions
            // calculate again the audience, selection and selection %
            if (lastEx == null || selectionPercentage == null || lastEx.daysBetween(Date.today()) > 21){
                Decimal currentAudience = [SELECT count()
                                           FROM Contact
                                           WHERE IsNPSValid__c = true];
                toucpConfig.CurrentAudience__c = currentAudience;
                Decimal alreadyContacted = [SELECT count()
                                            FROM Contact
                                            WHERE DateNPSSent__c != null AND IsNPSValid__c = false];
                toucpConfig.AlreadyContacted__c = alreadyContacted;
                Decimal monthlyDesiredAudience = (currentAudience + alreadyContacted) / 12.0;
                //desired number of Contacts to send the Survey
                monthlyDesiredAudience = monthlyDesiredAudience.round(System.RoundingMode.UP);
                toucpConfig.MonthlyDesiredAudience__c = monthlyDesiredAudience;
                List<Account> uniqueAccountList = [SELECT A.Id
                                                   FROM Account A
                                                   WHERE A.Id IN (SELECT B.AccountId FROM Contact B WHERE B.IsNPSValid__c = true)];
                if(!uniqueAccountList.isEmpty()){
                    selectionPercentage = monthlyDesiredAudience / uniqueAccountList.size() * 100.0;
                    toucpConfig.SelectionPercentage__c = selectionPercentage;
                    toucpConfig.LastExecutionDate__c = Date.today();
                    update toucpConfig;
                }
        	}
        }
        
        return selectionPercentage;
    }

    public static void filterContacts(List<Account> accountList, Decimal percentage){
        List<TouchPointParticipant__c> tppList = new List<TouchPointParticipant__c>();
        Date scheduledDate = getFirstMondayAfter9();
        for (Account resellerAcc : accountList){
            List<Contact> oneContactPerAccount = new List<Contact>();
            List<Contact> contactList = [SELECT Id, FirstName, LastName, Email, Language__c, AccountId, Country__c
                                         FROM Contact
                                         WHERE Account.Reseller_Name__c = :resellerAcc.Id AND IsNPSValid__c = TRUE
                                         ORDER By Country__c, account.DateLastNPSSent__c ASC
                                         NULLS FIRST, DateNPSSent__c ASC
                                         NULLS FIRST];
            Set<Id> accountSetId = new Set<Id>();
            Map<String, Integer> countByCountry = new Map<String, Integer>();
            for (Contact c : contactList){
                if (!accountSetId.contains(c.AccountID)){
                    accountSetId.add(c.AccountID);
                    oneContactPerAccount.add(c);
                    if (countByCountry.containsKey(c.Country__c)){
                        Integer countCountry = countByCountry.get(c.Country__c);
                        countCountry++;
                        countByCountry.put(c.Country__c, countCountry);
                    } else{
                        countByCountry.put(c.Country__c, 1);
                    }
                }
            }
            Integer counterByCountry;
            Integer numberPerCountry;
            String previousCountry = '';
            TouchPoint__c tmpT;
            Datetime scheduledDatetime; 
            
            for (Contact c2 : oneContactPerAccount){
                if (previousCountry != (String)c2.Country__c){
                    previousCountry = c2.Country__c;
                    counterByCountry = 0;
                    Decimal countCountry2 = (Decimal)countByCountry.get(c2.Country__c);
                    numberPerCountry = (Integer)(countCountry2 * percentage / 100.0).round(System.RoundingMode.UP);
                    tmpT = getTouchPointbyCountry(toucpList, c2.Country__c);
                    scheduledDatetime = DateTime.newInstance(scheduledDate, tmpT.ScheduledPushTime__c);
                }
                // Insert as a TouchPoint
                if (counterByCountry < numberPerCountry && tmpT != null ){
                    TouchPointParticipant__c tmp = new TouchPointParticipant__c(Email__c = c2.Email,
                                                                                FirstName__c = c2.FirstName,
                                                                                LastName__c = c2.LastName,
                                                                                Account__c = c2.AccountId,
                                                                                Contact__c = c2.Id,
                                                                                Language__c = c2.Language__c,
                                                                                Country__c = c2.Country__c,
                                                                                Status__c = 'Added',
                                                                                ScheduleDate__c = scheduledDatetime,
                                                                                TouchPoint__c = tmpT.Id);
                    tppList.add(tmp);
                    counterByCountry++;
                }
            }
            resellerAcc.LastNPSCalculation__c = Date.today();
        }
        insert tppList;
        Database.update(accountList,false);
    }

    private static TouchPoint__c getTouchPointbyCountry(List<TouchPoint__c> toucpList, String country){
        for (TouchPoint__c t : toucpList){
            if (t.Countries__c.contains(country))
                return t;
        }
        return null;
    }

    private static Date getFirstMondayAfter9(){
        //get first Monday of the Month, if running user has European Locale
        Date desiredMonday = Date.today().toStartOfMonth().toStartOfWeek();
        //add a week if "desiredMonday" is in last month
        while (desiredMonday.day() < 9)
            desiredMonday = desiredMonday.addDays(7);
        return desiredMonday;
    }
}