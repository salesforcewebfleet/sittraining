/**
 @Name: c_TestFactory_SalesCloud.cls
 @TestedBy: Implementing in a test class
 @Description:
  Example TestFactory for Sales Cloud objects
 @History
  VERSION     AUTHOR           DATE            DETAIL
  1.0         Matt Evans       7/12/2018       Draft
  1.1         Matt Evans       9/9/2019        Fixed reflection issues in factory classes, removed fix and simplified the Customer template 
*/

public class c_TestFactory_ServiceCloud {
    /**
    * Atomic Data; ex. an Account, Contact 
    **/

    /*** Service Account ***/
    public class ServiceAccount extends c_TestFactoryMaker {

        // Mandatory minimum default set up, returns an sObject, in this case a default Account for the Service Cloud
        SObject defaults() {
            

            // Default object
            Account rec = new Account();

            // Default values
            rec.Name = Math.random() + 'A Customer Account';
            rec.ShippingStreet = 'Nr 1 Some Street';
            rec.ShippingPostalCode = '11111';
            rec.ShippingCity = 'A City';
            rec.ShippingCountry = COUNTRY_NAME;
			rec.RecordTypeId=  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
            
            return (SObject) rec;
        }
    }

    /*** Service Contact ***/
    public class ServiceContact extends c_TestFactoryMaker {

        // Mandatory minimum default set up, returns an sObject, in this case a default Contact for the Service Cloud
        SObject defaults() {
            // Default object
            Contact rec = new Contact();

            rec.FirstName = Math.random() +'John';
            rec.LastName = Math.random() +'Doe';
            rec.Email = Math.random() + 'myemail@address.invalid';
            rec.Country__c = 'DE';
            
            return (SObject) rec;
        }
    }

    /**
     * @description Service Case
     **/
    public class ServiceCase extends c_TestFactoryMaker {

        // Mandatory minimum default set up, returns an sObject, in this case a default Case for the Service Cloud
        SObject defaults() {
            // Default object
            Case rec = new Case();
            rec.Type = 'Support Case';

            return (SObject) rec;
        }
    }

    /**
     * @description WorkOrder template
     **/
    public class ServiceWorkOrder extends c_TestFactoryMaker {

        // Mandatory minimum default set up, returns an sObject, in this case a default Work Order for the Service Cloud
        SObject defaults() {
            // Default object
            WorkOrder rec = new WorkOrder();

            return (SObject) rec;
        }
    }

    /**
     * @description CDO template. Might move this to a different class later, such as marketing.
     */
    public class CDO extends c_TestFactoryMaker {
        SObject defaults() {
            return (SObject) new CDO__c();
        }
    }

    /**
     * @description ACR template. Might move this to a different class later, such as marketing.
     */
    public class ServiceAccountContactRelation extends c_TestFactoryMaker {
        SObject defaults() {
            return (SObject) new AccountContactRelation();
        }
    }

    /**
     * @description Full customer tree template; Account, Contact, Case, perhaps Work Order
     */
    public class ServiceCustomer extends c_TestFactoryMaker {
        // Mandatory minimum default set up, return null for complex objects
        SObject defaults() {
            return null;
        }

        // Custom override for the maker
        public override SObject make(SObject sourceObject) {
            // Create an account
            Account customerAccount = (Account) c_TestFactory.make(Entity.SERVICE_ACCOUNT, (Account)sourceObject);

            // Create contacts for the account and add them to the factory
            Integer contactsToCreate = 2;
            List<Contact> contacts = new List<Contact>();
            for (Integer i = 0; i<contactsToCreate; i++)
            {
                contacts.add((Contact)c_TestFactory.make(Entity.SERVICE_CONTACT, new Contact(Account = customerAccount)));
            }

            // Create Cases for the account and add them to the factory
            Integer casesToCreate = 2;
            for (Integer i = 0; i<casesToCreate; i++)
            {
                SObject rec = new Case(Account = customerAccount, Contact = contacts[i]);

                // Case Process Builder expects a queue as the owner when the case is created, so we need to run the assignment rules.
                Database.DMLOptions dmlOptions = new Database.DMLOptions();
                dmlOptions.assignmentRuleHeader.useDefaultRule = true;
                rec.setOptions(dmlOptions);

                Case serviceCase = (Case)c_TestFactory.make(Entity.SERVICE_CASE, rec);

                // Add a work order for evey case
                c_TestFactory.make(Entity.SERVICE_WORK_ORDER, new WorkOrder(Case = serviceCase));
            }

            // Return the passed Account object as a root reference
            return (SObject) customerAccount;
        }

    }
    
public class CustomerAccount extends c_TestFactoryMaker {

        // Mandatory minimum default set up, returns an sObject, in this case a default Account for the Service Cloud
        SObject defaults() {

            // Default object
            Account rec = new Account();

            // Default values
            rec.Name = Math.random() + 'A Customer Account';
            rec.ShippingStreet = 'Nr 1 Some Street';
            rec.ShippingPostalCode = '11111';
            rec.ShippingCity = 'A City';
            rec.ShippingCountry = COUNTRY_NAME;
			rec.RecordTypeId=  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
            
            return (SObject) rec;
        }
    }
    
    /*** Reseller Account ***/
    public class ResellerAccount extends c_TestFactoryMaker {

        // Mandatory minimum default set up, returns an sObject, in this case a default Account for the Service Cloud
        SObject defaults() {
          
            // Default object
            Account rec = new Account();

            // Default values
            rec.Name = Math.random() + 'A Reseller Account';
            rec.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Reseller').getRecordTypeId();            
            
            return (SObject) rec;
        }
    }
    
        public class WebfleetContract extends c_TestFactoryMaker {

        // Mandatory minimum default set up, returns an sObject, in this case a default Account for the Service Cloud
        SObject defaults() {


            // Default object
            WEBFLEET_Contract__c cont = new WEBFLEET_Contract__c();

            // Default values
            cont.Name = 'UK00-08774';
            cont.ContractStatus__c = 'Installation Case Created';
            cont.ContractType__c = 'Sales';
            cont.ContractDate__c=date.today()-10;
            cont.ContractApprovalDate__c=date.today()-5;
            cont.ContractShippedDate__c=date.today();
            
            return (SObject) cont;
        }
    }
        
    public class Shipment extends c_TestFactoryMaker {

        // Mandatory minimum default set up, returns an sObject, in this case a default Account for the Service Cloud
        SObject defaults() {
                      
                // Default object
                SHIPMENT__c shipment = new SHIPMENT__c();
                   
                // Default values
            	shipment.DeliveryStatus__c='Completed';
            	shipment.CarrierCode__c='UPS';
            	shipment.CarrierTracking__c='1Z57E3W76801836749';
            	shipment.AlternativeShipToName__c='ATTN Graham Brewin/ TTT 0791755757';
            	shipment.Name='80513044';
                
                return (SObject) shipment;
        }
     
 }
    
    
    public class ShipmentItem extends c_TestFactoryMaker {

        // Mandatory minimum default set up, returns an sObject, in this case a default Account for the Service Cloud
        SObject defaults() {
            
            // Default object
            ShipmentItem__c shipmentItemrecord = new ShipmentItem__c();   
            
            // Default values
            shipmentItemrecord.Name='LINK510';
            shipmentItemrecord.ShipmentQuantity__c=4;
            
            return (SObject) shipmentItemrecord;
        }
     
 }
       public class ServiceEntitlement extends c_TestFactoryMaker {

        // Mandatory minimum default set up, returns an sObject, in this case a default Account for the Service Cloud
        SObject defaults() {
            
             // Default object
            Entitlement entitlementrecord = new Entitlement();

            // Default values
            entitlementrecord.Name='Webfleet SLA';
            entitlementrecord.Type='Phone Support';
            
            return (SObject) entitlementrecord;
        }
     
 }    
}