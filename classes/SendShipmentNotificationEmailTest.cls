/**
 * @author      msalmenautio
 * @author      Salesforce
 * @description Test class for SendShipmentNotficationEmail
 * @date 25/3/20		msalmenautio		Initial setup
 */
@IsTest
private class  SendShipmentNotificationEmailTest extends c_TestFactory {
    
    @TestSetup
    static void createData() {
       		 	c_TestFactory.setDefaultContext();

       		 	//Create Customer Account
       		 	Account[] accounts = new List<Account>();
       		 	Account account = (Account) make(Entity.CUSTOMER_ACCOUNT, new Account());
       		 	accounts.add(account);
        
       		 	//Create a contact ans link it to account
       			List<Contact> contacts = new List<Contact>();
       			for (Account acc : accounts) {
           		 	contacts.add((Contact) make(Entity.SERVICE_CONTACT, new Contact(Account = acc)));
           			 make(Entity.CDO, new CDO__c(Account__r = acc));
       			}
        
        		// Create a Reseller Account
       			Account[] reselleraccounts = new List<Account>();
				Account resaccount = (Account) make(Entity.RESELLER_ACCOUNT, new Account());
        
      
       		 	//Create Entitlement
       		 	List <Entitlement> entitlements = new List<Entitlement>();
       		 	for (Account acc : accounts) {
          		  	entitlements.add((Entitlement) make(Entity.SERVICE_ENTITLEMENT, new Entitlement(Account = acc)));
       		 	}
        
 
       		 	// Add Records
       		 	TriggerHandler.bypass('AccountContactRelationTriggerHandler');
       		 	TriggerHandler.bypass('CDOTriggerHandler');
      		 	run();     
     
        		// Create a  Webfleet Contract
				WEBFLEET_Contract__c[] Webfleetcontracts = new List<WEBFLEET_Contract__c>();
				for (Account acc : accounts) {
					WEBFLEET_Contract__c Webfleetcontract = (WEBFLEET_Contract__c) make(Entity.WEBFLEETCONTRACT, new WEBFLEET_Contract__c(Customer__c = acc.id));
					Webfleetcontracts.add(Webfleetcontract);
				}
       		 	run();
        
       			// Create a  Shipment Shipment__c
       			Shipment__c[] shipments = new List<Shipment__c>();
        		for (WEBFLEET_Contract__c acc : Webfleetcontracts) {
					Shipment__c shipment = (Shipment__c) make(Entity.SHIPMENT, new Shipment__c(WEBFLEETContractId__c=acc.id));
        			shipments.add(shipment);
       		 	}
        		run();
        
       			// Create Shipment Item
        
        		ShipmentItem__c[] shipmentitems = new List<ShipmentItem__c>();
        		for (Shipment__c acc : shipments) {
					ShipmentItem__c shipmentitem = (ShipmentItem__c) make(Entity.SHIPMENTITEM, new ShipmentItem__c(ShipmentId__c=acc.id));
        			shipmentitems.add(shipmentitem);
        		}       
        
       			// We don't want the CDO Sync to be trigger right now, because we need to test it in our methods.

        		run();
        		TriggerHandler.clearBypass('AccountContactRelationTriggerHandler');
        		TriggerHandler.clearBypass('CDOTriggerHandler');
            
    }
    
    @IsTest
    static void SendShipmentNotficationTest() {
        
        	Id contactId;
        	Id accountId;

			// Create data to WebfleetIds custom metadata. This is needed for a process builder process which is trigger by case creation.    
        	WebfleetIds__mdt WebfleetIds = new WebfleetIds__mdt();
        	WebfleetIds.DeveloperName = 'WebfleetIds';
        	WebfleetIds.Label = 'Webfleet Ids';
        	WebfleetIds.WebfleetSLAId__c ='55063000000J41nAAC';  
        
        	//Select account for a case        
        	For (Contact cont :[Select Id, AccountId From Contact LIMIT 1]){
        		contactId=cont.Id;
            	accountId=cont.AccountId;        
        	}
			
        	List<case> caselist = new List<Case>();
        	List<Id> caselistId = new List<Id>();
        	
			//Create case record        
       		Case caserecord = new Case();
            	caserecord.BusinessArea__c='Sales Support';
            	caserecord.subject='Test Data Case';
            	caserecord.ContactId=contactId;
            	caserecord.AccountId=accountId;
				caserecord.IssueType__c='Fulfillment';
            	caserecord.Type='Support Case';
 				//caserecord.Additionallnformation__c='Test Case';
        		Database.DMLOptions dmo = new Database.DMLOptions();
       			dmo.assignmentRuleHeader.useDefaultRule = true;
        		caserecord.setoptions(dmo);
        	insert caserecord ;
        	caselistId.add(caserecord.Id);
        	caselist.add(caserecord);
        
        	//Link case record to a shipment record
        	List<Shipment__c> Shipments = new List<Shipment__c>();
        	For (Shipment__c shipment :[Select Id from Shipment__c LIMIT 1]) {            
            		shipment.Case__c = caserecord.Id;
            		Shipments.add(shipment);
            }
        	update shipments;
        	 
        	
        
            Test.startTest();
        	SendShipmentNotificationEmail.SendShipmentNotification(caselist);
        	SendShipmentNotificationEmail.CreateShipmentNotification(caselistId);
        	system.assertEquals(2, Limits.getFutureCalls());
        	Test.stopTest();
        
    }
   
}