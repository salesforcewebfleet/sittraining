/*
Author: Abhijit Ghanekar
######################################################################################
SFT-153 - Head of finance field is added in Org Unit object. Contract record to be shared with head of finance.


*/
global class ContractSharingBatch implements Database.Batchable<sObject> 
{

Set<Id> ActivConts = new Set<Id>();
Set<Id> OrgUnitIds = new Set<Id>();
Map<Id,Id> OrgUnit = new Map<Id,Id>();
Map<Id,Id> OwnerMap = new Map<Id,Id>();
Map<Id,Id> Controller = new Map<Id,Id>();
Map<Id,Id> ControllerMap = new Map<Id,Id>();
Map<Id,Id> StreamOwner = new Map<Id,Id>();
Map<Id,Id> StreamOwnerMap = new Map<Id,Id>();
//SFT-153.sn
Map<Id,Id> HeadFinance = new Map<Id,Id>();
Map<Id,Id> HeadFinanceMap = new Map<Id,Id>();
//SFT-153.en
list<TT_Contract__Share> delshare = new list<TT_Contract__Share>();

public static Set<Id> OwnerDrillMap = new Set<Id>();
public static Boolean success=false;
public static list<TT_Contract__Share> listShare = new list<TT_Contract__Share>();

    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        String query = 'select Id,Org_Unit__c,OwnerId from TT_Contract__c where Restricted_Access__c = false';
        return Database.getQueryLocator(query);
    }


    global void execute(Database.BatchableContext BC, List<sObject> scope) 
    {
     for(sObject s : scope)
     {
        TT_Contract__c cont = (TT_Contract__c)s;
        ActivConts.add(cont.Id);
        OrgUnit.put(cont.Org_Unit__c,cont.Id);
        OwnerMap.put(cont.Id,cont.OwnerId);
        OrgUnitIds.Add(cont.Org_Unit__c);
     }
        
        //prepare controller map from Org Unit 
        list<Org_Unit__c > OrgList = [select Id,Controller__c,Stream_Owner__c,Head_of_Finance__c from Org_Unit__c where Id IN :OrgUnitIds]; //SFT-153.n
        if (OrgList.size() > 0)
        {//start if 2
        
        for (Org_Unit__c Orglist1:OrgList)
        {
        Controller.put(Orglist1.Id,Orglist1.Controller__c);
        StreamOwner.put(Orglist1.Id,Orglist1.Stream_Owner__c);
        HeadFinance.put(Orglist1.Id,Orglist1.Head_of_Finance__c);  //SFT-153.n
        }
         
        }//end if 2
        
        //head finance Map prepare SFT-153.sn
        for (Id id1:HeadFinance.keySet())
        {
        
           for (Id id2:OrgUnit.keySet())
           {
        
               if (id1.equals(id2))
               {
                   HeadFinanceMap.put(OrgUnit.get(id2),HeadFinance.get(id1));
                   break;
               }
           
           }
        
        }
        //SFT-153.en
        
        for (Id id1:Controller.keySet())
        {
        
           for (Id id2:OrgUnit.keySet())
           {
        
               if (id1.equals(id2))
               {
                   ControllerMap.put(OrgUnit.get(id2),Controller.get(id1));
                   break;
               }
           
           }
        
        }
        //end of controller map prepare
        
        //Stream Owner Map prepare
        for (Id id1:StreamOwner.keySet())
        {
        
           for (Id id2:OrgUnit.keySet())
           {
        
               if (id1.equals(id2))
               {
                   StreamOwnerMap.put(OrgUnit.get(id2),StreamOwner.get(id1));
                   break;
               }
           
           }
        
        }
        //end of Stream Owner Map prepare //SFT-153.n added Head Of Finance RowCause in where clause
        list<TT_Contract__Share> contshare = [select Id,ParentId,RowCause from TT_Contract__Share where ParentId IN :ActivConts and RowCause IN ('Hierarchy__c','Controller__c','Stream_Owner__c','Head_of_Finance__c')];
        if (contshare.size() > 0)
        {
            for (TT_Contract__Share delshare1:contshare)
            {
            //add records to be deleted from sharing object to list
            delshare.add(delshare1);
            
            }
            
            if (delshare.size() > 0)
            {
            list<Database.DeleteResult> sr1 = Database.delete(delshare,false);
            
            }
        }
        //SFT-153 added HeadFinanceMap in if condition and also as a parameter for ShareRecord
        if (ControllerMap.size() > 0 || OwnerMap.size() > 0 || StreamOwnerMap.size() > 0 || HeadFinanceMap.size() > 0) //SFT-153.n
        {
           ShareRecord(ControllerMap,OwnerMap,StreamOwnerMap,HeadFinanceMap); //SFT-153.n
           ActivConts.Clear();
           OrgUnitIds.Clear();
           OrgUnit.Clear();
           OwnerMap.Clear();
           Controller.Clear();
           ControllerMap.Clear();
           StreamOwner.Clear();
           StreamOwnerMap.Clear();
           HeadFinance.Clear();
           HeadFinanceMap.Clear();
           OwnerDrillMap.Clear();
           delshare.Clear();
           listShare.Clear();
           
        }

}   

public static void ShareRecord(Map<Id,Id> ControllerMap, Map<Id,Id> OwnerMap,Map<Id,Id> StreamOwnerMap,Map<Id,Id> HeadFinanceMap)
{
//start of ShareRecord
    //Ownersharing
    for (Id recordid:OwnerMap.keySet())
    {
        ProcessSharing(recordid,OwnerMap.get(recordid),'Owner');
    
    }
    if (ControllerMap.size() > 0 )
    {
    for (Id recordid:ControllerMap.keySet())
    {
        
        ProcessSharing(recordid,ControllerMap.get(recordid),'Controller');
    }
    
    }
    
    if (StreamOwnerMap.size() > 0 )
    {
    for (Id recordid:StreamOwnerMap.keySet())
    {
        
        ProcessSharing(recordid,StreamOwnerMap.get(recordid),'StreamOwner');
    }
    }
    //SFT-153.sn
    if (HeadFinanceMap.size() > 0 )
    {
    for (Id recordid:HeadFinanceMap.keySet())
    {
        
        ProcessSharing(recordid,HeadFinanceMap.get(recordid),'HeadFinance');
    }

    }
    //SFT-153.en
    
    list<Database.SaveResult> sr = Database.insert(listShare,false);
    
    for (Database.SaveResult sr1:sr)
    {
        if (sr1.isSuccess())
        {
            success = true;
        }
        else
        {
          Database.Error err = sr1.getErrors()[0];

         if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  && 
                  err.getMessage().contains('AccessLevel')){
            success = true;
         }
         else{

            success = false;
          
            }
        }
    
    }

}
//end of ShareRecord method

public static void ProcessSharing(Id recordid, Id ownerid, String typeOfSharing)
{
//start ProcessSharing
  if (typeOfSharing == 'Owner')
  {
     OwnerSharingDrillDown(ownerid);
     OwnerSharingProcess(recordid,OwnerDrillMap);
     OwnerDrillMap.Clear();
  }
  if (typeOfSharing == 'Controller')
  {
    AddSharing(recordid,ownerid,'Controller');
  
  }
  if (typeOfSharing == 'StreamOwner')
  {
    AddSharing(recordid,ownerid,'StreamOwner');
  }
  if (typeOfSharing == 'HeadFinance')
  {
     AddSharing(recordid,ownerid,'HeadFinance');
  }
}
//end ProcessSharing
private static void OwnerSharingDrillDown(Id ownerid)
{
//Start of OwnerSharingDrillDown
     Boolean Noexit=true;
     Id tempid = ownerid;
     Integer count=0;
     
     do
     {
     
     list<User> u = [select Id, ManagerId from User where Id =:tempid LIMIT 1];
     
     if (u.size() > 0)
     {// if user exists
     
         if (u[0].ManagerId == null || count == 5)
         {
            Noexit = false;
         } 
         else
         {
          tempid = u[0].ManagerId;
          OwnerDrillMap.add(tempid);
         }
     }// end if user exists
     if (count == 5 || u.size() <=0)
     {
         Noexit = false;
     }
     count = count + 1;
     }
     while (Noexit);

}
//end of OwnersharingDrillDown
public static void OwnerSharingProcess(Id recordid,Set<Id> OwnerDrillMap)
{
    for (Id ownerid:OwnerDrillMap)
    {
        AddSharing(recordid,ownerid,'Owner');
    
    }

}
public static void AddSharing(Id recordid, Id ownerid, String type)
{//start of addsharing
    String rowCause;

    TT_Contract__Share contShare = new TT_Contract__Share();
    contShare.ParentId = recordid;
    contShare.UserOrGroupId = ownerid;
    contShare.AccessLevel = 'Read';
    
    if (type.equals('Owner'))
    {
        contShare.RowCause = TT_Contract__Share.RowCause.Hierarchy__c;
    }
    else if (type.equals('Controller'))
    {
        contShare.RowCause = TT_Contract__Share.RowCause.Controller__c;

    }
    else if (type.equals('StreamOwner'))
    {
        contShare.RowCause = TT_Contract__Share.RowCause.Stream_Owner__c;
    }
    else
    {
        contShare.RowCause = TT_Contract__Share.RowCause.Head_of_Finance__c;
    }
     listShare.add(contShare);
 
    
    
}//end of addsharing





    global void finish(Database.BatchableContext BC) 
    {
    }
}