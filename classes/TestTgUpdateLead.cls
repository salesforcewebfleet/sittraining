@isTest
private class TestTgUpdateLead {
    static testMethod void myUnitTest() {

    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        Campaign c = new Campaign(); 
        c.name='NameTestCampaign';
        c.Region__c='Benelux';
        insert c;
                
        Lead l = new Lead(); 
        l.LastName='NameTest';
        l.Company ='CompanyTest';
        l.Status='Nuevo';
        l.Appointment_date__c=null;
        l.Campaign_Id__c= c.id;
        insert l;
        
        Task t = new Task(); 
        t.Type ='Appointment made – Inside Sales';
        t.Subject= 'TestStatus';
        t.Status = 'Pendiente'; 
        t.Priority = 'Normal'; 
        t.OwnerId = UserInfo.getUserId(); 
        t.WhoId = l.id; 
      
        insert t;
        

       l = [SELECT Appointment_date__c FROM Lead WHERE Id =:l.Id];
      
       // Test that the trigger correctly updated appointment date
       System.assertEquals(system.today(), l.Appointment_date__c);

        

}
}