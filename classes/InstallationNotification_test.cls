/**
 * @description Test class for InstallationNotificationHandler
 *
 * @see InstallationNotificationHandler
 *
 * @author fheeneman Salesforce
 *
 * @date 24/4/20		fheeneman		Initial setup
 */
@IsTest
private class InstallationNotification_test extends c_TestFactory {
    // Bulkify or not? Look at the settings on the TestFactory to decide how much data to build
    static Integer totalCases = c_TestFactory.BULKIFY_TESTS ? 20 : 2;

    @IsTest
    static void testEmailWorkOrder() {
        // Arrange
        List<WorkOrder> workOrders = [SELECT CaseId, Case.ContactId FROM WorkOrder];

        // Act
        Test.startTest();
        InstallationNotificationHandler.sendInstallationAssignmentEmail(workOrders);
        Test.stopTest();

        // Assert
        List<EmailMessage> emailMessages = [SELECT Id FROM EmailMessage];

        // Every case has 1 related work order, so for every work order there should be 1 email
        System.assertEquals(workOrders.size(), emailMessages.size());
    }

    @IsTest
    static void testEmailMultipleWorkOrdersPerCase() {
        // Arrange
        List<WorkOrder> workOrders = [SELECT CaseId, Case.ContactId FROM WorkOrder];

        WorkOrder workOrder = (WorkOrder) make(Entity.SERVICE_WORK_ORDER, new WorkOrder(Case = workOrders[0].Case));
        run();
        workOrders.add(workOrder);

        // Act
        Test.startTest();
        InstallationNotificationHandler.sendInstallationAssignmentEmail(workOrders);
        Test.stopTest();

        // Assert
        List<EmailMessage> emailMessages = [SELECT Id, ParentId FROM EmailMessage];
        List<EmailMessage> caseEmail = [SELECT Id, ParentId FROM EmailMessage WHERE ParentId = :workOrders[0].CaseId];

        // The case has 2 work orders, and we need to combine them in 1 email.
        System.assert(workOrders.size() > emailMessages.size());
        System.assertEquals(caseEmail[0].ParentId, workOrders[0].CaseId, 'Email was not linked the the right Case.');
    }

    @IsTest
    static void testEmailMultipleWorkOrdersBulk() {
        // Arrange
        List<Contact> contacts = [SELECT Account.Id FROM Contact LIMIT 1];
        Contact contact = (contacts.size() == 1) ? contacts.get(0) : null;

        // We're creating Cases with 1 Work Order, related to the same Account/Contact in bulk
        List<Case> installationCases = new List<Case>();
        for (Integer i = 0; i < totalCases; i++) {
            Case installationCase = (Case) make(Entity.SERVICE_CASE, new Case(Contact = contact, Account = contact.Account));

            installationCases.add(installationCase);

            // Case Process Builder expects a queue as the owner when the case is created, so we need to run the assignment rules.
            Database.DMLOptions dmlOptions = new Database.DMLOptions();
            dmlOptions.assignmentRuleHeader.useDefaultRule = true;
            installationCase.setOptions(dmlOptions);

            make(Entity.SERVICE_WORK_ORDER, new WorkOrder(Case = installationCase));
        }
        run();

        List<WorkOrder> workOrders = [SELECT CaseId, Case.ContactId FROM WorkOrder];

        // Act
        Test.startTest();
        InstallationNotificationHandler.sendInstallationAssignmentEmail(workOrders);
        Test.stopTest();

        // Assert
        Set<Id> caseIds = new Map<Id, Case>(installationCases).keySet();
        List<EmailMessage> emailMessages = [SELECT Id, ParentId FROM EmailMessage WHERE ParentId IN :caseIds];

        System.assertEquals(emailMessages.size(), caseIds.size(), 'Not ever case got an email.');
    }

    @TestSetup
    static void createData() {
        c_TestFactory.setDefaultContext();

        // need work orders so account > contact > case > workorder tree
        make(Entity.SERVICE_CUSTOMER, new Account());
        run();

    }
}