// SWI added comment
@isTest
private class AssignLeadMQLTest {
    @isTest static void testInvokeRule() {
        Lead testLead = new Lead(FirstName = 'Test', LastName = 'Lead', Email = 'test@test.com', Company = 'Test Company');
        insert testLead;
        Map<String,String> stringMap = new Map<String,String>();
        stringMap.put('LeadId', String.valueOf(testLead.Id));
        
        AssignLeadMQL.InvokeRule(stringMap);
    }
}