@isTest
private class OptInByPhoneTest {

    private class MockHttpResponseGeneratorSuccess implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            return res;
        }
    }

    @isTest static void testOptInByPhone() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorSuccess());
        OptInByPhone.OptInByPhoneRequest testRequest = new OptInByPhone.OptInByPhoneRequest();
        testRequest.email = 'mwisetest@mailinator.com';
        testRequest.country = 'Germany';
        
        List<OptInByPhone.OptInByPhoneRequest> testRequests = new List<OptInByPhone.OptInByPhoneRequest>();
        testRequests.add(testRequest);
        OptInByPhone.OptInByPhone(testRequests);
    }
}