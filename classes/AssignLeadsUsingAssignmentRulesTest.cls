@isTest
private class AssignLeadsUsingAssignmentRulesTest {
    @isTest static void testLeadAssign() {
		Lead testLead = new Lead(FirstName = 'Test', LastName = 'Lead', Email = 'test@test.com', Company = 'Test Company');
		insert testLead;
        List<Id> leadIds = new List<Id>();
        leadIds.add(testLead.Id);
        AssignLeadsUsingAssignmentRules.LeadAssign(leadIds);
        
    }

}