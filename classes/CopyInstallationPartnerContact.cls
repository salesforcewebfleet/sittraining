public class CopyInstallationPartnerContact {
    public static void copyIPContactfromAccountoWorkorder(){
        List<WorkOrder> woList = [SELECT AlternativeContact__c, InstallationPartner__r.InstallationPartnerDefaultContact__c FROM WorkOrder 
                                  WHERE Case.SkipIPAssignmentEmail__c = false AND IPAssignmentEmailStatus__c IN ('Send Pending', 'Send Failed') ];
        if(!woList.isEmpty()){
            for(WorkOrder w: woList){
            	if(w.InstallationPartner__r.InstallationPartnerDefaultContact__c != null)
            		w.AlternativeContact__c = w.InstallationPartner__r.InstallationPartnerDefaultContact__c;
       	 	}
            Database.update(woList,false);
        }
    }
}