// Updated by SWI for Autorabit test
public without sharing class AssignLeadMQL {
    @future(callout = true) public static void InvokeRule(Map<String, String> m) {
        string leadId = m.get('LeadId');
        List<lead> leadList = [SELECT Id FROM Lead WHERE Id = :leadId];
        if(!leadList.isEmpty()) {
            Lead leadRecord = leadList [0];
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            leadRecord.setOptions(dmo);
            Database.update(leadRecord, false);
        }
    }
}