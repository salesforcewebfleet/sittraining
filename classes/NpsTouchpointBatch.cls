public without sharing class NpsTouchpointBatch implements Database.Batchable<SObject> {
    
     public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        String queryString = 'SELECT Id,LastNPSCalculation__c FROM Account WHERE U_RID__c != null AND Type = \'Reseller\'';
        return Database.getQueryLocator(queryString);
    }

    public void execute(Database.BatchableContext batchableContext, List<SObject> scope) {
        Decimal percentage = NpsTouchpointHandler.calculatePercentageAudience();
        NpsTouchpointHandler.filterContacts((List<Account>)scope,percentage);
    }

    public void finish(Database.BatchableContext batchableContext) {
    }
}