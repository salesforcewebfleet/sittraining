//Autorabi1 Update 1
public class TouchPointParticipantHandler{
    public static void isEmailContactexcludedforNPS(List<TouchPointParticipant__c> tppList){
        NpsTouchpoint__mdt npsTouchpoint= [SELECT EmailsToExclude__c
                                        FROM NpsTouchpoint__mdt
                                        WHERE DeveloperName = 'InicialSetup'
                                        LIMIT 1];
        List<String> lstEmails = npsTouchpoint.EmailsToExclude__c.split(',');
//Autorabi1 Update 2
        for (TouchPointParticipant__c t : tppList){
            Boolean isAMatch = false;
            for (Integer i = 0; i < lstEmails.size() && !isAMatch; i++){
                if (t.Email__c.contains(lstEmails.get(i))){
                    t.Status__c = 'Excluded';
                    isAMatch = true;
                }
            }
        }
    }
//Autorabi1 Update END

}