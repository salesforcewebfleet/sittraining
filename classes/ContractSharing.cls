/*
Author: Abhijit Ghanekar
Tickets (latest ones on top):
##########################################################################################################################################################
SFT-153 - Head of finance field added on Org Unit Object. Sharing of Contract record needs to be done with head of finance. New apex reason for sharing created


*/

public class ContractSharing{

public static Set<Id> OwnerDrillMap = new Set<Id>();
public static Boolean success=false;
public static Boolean CalledfromUserTermination=false;
public static list<TT_Contract__Share> listShare = new list<TT_Contract__Share>();

@future(callout=true)
public static void ShareRecordForOwnerChange(Map<Id,Id> OwnerMap, Set<Id> ContIds)
{
// delete existing shared records of type hierarchy
    list<TT_Contract__Share> delshare = new list<TT_Contract__Share>();
    list<TT_Contract__Share> contshare = [select Id,ParentId,RowCause from TT_Contract__Share where ParentId IN :ContIds and RowCause = 'Hierarchy__c'];
    if (contshare.size() > 0)
    {//start if
    for (TT_Contract__Share delshare1:contshare)
    {
    //add records to be deleted from sharing object to list
    delshare.add(delshare1);
    
    }
    }//end if
    
    if (delshare.size() > 0)
    {
    list<Database.DeleteResult> sr1 = Database.delete(delshare,false);
    
    }

    //Owner Sharing for changed owner.
    for (Id recordid:OwnerMap.keySet())
    {
        system.debug('Inside debug for changed owner process sharing');
        ProcessSharing(recordid,OwnerMap.get(recordid),'Owner');
    
    }
    list<Database.SaveResult> sr = Database.insert(listShare,false);
    
    for (Database.SaveResult sr1:sr)
    {
        if (sr1.isSuccess())
        {
            success = true;
        }
        else
        {
          Database.Error err = sr1.getErrors()[0];

         if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  && 
                  err.getMessage().contains('AccessLevel')){
            success = true;
         }
         else{

            success = false;
          
            }
        }
    
    }

    
}

public static void SendEmail(Set<Id> UserId)
{
    String[] alladdresses = new String[]{'abhijit.ghanekar@tomtom.com','veerle.rutten@tomtom.com','tom.tavernier@tomtom.com'};
    String subject = 'Users Terminated in Salesforce.com which are contract Owners';
    String htmlbody = '';
    List<Messaging.SingleEmailMessage> teremailList = new List<Messaging.SingleEmailMessage>(); // list of email messages
    list<User> teruser = [select Id, FirstName, LastName from User where Id IN :UserId ];
    if (teruser.size() > 0)
    {
        for (User t1:teruser)
        {
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          htmlbody = 'Contract owner ' +  t1.FirstName + '  ' + t1.LastName + ' with Id ' +  t1.Id + ' has been deactivated during Workday-Sfdc synchronization';
          mail.setToAddresses(alladdresses);
          mail.setSubject(subject);
          mail.saveAsActivity = false;
          mail.setHtmlBody(htmlbody);
          teremailList.add(mail);
        
        }
    
    }
    if (teremailList.size() > 0)
    {
        Messaging.SendEmailResult[] results = Messaging.sendEmail(teremailList);
    }
}

public static void ShareRecordForOwnerChangeForUserTermination(Map<Id,Id> OwnerMap, Set<Id> ContIds)
{
// delete existing shared records of type hierarchy
    list<TT_Contract__Share> delshare = new list<TT_Contract__Share>();
    list<TT_Contract__Share> contshare = [select Id,ParentId,RowCause from TT_Contract__Share where ParentId IN :ContIds and RowCause = 'Hierarchy__c'];
    if (contshare.size() > 0)
    {//start if
    for (TT_Contract__Share delshare1:contshare)
    {
    //add records to be deleted from sharing object to list
    delshare.add(delshare1);
    
    }
    }//end if
    
    if (delshare.size() > 0)
    {
    list<Database.DeleteResult> sr1 = Database.delete(delshare,false);
    
    }

    //Owner Sharing for changed owner.
    for (Id recordid:OwnerMap.keySet())
    {
        system.debug('Inside debug for changed owner process sharing');
        ProcessSharing(recordid,OwnerMap.get(recordid),'Owner');
    
    }
    list<Database.SaveResult> sr = Database.insert(listShare,false);
    
    for (Database.SaveResult sr1:sr)
    {
        if (sr1.isSuccess())
        {
            success = true;
        }
        else
        {
          Database.Error err = sr1.getErrors()[0];

         if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  && 
                  err.getMessage().contains('AccessLevel')){
            success = true;
         }
         else{

            success = false;
          
            }
        }
    
    }

    
}



@future(callout=true)
public static void ShareRecord(Map<Id,Id> ControllerMap, Map<Id,Id> OwnerMap,Map<Id,Id> StreamOwnerMap,Map<Id,Id> HeadofFinanceMap)
{
//start of ShareRecord
    //Ownersharing
    for (Id recordid:OwnerMap.keySet())
    {
        ProcessSharing(recordid,OwnerMap.get(recordid),'Owner');
    
    }
    if (ControllerMap.size() > 0 )
    {
    for (Id recordid:ControllerMap.keySet())
    {
        
        ProcessSharing(recordid,ControllerMap.get(recordid),'Controller');
    }
    
    }
    
    if (StreamOwnerMap.size() > 0 )
    {
    for (Id recordid:StreamOwnerMap.keySet())
    {
        
        ProcessSharing(recordid,StreamOwnerMap.get(recordid),'StreamOwner');
    }
    
    }
    //SFT-153.sn
    if (HeadofFinanceMap.size() > 0 )
    {
    for (Id recordid:HeadofFinanceMap.keySet())
    {
        
        ProcessSharing(recordid,HeadofFinanceMap.get(recordid),'HeadOfFinance');
    }
    
    }
    //SFT-153.en

    
    
    
    list<Database.SaveResult> sr = Database.insert(listShare,false);
    
    for (Database.SaveResult sr1:sr)
    {
        if (sr1.isSuccess())
        {
            success = true;
        }
        else
        {
          Database.Error err = sr1.getErrors()[0];

         if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  && 
                  err.getMessage().contains('AccessLevel')){
            success = true;
         }
         else{

            success = false;
          
            }
        }
    
    }

}
//end of ShareRecord method

public static void ProcessSharing(Id recordid, Id ownerid, String typeOfSharing)
{
//start ProcessSharing
  if (typeOfSharing == 'Owner')
  {
     OwnerSharingDrillDown(ownerid);
     OwnerSharingProcess(recordid,OwnerDrillMap);
     OwnerDrillMap.Clear();
  }
  if (typeOfSharing == 'Controller')
  {
    AddSharing(recordid,ownerid,'Controller');
  
  }
  if (typeOfSharing == 'StreamOwner')
  {
    AddSharing(recordid,ownerid,'StreamOwner');
  }
  //SFT-153.sn
  if (typeOfSharing == 'HeadOfFinance')
  {
    AddSharing(recordid,ownerid,'HeadOfFinance');
  }
  //SFT-153.en
}
//end ProcessSharing
private static void OwnerSharingDrillDown(Id ownerid)
{
//Start of OwnerSharingDrillDown
     Boolean Noexit=true;
     Id tempid = ownerid;
     Integer count=0;
     
     do
     {
     
     list<User> u = [select Id, ManagerId from User where Id =:tempid LIMIT 1];
     
     if (u.size() > 0)
     {// if user exists
     
         if (u[0].ManagerId == null || count == 5)
         {
            Noexit = false;
         } 
         else
         {
          tempid = u[0].ManagerId;
          OwnerDrillMap.add(tempid);
         }
     }// end if user exists
     if (count == 5 || u.size() <=0)
     {
         Noexit = false;
     }
     count = count + 1;
     }
     while (Noexit);

}
//end of OwnersharingDrillDown
public static void OwnerSharingProcess(Id recordid,Set<Id> OwnerDrillMap)
{
    for (Id ownerid:OwnerDrillMap)
    {
        AddSharing(recordid,ownerid,'Owner');
    
    }

}
public static void AddSharing(Id recordid, Id ownerid, String type)
{
    String rowCause;

    TT_Contract__Share contShare = new TT_Contract__Share();
    contShare.ParentId = recordid;
    contShare.UserOrGroupId = ownerid;
    contShare.AccessLevel = 'Read';
    
    if (type.equals('Owner'))
    {
        contShare.RowCause = TT_Contract__Share.RowCause.Hierarchy__c;
    }
    else if (type.equals('Controller'))
    {
        contShare.RowCause = TT_Contract__Share.RowCause.Controller__c;

    }
    else if (type.equals('StreamOwner'))
    {
        contShare.RowCause = TT_Contract__Share.RowCause.Stream_Owner__c;
    }
    else
    {
        contShare.RowCause = TT_Contract__Share.RowCause.Head_of_Finance__c; //SFT-153.n
    }
     listShare.add(contShare);
 
    
    
}

@future(Callout=true)
public static void RunContractShareOnUserInactivation(Set<Id> UserSet)
{//start of run contract sharing
    Map<Id,Id> UserwithConts = new Map<Id,Id>(); //cont id and user id in a map
    Map<Id,Id> MgrwithConts = new Map<Id,Id>();  // contract id and manager id in a map
    Map<Id,Id> MgrwithContsSorted = new Map<Id,Id>(); //contract id and manager id sorted by Manager id in a map
    Map<Id,String> ContIdwithName = new Map<Id,String>(); //contract id and conract name stored in a map
    List<String> QueueEmails = new List<String>(); // store email addresses of queue members
    List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>(); // list of email messages
    
    Set<Id> UsersSet = new Set<Id>();
    Set<Id> MgrSet = new Set<Id>();
    Set<Id> MgrNewSet = new Set<Id>();
    Set<Id> ContwithErrors = new Set<Id>(); //store contracts records with errors
    Map<Id,Id> UserMgrMapNew = new Map<Id,Id>();
    
    Map<Id,List<String>> tempemailmap = new Map<Id,List<String>>();
    List<String> toaddresses = new List<String>();
    Id currentId;
    Id nextId;
    String contname;
    String bodytext1;
    String bodytext2;
    String htmlbody='';
    String subject='Contract Transferred to You';
    Boolean sendMail = false;
    
    //Queue of System admins who own the contract record in case manager is blank on user or manager doesnt have valid SFDC license
    Group queue = [Select Id, DeveloperName, Type from Group where Type = 'Queue' and DeveloperName = 'System_Administrators' LIMIT 1];
    list<User> email = [SELECT Email FROM User WHERE Id IN (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'System_Administrators')];
    for (User uemail:email)
    {
     QueueEmails.add(uemail.email); //add email ids of queue members to a list. this list will be added to a Map later
    }
    Map<Id,List<String>> tempmap = new Map<Id,List<String>>();
    tempmap.put(queue.Id,QueueEmails);
    
    //filter and prepare set of users who have contracts. prepare map of contract id and ownerid of contract
    
    list<TT_Contract__c> listofcontracts = [select Id,OwnerId,Name,Status__c from TT_Contract__c where OwnerId IN :UserSet AND status__c <> 'Inactive' order by OwnerId];
    for (TT_Contract__c Cont:listofcontracts)
    {
       UserwithConts.put(Cont.Id,Cont.OwnerId);
       ContIdwithName.put(Cont.Id,Cont.Name);
       UsersSet.add(Cont.OwnerId);
    }
    
    //prepare map of user id and manager id. prepare set of managers of users who are present as contract owners
    list<User> usr=[select Id,ManagerId from user where Id IN :UsersSet];
    for (User usr1:usr)
    {
        UserMgrMapNew.put(usr1.Id,usr1.ManagerId);
        MgrSet.add(usr1.ManagerId);
    }   
    
    //get list of Managers who have active SFDC license
    UserLicense usrlicense = [select Id from UserLicense where LICENSEDEFINITIONKEY = 'SFDC' LIMIT 1];
    list<User> usrmgr=[select Id,ProfileId,Email from User where Id IN :MgrSet AND isActive = true];
    
    //add managers ids in a new set to store only those managers who have active SFDC license
    for (User usrmgr1:usrmgr)
    {
        MgrNewSet.add(usrmgr1.Id);
    }
    
  
    //loop through contracts and replace all owners with manager ids if Manager is valid otherwise replace owner id with Queue id of system admin queue
    for (Id contnewid:UserwithConts.keySet())
    {
        for (Id userid1:UserMgrMapNew.keySet())
        {
            if (userid1.equals(UserwithConts.get(contnewid)))
            {
                if (ManagerisValid(UserMgrMapNew.get(userid1),MgrNewSet))
                {
                MgrwithConts.put(contnewid,UserMgrMapNew.get(userid1));
                }
                else
                {
                MgrwithConts.put(contnewid,queue.Id);
                }
                break;
            }
        }
    }//end of for loop to prepare Contract ids with managers map
 
    //sort the values in Map of MgrwithConts. Intenion is to sort values in Map and loop through sorted map values and create a new map of contract ids and 
    //sorted Manager ids or queueids
    
    for (Id valuemgr:getSortedvalueset(MgrwithConts))
    {//sort the contract map based on ManagerId i.e. values 
        for (Id contsortid:MgrwithConts.keySet())
        {//loop through contract map start of inner for
        
            if (valuemgr.equals(MgrwithConts.get(contsortid)))
            {//start if
                MgrwithContsSorted.put(contsortid,valuemgr);
            }//end if
        
        }//end of inner for
    }//end of for loop for values
    
    //loop through map to update owners on Contracts
    
    TT_Contract__c[] Contupdates = new TT_Contract__c[] {};
    for (Id id : MgrwithContsSorted.keySet()) {
        Contupdates.add(new TT_Contract__c(Id = id, OwnerId = MgrwithContsSorted.get(id)));
    }
    CalledfromUserTermination = true;
    list<Database.SaveResult> srCont = Database.update(Contupdates,false);
    //end of update owners on contracts
    SendEmail(UsersSet);
    
    for (Integer i = 0; i < Contupdates.size(); i++)
    {
        Database.SaveResult s = srCont[i];
        TT_Contract__c origRecord = Contupdates[i];
        if (!s.isSuccess())
        {
          system.debug('RECORD id with error during update is>'+ origRecord.Id + '<<');
          //system.debug('RECORD id with error during update is>'+ s.Id + '<<');
            for(Database.Error err : s.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Account fields that affected this error: ' + err.getFields());
             }
          
          
          
          ContwithErrors.add(origRecord.Id);
        } 
    }
    
    for (Id ContKey:MgrwithContsSorted.keySet())
    {//for loop for preparing emails
    system.debug('Inside for Loop for email');
      currentId = MgrwithContsSorted.get(ContKey);
      system.debug('Inside for Loop for email currentId is >' + currentId + '<<');
      if (currentId != nextId && nextId != null)
      {//add to send email list on change of manager id
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          if (nextId.equals(queue.Id))
          {// logic for email sending to queue members
             mail.setToAddresses(QueueEmails);
          }//
          else
          {// logic for sending email to managerid
             mail.setTargetObjectId(nextId);
          }//
          mail.setSubject(subject);
          mail.saveAsActivity = false;
          mail.setHtmlBody(htmlbody);
          emailList.add(mail);
          htmlbody='';
      }
      nextId = currentId;
      if (ContIdwithName.ContainsKey(ContKey))
      {
          contname = ContIdwithName.get(ContKey);
      }
      if (!ContwithErrors.Contains(ContKey))
      {
       bodytext1 = 'Contract ' +  contname +  ' has been assigned to you. Please click on the link below to view the record.' + '<br/>';
     
      }
      else
      {
      system.debug('Inside debug could not assign contract');
       bodytext1 = 'Contract ' + contname + ' could not be assigned to you because of an error during update, please contact the salesforce administrator.' + '<br/>';
      }
      bodytext2 = URL.getSalesforceBaseUrl().toExternalForm() + '/' + ContKey + '<br/>' + '<br/>';
      bodytext1 = bodytext1 + bodytext2;
      htmlbody = htmlbody + bodytext1;
      
    }//end of for loop to prepare email
    
    if (MgrwithContsSorted.size() > 0 && currentId.equals(nextId) && nextId != null)
    {//only 1 Manager id or queue id exists in the map 
    
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          if (nextId.equals(queue.Id))
          {// logic for email sending to queue members
             mail.setToAddresses(QueueEmails);
          }//
          else
          {// logic for sending email to managerid
             mail.setTargetObjectId(nextId);
          }//
          mail.setSubject(subject);
          mail.saveAsActivity = false;
          mail.setHtmlBody(htmlbody);
          emailList.add(mail);
          htmlbody='';
    }//end if
    
    Messaging.SendEmailResult[] results = Messaging.sendEmail(emailList);
    
    
}//end of run contract sharing

public static Boolean ManagerisValid(Id MgrId,Set<Id> MgrCheckSet)
{//
    Boolean exitloop=false;
    If (MgrId == null)
    {//if manager is not present return false dont enter loop
         exitloop = false;
    }
    
    if (MgrId != null)
    {//Check if ManagerId is null
        for (Id validmgrid:MgrCheckSet)
        {//loop through valid managers to check if Manager id passed as input is valid
            
            if (MgrId.equals(validmgrid))
            {//break from for loop if manager id matches one of the valid manager ids
                exitloop = true;
                break;        
            }
        }//end of for
        }//end of if to check ManagerId
    if (exitloop)
    {
    return true;
    }
    else
    {
    return false;
    }
}
//method that sorts map keys. used for sorting map of Manager with contracts by ManagerId
public static List<Id> getSortedvalueset(Map<Id, Id> MgrwithConts) {
List<Id> valueSetList = new List<Id>();
valueSetList.addAll(MgrwithConts.values());
valueSetList.sort();
return valueSetList;
}


//end of class
}