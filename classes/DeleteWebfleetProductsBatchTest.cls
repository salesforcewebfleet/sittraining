@istest 
public class DeleteWebfleetProductsBatchTest {
  
   static testmethod void test() {

      Test.startTest();
       
      WEBFLEET_Products__c cp = new WEBFLEET_Products__c();
      cp.Name = 'testWEBFLEETProduct';       
      insert cp;
       
      System.assertEquals('testWEBFLEETProduct', [SELECT Id, Name FROM WEBFLEET_Products__c WHERE Id = :cp.Id].Name);
      
    /*  Datetime dt = Datetime.now().addMinutes(1);
      String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
      String jobId = System.schedule('testBasicScheduledApex', CRON_EXP, new DeleteWEBFLEETProductsRecords());*/
       DeleteWebfleetProductsBatch obj= new DeleteWebfleetProductsBatch();
       DataBase.executeBatch(obj);
              
      Test.stopTest();              
     
      List<WEBFLEET_Products__c> allProducts = [SELECT Id FROM WEBFLEET_Products__c];
      System.assertEquals(0, allProducts.size());

   }
}