/**
 * @description Class containing an invocable method to assign cases using various assignment rules.
 *
 * @see CaseAssignmentRuleInvocableAction_test Test class
 *
 * @author fheeneman Salesforce
 *
 * @date 14/5/20    fheeneman    Initial setup
 */
public with sharing class CaseAssignmentRuleInvocableAction {

    /**
     * @description Invocable Method to assign cases using various assignment rules.
     *
     * @param caseAssignmentRuleRequests Cases that need to be reassigned following the provided assignment rule
     */
    @InvocableMethod(Description='Will trigger the provided assignment rule for the provided case. This allows you to give the developer name of an assignment rule that is not the default active rule.' Label='Trigger Case Assignment Rule')
    public static void runAssignmentRule(List<CaseAssignmentRuleRequest> caseAssignmentRuleRequests) {

        Set<String> assignmentRuleNames = new Set<String>();
        for (CaseAssignmentRuleRequest request : caseAssignmentRuleRequests) {
            assignmentRuleNames.add(request.assignmentRuleName);
        }

        Map<String, Id> assignmentsRuleIdByName = new Map<String, Id>();
        List<AssignmentRule> assignmentRules = [SELECT Id, Name FROM AssignmentRule WHERE SobjectType = 'Case' AND Name IN :assignmentRuleNames];
        for (AssignmentRule assignmentRule : assignmentRules) {
            assignmentsRuleIdByName.put(assignmentRule.Name, assignmentRule.Id);
        }

        List<Case> cases = new List<Case>();
        for (CaseAssignmentRuleRequest request : caseAssignmentRuleRequests) {
            Database.DMLOptions dmo = new Database.DMLOptions();

            if (String.isBlank(request.assignmentRuleName)) {
                dmo.assignmentRuleHeader.useDefaultRule = true;
            } else {
                dmo.assignmentRuleHeader.assignmentRuleId = assignmentsRuleIdByName.get(request.assignmentRuleName);
            }

            // Added this part because updating the case you get straight from the Flow throws an INVALID_TYPE_ON_FIELD_IN_RECORD error.
            // So instead, we'll just be updating a new instance of the same case. We're not actually updating any values anyway.
            Case newCase = new Case();
            newCase.Id = request.aCase.Id;
            newCase.setOptions(dmo);

            cases.add(newCase);
        }

        update cases;
    }

    /**
     * @description Wrapper class for the Invocable Method requests.
     */
    public class CaseAssignmentRuleRequest {
        @InvocableVariable(Label = 'Assignment Rule Developer Name' Description = 'The developer name of the assignment rule that needs to be run for this case, leave empty to use the default rule.' Required=false)
        public String assignmentRuleName;

        @InvocableVariable(Label = 'Case' Description = 'The case that needs to be assigned according to the provided or default assignment rule' Required=true)
        public Case aCase;
    }
}