/**
 * @author      fheeneman
 * @author      Salesforce
 * @description Test for CDOSyncBatchSchedulable
 * @date 4/3/20		fheeneman		Initial setup
 */
@IsTest
private class CDOSyncBatchSchedulable_test {

    @IsTest
    static void testBehavior() {
        // Arrange
        String cron = '0 0 23 * * ?';

        // Act
        Test.startTest();
        System.schedule('testjob', cron, new CDOSyncBatchSchedulable());
        Test.stopTest();

        // Assert
        System.assert(true, 'nothing to assert.');
    }
}