/**
 * @description Authentication Provider Plugin for the WEBFLEET SSO Auth Provider.
 * Provides methods for the Auth Provider framework to run a custom OAuth flow with the WEBFLEET Auth Server
 *
 * @see WebfleetAuthProviderPlugin_test Test class
 *
 * @author fheeneman Salesforce
 *
 * @date 9/6/20    fheeneman    Initial setup
 */
public with sharing class WebfleetAuthProviderPlugin extends Auth.AuthProviderPluginClass {

    /**
     * @description Returns the custom metadata type API name for a custom OAuth-based authentication provider for single sign-on to Salesforce.
     *
     * @return Api name for the custom metadata type created for this auth provider.
     */
    public String getCustomMetadataType() {
        return 'WebfleetAuthProvider__mdt';
    }

    /**
     * @description Builds the URL where the user is redirected for authentication.
     *
     * @param authProviderConfiguration The configuration for the custom authentication provider.
     * This is set with values you enter when you create the custom provider in Auth. Providers in Setup.
     * @param stateToPropagate The state passed in to initiate the authentication request for the user.
     * This is a generated string that remains the same during the whole flow
     *
     * @return The URL of the page where the user is redirected for authentication.
     */
    @SuppressWarnings('sf:OpenRedirect') // Suppressing Open Redirect warning, as url is fully configured by Webfleet from AuthProvider config
    public PageReference initiate(Map<String, String> authProviderConfiguration, String stateToPropagate) {
        String authUrl = authProviderConfiguration.get('AuthorizeURL__c');
        String clientId = authProviderConfiguration.get('ClientId__c');
        String scope = authProviderConfiguration.get('Scope__c');
        String redirectUrl = authProviderConfiguration.get('CallbackURL__c');

        String url = authUrl
                + '?client_id=' + clientId
                + '&scope=' + scope
                + '&redirect_uri=' + EncodingUtil.urlEncode(redirectUrl, 'UTF-8')
                + '&state=' + stateToPropagate
                + '&response_type=code';

        return new PageReference(url);
    }

    /**
     * @description Handles the call back in the OAuth flow.
     * Sends a POST to the Token Endpoint to exchange the authorization code for an access and refresh token.
     *
     * @param authProviderConfiguration The configuration for the custom authentication provider.
     * This is set with values you enter when you create the custom provider in Auth. Providers in Setup.
     * @param callbackState An object that contains the HTTP headers, body, and queryParameters of the authentication request.
     *
     * @return An instance of the AuthProviderTokenResponse class. This contains the SSO Provider, access token, refresh token and the state
     */
    public Auth.AuthProviderTokenResponse handleCallback(Map<String, String> authProviderConfiguration, Auth.AuthProviderCallbackState callbackState) {
        String clientId = authProviderConfiguration.get('ClientId__c');
        String secret = authProviderConfiguration.get('ClientSecret__c');
        String accessTokenUrl = authProviderConfiguration.get('AccessTokenUrl__c');
        String redirectUrl = authProviderConfiguration.get('CallbackURL__c');

        Map<String, String> queryParameters = callbackState.queryParameters;
        String code = queryParameters.get('code');
        String sfdcState = queryParameters.get('state');

        HttpRequest request = new HttpRequest();
        String url = accessTokenUrl
                + '?code=' + code
                + '&client_id=' + clientId
                + '&client_secret=' + secret
                + '&grant_type=authorization_code'
                + '&redirect_uri=' + redirectUrl;
        request.setEndpoint(url);
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setMethod('POST');

        Http http = new Http();
        HttpResponse response = http.send(request);
        String responseBody = response.getBody();

        Map<String, Object> json = (Map<String, Object>) JSON.deserializeUntyped(responseBody);
        String token = (String) json.get('access_token');
        String refreshToken = (String) json.get('refresh_token');

        return new Auth.AuthProviderTokenResponse('WEBFLEET', token, refreshToken, sfdcState);
    }

    /**
     * @description Retrieves information from the WEBFLEET User Info endpoint about the current user.
     * This information is used by the registration handler and in other authentication provider flows.
     * <br />
     * Unfortunately, the WEBFLEET UserInfo endpoint does not contain enough information to do anything other than identifying existing users.
     *
     * @param authProviderConfiguration The configuration for the custom authentication provider.
     * This is set with values you enter when you create the custom provider in Auth. Providers in Setup.
     * @param tokenResponse This contains the SSO Provider, access token, refresh token and the state
     *
     * @return An instance of Auth.UserData containing information about the user, mainly unique identifier, email and locale.
     */
    @SuppressWarnings('sf:AvoidHardCodedCredential') // Suppressing because Authorization Header is necessary in the case of OAuth flow
    public Auth.UserData getUserInfo(Map<String, String> authProviderConfiguration, Auth.AuthProviderTokenResponse tokenResponse) {
        String userInfoUrl = authProviderConfiguration.get('UserInfoURL__c');
        String token = tokenResponse.oauthToken;

        HttpRequest request = new HttpRequest();
        request.setHeader('Authorization', 'Bearer ' + token);
        request.setEndpoint(userInfoUrl);
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('GET');

        Http http = new Http();
        HttpResponse response = http.send(request);
        String responseBody = response.getBody();

        Map<String, Object> json = (Map<String, Object>) JSON.deserializeUntyped(responseBody);

        return new Auth.UserData((String) json.get('sub'), null, null, null, (String) json.get('email'), null, null, (String) json.get('locale'), 'WEBFLEET', null, new Map<String, String>());
    }
}