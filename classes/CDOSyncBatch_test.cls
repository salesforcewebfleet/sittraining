/**
 * @author      fheeneman
 * @author      Salesforce
 * @description Test for CDOSyncBatch
 * @date 4/3/20		fheeneman		Initial setup
 */
@IsTest
private class CDOSyncBatch_test extends c_TestFactory {

    // Bulkify or not? Look at the settings on the TestFactory to decide how much data to build
    static Integer totalAccounts = c_TestFactory.BULKIFY_TESTS ? 80 : 2;

    @IsTest
    static void testCDOBatch() {
        // Arrange
        // Act
        Test.startTest();
        Database.executeBatch(new CDOSyncBatch());
        Test.stopTest();

        // Assert
        System.assert(true, 'nothing to assert, all logic is tested in the actual CDOSync class');
    }

    @TestSetup
    static void createData() {
        c_TestFactory.setDefaultContext();

        Account[] accounts = new List<Account>();

        for (Integer i = 0; i < totalAccounts; i++) {
            Account account = (Account) make(Entity.SERVICE_ACCOUNT, new Account());
            accounts.add(account);
        }

        List<Contact> contacts = new List<Contact>();
        // Giving every account 1 contact, which generates an ACR between them by default.
        for (Account account : accounts) {
            contacts.add((Contact) make(Entity.SERVICE_CONTACT, new Contact(Account = account)));
            make(Entity.CDO, new CDO__c(Account__r = account));
        }

        // make some additional ACR links with some logic to not link the contacts to the same account twice
        Integer i = 0;
        for (Contact contact : contacts) {
            if (i < accounts.size() - 1) {
                make(Entity.SERVICE_ACR, new AccountContactRelation(Contact = contact, Account = accounts[i + 1]));
            }
            i++;
        }
        // We don't want the CDO Sync to be trigger right now, because we need to test it in our methods.
        TriggerHandler.bypass('AccountContactRelationTriggerHandler');
        run();
        TriggerHandler.clearBypass('AccountContactRelationTriggerHandler');
    }
}