@isTest
private class TriggerLeadTest {
    @isTest static void checkMKTNurtureInsert() {
        Lead le = new Lead(Status = 'MKT Nurture', Company = 'Test Lead', email = 'test@tesf.com',LastName = 'Last Name test' );
        insert le;
    }
    
    @isTest static void checkNewUpdate() {
        Lead le = new Lead(Status = 'UNQ Nurture', Company = 'Test Lead', email = 'test@tesf.com',LastName = 'Last Name test' );
        insert le;
        le.Status = 'New';
        update le;
    }
    
    @isTest static void checkMKTNurtureUpdate(){
        Lead le = new Lead(Status = 'UNQ Nurture', Company = 'Test Lead', email = 'test@tesf.com',LastName = 'Last Name test' );
        insert le;
        le.Status = 'MKT Nurture';
        update le;
    }
}