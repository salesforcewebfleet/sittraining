/**
 * @author      eto
 * @author      Salesforce
 * @description Description
 *              Tests executed:
 *              1 - testPost() - description
 *              2 - testPostWithException() - description
 * @date 18/5/20		eto		Initial setup
 */
@IsTest
private class CommunityUserPwResetInvocableAction_test extends c_TestFactory {
    static Integer totalUsers = c_TestFactory.BULKIFY_TESTS ? 200 : 2;

    @IsTest
    static void runPasswordReset_Default() {
        // Arrange
        User newUser = (User) make(Entity.STANDARD_USER, new User(managerId=userinfo.getUserId()));
        run();
		
        List <Id> userIds = new List <Id>();
        userIds.add(newUser.id);
    
        // Act
        Test.startTest();
        CommunityUserPwResetInvocableMethod.SendPasswordResetEmail(userIds);
        Test.stopTest();
        
        // Assert
        System.assert(true);
       
    }
    
}