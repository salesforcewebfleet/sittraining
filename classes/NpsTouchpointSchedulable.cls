public with sharing class NpsTouchpointSchedulable implements System.Schedulable {
    public void execute(SchedulableContext schedulableContext) {
        //List<FulfillmentEmailSettings__mdt> fulfillmentEmailSettings = [SELECT BulkSize__c FROM FulfillmentEmailSettings__mdt WHERE DeveloperName = 'InstallationNotification' LIMIT 1];
        //Integer batchSize = fulfillmentEmailSettings[0] != null && fulfillmentEmailSettings[0] != null ? Integer.valueOf(fulfillmentEmailSettings[0].BulkSize__c) : 200;
        Integer batchSize = 200;
        Database.executeBatch(new InstallationNotificationBatch(), batchSize);
    }
}