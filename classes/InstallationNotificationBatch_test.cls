/**
 * @description Test class for InstallationNotificationBatch
 *
 * @author fheeneman Salesforce
 *
 * @date 28/4/20		fheeneman		Initial setup
 */
@IsTest
private class InstallationNotificationBatch_test extends c_TestFactory{

    @IsTest
    static void testBehavior() {
        // Arrange
        // Only work orders that are Send Pending or Send Failed should be executed.
        List<WorkOrder> workOrders = [SELECT Id FROM WorkOrder];
        for (WorkOrder workOrder : workOrders) {
            workOrder.IPAssignmentEmailStatus__c = 'Send Pending';
        }
        update workOrders;

        List<Case> cases = [SELECT Id FROM Case];
        for (Case aCase : cases) {
            aCase.SkipIPAssignmentEmail__c = false;
        }
        update cases;

        // Verify that AsyncApexJob is empty
        // not strictly necessary but makes what is going on later clearer
        List<AsyncApexJob> jobsBefore = [SELECT Id, ApexClassId, ApexClass.Name, Status, JobType FROM AsyncApexJob];
        System.assertEquals(0, jobsBefore.size(), 'not expecting any asyncjobs');

        // Act
        Test.startTest();
        Database.executeBatch(new InstallationNotificationBatch());
        Test.stopTest();

        // Assert
        // check apex batch is in the job list
        List<AsyncApexJob> jobsApexBatch = [SELECT Id, ApexClassId, ApexClass.Name, Status, JobType FROM AsyncApexJob WHERE JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
        System.assertEquals('InstallationNotificationBatch', jobsApexBatch[0].ApexClass.Name, 'expecting InstallationNotificationBatch batch job');
    }

    @TestSetup
    static void createData() {
        c_TestFactory.setDefaultContext();

        // need work orders so account > contact > case > workorder tree
        make(Entity.SERVICE_CUSTOMER, new Account());
        run();
    }
}