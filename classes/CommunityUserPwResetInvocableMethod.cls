/**
 * @description  Class containing an invocable method to send Community Users a welcome email to set their password.
 *
 * @author	eto
 *
 * @date 12-5-2020 eto initial set up
 */
public class CommunityUserPwResetInvocableMethod {

    /**n
     * @description Invocable Method to send Community Users a welcome email to set their password.
     *
     * @param userIds
     */
    @InvocableMethod(Label='Send Password Reset Email' Description='Sends a password reset and welcome email to a newly created Customer Community User' category='User')
    public static void SendPasswordResetEmail(List<Id> userIds) {
   
         //Get Id for welcome email template
         List <Network> networks = [SELECT WelcomeEmailTemplateId FROM Network WHERE name = 'webfleet community' LIMIT 1]; 
           
         //Get Developername for welcome email template                     
         List <EmailTemplate> emailTemplates = [SELECT DeveloperName FROM EmailTemplate WHERE id = :networks[0].WelcomeEmailTemplateId ];
              
        for (Id userId: userIds) {
  
            System.resetPasswordWithEmailTemplate(userId, true, emailTemplates[0].DeveloperName);
        }

    }
}