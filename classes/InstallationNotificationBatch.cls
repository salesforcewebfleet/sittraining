/**
 * @author        	fheeneman
 * @author       	Salesforce
 * @description	    Description
 * @see           	InstallationNotificationBatch_test Test class
 * @date 17/4/20    fheeneman    Initial setup
 */
public without sharing class InstallationNotificationBatch implements Database.Batchable<SObject> {

    /**
     * @description Initializes the scope of the batch job
     *
     * @param batchableContext standard batchable context
     *
     * @return Query Locator containing all the ACRs
     */
    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        String queryString = 'SELECT CaseId, Case.ContactId FROM WorkOrder ' +
                'WHERE Case.SkipIPAssignmentEmail__c = false ' +
                'AND IPAssignmentEmailStatus__c IN (\'Send Pending\', \'Send Failed\')';

        return Database.getQueryLocator(queryString);
    }

    public void execute(Database.BatchableContext batchableContext, List<SObject> scope) {
        CopyInstallationPartnerContact.copyIPContactfromAccountoWorkorder();
        InstallationNotificationHandler.sendInstallationAssignmentEmail((List<WorkOrder>)scope);
    }

    public void finish(Database.BatchableContext batchableContext) {
    }

}