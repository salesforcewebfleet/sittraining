@isTest
private class TriggerOpportunityTest {
//TODO fix this test class to use the test factory, and evaluate if it's necessary still
    @isTest static void checkClosedLostOpportunity() {
        Opportunity opp = new Opportunity(Name='testOpName', CloseDate = Date.today(),StageName='testStage' );
        insert opp;
        Account acc = new Account(Name = 'Test Company', Industry = 'Agriculture');
        insert acc;
        Contact c = new Contact(LastName = 'Test', FirstName = 'Contact', AccountId = acc.Id, Active__c = 'Active', Country__c = 'DE');
        insert c;
        opp.AccountId = acc.Id;
        Test.startTest();
        opp.StageName ='Closed Lost';
        opp.Reason__c = 'Is a test Opp';
        update opp;
        Test.stopTest();
        List<Contact> coList = [SELECT id FROM Contact WHERE Active__c = 'Active MKT Nurture' AND Id =: c.Id];
        System.assertEquals(coList.size(), 1);
    }
    
    @isTest static void checkClosedWonOpportunity() {
        Opportunity opp = new Opportunity(Name='testOpName2', CloseDate = Date.today(),StageName='testStage' );
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', Family = 'Hardware');
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry pbEntry = new PricebookEntry(Pricebook2Id = pricebookId,Product2Id = prod.Id,UnitPrice = 100.00,IsActive = true);
		insert pbEntry;
		OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID=opp.Id,Product2Id = prod.Id, quantity=4, totalprice=200, PricebookEntryId  = pbEntry.Id);
		insert lineItem1;
        Account acc = new Account(Name = 'Test Company2', Industry = 'Agriculture', WEBFLEET_Customer_Number__c ='111', External_Guid__c = '111');
        insert acc;
        Contact c = new Contact(LastName = 'Test2', FirstName = 'Contact', AccountId = acc.Id, Active__c = 'Active MKT Nurture', Country__c = 'DE');
        insert c;
        Test.startTest();
        opp.AccountId = acc.Id;
        opp.StageName ='Closed Won';
        opp.Reason__c = 'Is a test Opp';
        update opp;
        Test.stopTest();
        List<Contact> coList = [SELECT id FROM Contact WHERE Active__c = 'Active' AND Id =: c.Id];
        System.assertEquals(coList.size(), 1);
    }
}