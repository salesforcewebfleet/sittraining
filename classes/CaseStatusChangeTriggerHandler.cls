//FGR Autorabit Test1
public without sharing class CaseStatusChangeTriggerHandler{

   private static List<Case> manageAutoResponseEmailAddress(Case[] caseObjects){
        List<Case> caseChanged = new List<Case>();
        AutoResponseExclusions__mdt autoResponseExclusions= [SELECT EmailAddress__c
                                        FROM AutoResponseExclusions__mdt
                                        WHERE DeveloperName = 'AutoResponseExclusions'
                                        LIMIT 1];
        List<String> lstEmails = autoResponseExclusions.EmailAddress__c.split(',');
        for (Case c : caseObjects ){
            Boolean isAMatch = false;

            for (Integer i = 0; i < lstEmails.size() && !isAMatch; i++){
                if (c.ContactEmail != null && c.sendAutoResponse__c && c.ContactEmail == lstEmails.get(i)){
                    c.sendAutoResponse__c= false;
                    caseChanged.add(c);
                    isAMatch = true;
                }
            }
        }
       return caseChanged;
   }
 //FGR Autorabit Test2 
   private static List<Case> manageAutoResponseEmailDomain(Case[] caseObjects){

        List<Case> caseChanged = new List<Case>();
        AutoResponseExclusions__mdt autoResponseExclusions= [SELECT DomainAddress__c
                                        FROM AutoResponseExclusions__mdt
                                        WHERE DeveloperName = 'AutoResponseExclusions'
                                        LIMIT 1];
        List<String> lstEmails = autoResponseExclusions.DomainAddress__c.split(',');
        for (Case c : caseObjects ){
            Boolean isAMatch = false;
            for (Integer i = 0; i < lstEmails.size() && !isAMatch; i++){
                if ( c.ContactEmail != null && c.sendAutoResponse__c && c.ContactEmail.contains(lstEmails.get(i))){
                    c.sendAutoResponse__c= false;
                    caseChanged.add(c);
                    isAMatch = true;
                }
            }
        }
        return caseChanged;
   }  

    
    

    public static void OnAfterInsert(Case[] newCases){
       List<Case> casesList = ([SELECT CaseNumber, ContactEmail, sendAutoResponse__c
                                                 from Case
                                                 where id in :newCases]);
       
        List<Case> caseUpdate = new List<Case>();
        caseUpdate.addAll(manageAutoResponseEmailAddress(casesList));
        caseUpdate.addAll(manageAutoResponseEmailDomain(casesList));
        if(caseUpdate != NULL && caseUpdate.size()>0){
            update caseUpdate;
        }
        
        
        
        List<Case_Status_Change__c> statusChanges = new List<Case_Status_Change__c>();
        statusChanges.addAll(createCaseStatus(newCases, null));
        insert statusChanges;
    }

    public static void OnAfterUpdate(Case[] updatedCases, Map<Id, Case> oldCaseMap){
        List<Case_Status_Change__c> statusChangesList = new List<Case_Status_Change__c>();
        List<Case_Status_Change__c> statusChangesToUpdateList = new List<Case_Status_Change__c>();
        List<String> oldStatusesList = new List<String>();
        List<Id> caseIdsList = new List<Id>();

        statusChangesList.addAll(createCaseStatus(updatedCases, oldCaseMap));

        //construct old status List and Ids to retrieve old case status changes to update the business hours
        for (Case_Status_Change__c statusChange : statusChangesList){
            oldStatusesList.add(statusChange.Status_From__c);
            caseIdsList.add(statusChange.Case__c);
        }

        if (caseIdsList.size() > 0 && oldStatusesList.size() > 0){
            List<Case_Status_Change__c> oldStatusChangesList = [select Id, Name, Set_Time__c
                                                                from Case_Status_Change__c
                                                                where Status_Name__c IN :oldStatusesList and Case__c IN :caseIdsList and Change_Time__c = null];
            BusinessHours bh = [SELECT Id
                                FROM BusinessHours
                                WHERE IsDefault = true];

            for (Case_Status_Change__c oldStatusChange : oldStatusChangesList){

                // calculate business hours
                oldStatusChange.Change_Time__c = Datetime.now();
                Double milliseconds = BusinessHours.diff(bh.Id, oldStatusChange.Set_Time__c, oldStatusChange.Change_Time__c);
                oldStatusChange.Status_Time__c = (Decimal.valueOf(milliseconds)).Divide((60 * 60 * 1000), 2);
                oldStatusChange.Age__c = getAge(milliseconds);
                statusChangesToUpdateList.add(oldStatusChange);
            }
        }

        insert statusChangesList;
        update statusChangesToUpdateList;
    }

    private static List<Case_Status_Change__c> createCaseStatus(Case[] caseObjects, Map<Id, Case> oldObjectMap){

        List<Case_Status_Change__c> statusChanges = new List<Case_Status_Change__c>();
        Map<Id, Case> cases = new Map<Id, Case>([SELECT CaseNumber, OwnerId, Owner.Name
                                                 from Case
                                                 where id in :caseObjects]);

        for (Case caseObject : caseObjects){
            Case_Status_Change__c statusChange = null;

            if (cases.size() > 0){
                //this is the update case, check if the status has changed or not, if changed
                //a new case status change record is created
                if (oldObjectMap != null){
                    Case oldObject = oldObjectMap.get(caseObject.Id);
                    if (caseObject.Status != oldObject.Status || caseObject.OwnerId != oldObject.OwnerId){

                        statusChange = new Case_Status_Change__c();
                        statusChange.Status_From__c = oldObject.Status;
                        if (caseObject.Status != oldObject.Status){
                            statusChange.Name = cases.get(caseObject.id).CaseNumber + ' from ' + oldObject.Status + ' to ' + caseObject.Status;
                        } else if (caseObject.OwnerId != oldObject.OwnerId){
                            if (String.valueOf(oldObject.OwnerId).startsWith('005')){
                                List<User> oldUserList = [SELECT Name FROM user WHERE id = :oldObject.OwnerId];
                                if (!oldUserList.isEmpty())
                                    statusChange.Name = cases.get(caseObject.id).CaseNumber + ' from ' + oldUserList[0].Name + ' to ' + cases.get(oldObject.id).Owner.Name;
                                } else if (String.valueOf(oldObject.OwnerId).startsWith('00G')){
                                    List<Group> oldQueueList = [SELECT Name FROM Group WHERE Type = 'Queue' AND id = :oldObject.OwnerId];
                                    if (!oldQueueList.isEmpty())
                                        statusChange.Name = cases.get(caseObject.id).CaseNumber + ' from ' + oldQueueList[0].Name + ' to ' + cases.get(oldObject.id).Owner.Name;
                                }
                        }
                    }
                }

                // this is the new case, the first case status change record is created
                else{
                    statusChange = new Case_Status_Change__c();
                    statusChange.Name = cases.get(caseObject.id).CaseNumber+' status: '+caseObject.Status;
                }

                //populate the rest of fields, add it to the result list to return
                if (statusChange != null){
                    statusChange.Case__c = caseObject.Id;
                    statusChange.Status_Name__c = caseObject.Status;
                    statusChange.Current_Case_Owner__c = cases.get(caseObject.id).Owner.Name;
                    statusChange.Set_Time__c = Datetime.now();
                    statusChanges.add(statusChange);
                }
            }
        }
        return statusChanges;
    }

    private static String getAge(Double milliseconds){
        Double duration = milliseconds;
        Double d = Math.floor(duration / 86400000); //Get whole days
        duration -= d * 86400000;
        Double h = Math.floor(duration / 3600000); //Get whole hours
        duration -= h * 3600000;
        Double m = Math.floor(duration / 60000);
        duration -= m * 60000;
        Double s = Math.floor(duration / 1000);
        return d.format()+' days ' + h.format()+' hours ' + m.format()+' minutes ' + s.format()+' seconds';
    }
}