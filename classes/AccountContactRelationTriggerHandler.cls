/**
 * @author          fheeneman
 * @author          Salesforce
 * @description     Trigger Handler for the AccountContactRelation object
 * @see             AccountContactRelationTriggerHandler_test Test class
 * @data 10/2/20    fheeneman    Initial setup
 **/
 //Updated From FGRRABDev
public with sharing class AccountContactRelationTriggerHandler extends TriggerHandler {

    private List<AccountContactRelation> newAccountContactRelations;
    private List<AccountContactRelation> oldAccountContactRelations;
    private Map<Id, AccountContactRelation> newAccountContactRelationsByIds;
    private Map<Id, AccountContactRelation> oldAccountContactRelationsByIds;

    /**
     * @description Constructor. Sets trigger context variables to member variables
     */
    public AccountContactRelationTriggerHandler() {
        this.newAccountContactRelations = (List<AccountContactRelation>) Trigger.new;
        this.oldAccountContactRelations = (List<AccountContactRelation>) Trigger.old;
        this.newAccountContactRelationsByIds = (Map<Id, AccountContactRelation>) Trigger.newMap;
        this.oldAccountContactRelationsByIds = (Map<Id, AccountContactRelation>) Trigger.oldMap;
    }

    /**
     * @description beforeInsert handler. Executes CDO Sync
     */
    protected override void beforeInsert() {
        CDOSync.synchronizeFromACRs(newAccountContactRelations);
    }
}