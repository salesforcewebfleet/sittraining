/**
 * @author        	Msalmenautio
 * @author       	Salesforce
 * @description	    This class is a controller for Shipment notification visualforce email template
 * @see           	ShipmentNotification_Controller
 * @data 31/03/20    Msalmenautio    Initial setup
 */


global with sharing class  ShipmentNotificationController  {
    
    	global String caseId{get;set;} 
    	global List<ShipmentItem__c> getShipmentItem() {
        List<ShipmentItem__c> shipmentItem = new List<ShipmentItem__c>([Select ShipmentId__r.Name,Name,  ShipmentId__r.WEBFLEETContractId__r.Name , ShipmentQuantity__c, ShipmentId__r.CarrierCode__c, ShipmentId__r.CarrierTracking__c, ShipmentId__r.CarrierTrackingURLforEmail__c, ShipmentId__r.SAPShipmentDate__c From ShipmentItem__c where ShipmentId__r.Case__c= :caseId]);

        return shipmentItem;    
        }
}