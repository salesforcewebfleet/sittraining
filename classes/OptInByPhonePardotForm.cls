public class OptInByPhonePardotForm extends PardotForm {
    public OptInByPhonePardotForm() {
        super('https://e.webfleet.com/l/844203/2020-11-16/xxlr8');

        formFields.add('email');
        formFields.require('email');
        formFields.add('country');
        formFields.require('country');
    }

    public void updateForm(String email, String country) {
        updateIncludeFormField('email', email);
        updateIncludeFormField('country', country);
    }
}