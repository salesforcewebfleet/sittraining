/**
 * @author        	fheeneman
 * @author       	Salesforce
 * @description	    Contains methods to enable the synchronization mechanism from the CDO__c object to the ACR object.
 * @see           	CDOSync_test Test class
 * @data 7/2/20    fheeneman    Initial setup
 */
public without sharing class CDOSync {

    /**
     * @description Lazy loading of the fieldMapping. Contains fields on the ACR object mapped to fields on the CDO object. cdoField => acrField
     */
    static Map<String, String> fieldMapping {
        get {
            if (fieldMapping == null) {
                fieldMapping = getCDOMapping();
            }
            return fieldMapping;
        }
        private set;
    }

    /**
     * @description Retrieves the field mapping from the custom metadata and maps the ACR fields to the CDO fields.
     *
     * @return Map of cdoField => acrField
     */
    public static Map<String, String> getCDOMapping() {
        Map<String, String> fieldMapping = new Map<String, String>();
        List<CDOMapping__mdt> cdoMappings = [SELECT Id, ACRField__c, CDOField__c FROM CDOMapping__mdt];
        for (CDOMapping__mdt cdoMapping : cdoMappings) {
            fieldMapping.put(cdoMapping.CDOField__c, cdoMapping.ACRField__c);
        }
        return fieldMapping;
    }

    /**
     * @description Synchronizes to the supplied ACR records from the relevant CDO records. Relevant means that the ACR is related to the same Account as the CDO.
     *
     * @param acrs ACRs to synchronize to
     */
    public static void synchronizeFromACRs(List<AccountContactRelation> acrs) {
        // get accounts from the affected ACRs
        Set<Id> accountIds = new Set<Id>();
        for (AccountContactRelation acr : acrs) {
            accountIds.add(acr.AccountId);
        }

        // retrieve related CDOs based on the accountId
        // Query is built based on the fields in the mapping
        // Account__c is always queried as it's necessary to build the CDO map
        String queryString = 'SELECT Account__c, ';
        for (String cdoField : fieldMapping.keySet()) {
            queryString += cdoField + ',';
        }
        queryString = queryString.removeEnd(',');
        queryString += ' FROM CDO__c WHERE Account__c IN :accountIds';

        List<CDO__c> cdos = Database.query(String.escapeSingleQuotes(queryString));
        Map<Id, CDO__c> cdosByAccountIds = new Map<Id, CDO__c>();

        for (CDO__c cdo : cdos) {
            cdosByAccountIds.put(cdo.Account__c, cdo);
        }

        for (AccountContactRelation acr : acrs) {
            CDO__c cdo = cdosByAccountIds.get(acr.AccountId);
            // If we find a CDO, we map the new values to the ACR. Else we need to clear it.
            if (cdo != null) {
                mapCDOToACR(cdo, acr, false);
            } else {
                mapCDOToACR(cdo, acr, true);
            }
        }

        // update ACRs if it's not a before trigger, because in a before trigger the changes get committed automatically
        // Trigger.isBefore can be null when not in a trigger context
        if ((Trigger.isBefore != null && !Trigger.isBefore) || System.isFuture() || System.isBatch()) {
            TriggerHandler.bypass('AccountContactRelationTriggerHandler');
            update acrs;
            TriggerHandler.clearBypass('AccountContactRelationTriggerHandler');
        }
    }

    /**
     * @description Maps values of fields of the supplied CDO to fields on the ACR depending on the field mapping that is configured in the CDOMapping__mdt custom metadata type.
     *
     * @param cdo CDO record that contains the values that need to be synced
     * @param acr ACR record that should be synchronized with the CDO record
     * @param clearValues Indicates whether the values need to be cleared from the ACR, for instance in a scenario where the CDO was deleted.
     */
    @TestVisible
    private static void mapCDOToACR(CDO__c cdo, AccountContactRelation acr, Boolean clearValues) {
        for (String cdoField : fieldMapping.keySet()) {
            String acrField = fieldMapping.get(cdoField);

            try {
                if (!clearValues) {
                    acr.put(acrField, cdo.get(cdoField));
                } else {
                    acr.put(acrField, null);
                }
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, 'While mapping CDO__c.' + cdoField + ' to AccountContactRelation.' + acrField + ': ' + e.getMessage());
                System.debug(LoggingLevel.ERROR, e.getStackTraceString());
            }
        }
    }
}