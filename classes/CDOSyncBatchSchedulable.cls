/**
 * @author        	fheeneman
 * @author       	Salesforce
 * @description	    Description
 * @see           	CDOSyncBatchSchedulable_test Test class
 * @date 4/3/20    fheeneman    Initial setup
 */
public with sharing class CDOSyncBatchSchedulable implements System.Schedulable {

    /**
     * @description Schedules the CDOSyncBatch
     *
     * @param schedulableContext
     */
    public void execute(SchedulableContext schedulableContext) {
        Database.executeBatch(new CDOSyncBatch());
    }

}