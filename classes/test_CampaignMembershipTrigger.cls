@isTest
//
// test class for the checkLeadCampaignMembership and checkContactCampaignMembership triggers
// Tom Tavernier - May 2013
//
public class test_CampaignMembershipTrigger
{

static testMethod void testCampaignMembership()
    {
        Campaign cp = new Campaign();
        cp.Name='Test Campaign';
        insert cp;

        Lead ld = new Lead();
        ld.LastName='LastName';
        ld.Company='Company';
        ld.email='a.b@c.com';
        ld.Primary_Fleet_Size__c=10;
        ld.Campaign_Id__c=cp.id;
        insert ld;

        Contact ct =new Contact(LastName='LastName',Campaign_Id__c=cp.id, FirstName = 'FirstName' );
        insert ct;

        list<Contact> ct1=[select Active__c from Contact where Id=:ct.Id LIMIT 1];
        ct1[0].Active__c = 'Active MKT Nurture';
        update ct1;
    }

}