/**
 * @author      fheeneman
 * @author      Salesforce
 * @description Description
 *              Tests executed:
 *              1 - testPost() - description
 *              2 - testPostWithException() - description
 * @date 28/4/20		fheeneman		Initial setup
 */
@IsTest
private class InstallationNotificationScheduler_test {

    @IsTest
    static void testBehavior() {
        // Arrange
        String cron = '0 0 23 * * ?';

        // NB: test data not needed, because we're not testing Apex Batch results
        // That will be tested somewhere else

        // Verify that AsyncApexJob is empty
        // not strictly necessary but makes what is going on later clearer
        List<AsyncApexJob> jobsBefore = [SELECT Id, ApexClassId, ApexClass.Name, Status, JobType FROM AsyncApexJob];
        System.assertEquals(0, jobsBefore.size(), 'not expecting any asyncjobs');

        // Act
        Test.startTest();
        System.schedule('testjob', cron, new InstallationNotificationScheduler());
        Test.stopTest();

        // Assert
        // Check schedulable is in the job list
        List<AsyncApexJob> jobsScheduled = [SELECT Id, ApexClassId, ApexClass.Name, Status, JobType FROM AsyncApexJob WHERE JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
        System.assertEquals('InstallationNotificationScheduler', jobsScheduled[0].ApexClass.Name, 'expecting InstallationNotificationScheduler scheduled job');

        // check apex batch is in the job list
        List<AsyncApexJob> jobsApexBatch = [SELECT Id, ApexClassId, ApexClass.Name, Status, JobType FROM AsyncApexJob WHERE JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
        System.assertEquals('InstallationNotificationBatch', jobsApexBatch[0].ApexClass.Name, 'expecting InstallationNotificationBatch batch job');
    }
}