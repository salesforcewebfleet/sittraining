/**
 * @description Test class for WebfleetAuthProviderPlugin
 *
 * @author fheeneman Salesforce
 *
 * @date 24/6/20		fheeneman		Initial setup
 */
@IsTest
private class WebfleetAuthProviderPlugin_test {

    private static final String ACCESS_TOKEN = 'testToken';
    private static final String REFRESH_TOKEN = 'refreshToken';
    private static final String STATE = 'mocktestState';
    private static final String SUB = 'testLoginId';
    private static final String EMAIL_ADDRESS = 'testEmailAddress';
    private static final String CALLBACK_URL = 'http://localhost/services/authcallback/';
    private static final String CLIENT_ID = 'testKey';
    private static final String CLIENT_SECRET = 'testSecret';
    private static final String SCOPE = 'testScope';
    private static final String AUTHORIZE_URL = 'http://www.dummyhost.com/authorizeUri';
    private static final String TOKEN_URL = 'http://www.dummyhost.com/accessTokenUri';
    private static final String USER_INFO_URL = 'www.dummyhost.com/user/api';

    @IsTest
    static void getCustomMetadataType() {
        // Act
        Test.startTest();
        WebfleetAuthProviderPlugin authProviderPlugin = new WebfleetAuthProviderPlugin();
        String result = authProviderPlugin.getCustomMetadataType();
        Test.stopTest();

        // Assert
        System.assertEquals('WebfleetAuthProvider__mdt', result, 'Method did not return the correct metadata type.');
    }
    @IsTest
    static void initiate() {
        // Arrange
        Map<String, String> config = setupAuthProviderConfig();
        WebfleetAuthProviderPlugin plugin = new WebfleetAuthProviderPlugin();

        // Act
        Test.startTest();
        PageReference result = plugin.initiate(config, STATE);
        Test.stopTest();

        // Assert
        String url = result.getUrl();
        System.assert(url.startsWith(AUTHORIZE_URL), 'URL did not start with the Authorize URL');
        System.assert(url.contains(CLIENT_ID), 'URL did not contain the Client Id');
        System.assert(url.contains(SCOPE), 'URL did not contain the Scope');
        System.assert(url.contains(EncodingUtil.urlEncode(CALLBACK_URL, 'UTF-8')), 'URL did not contain the (URL Encoded) Callback Url');
        System.assert(url.contains(STATE), 'URL did not contain the State');

    }

    @IsTest
    static void handleCallback() {
        // Arrange
        Map<String, String> config = setupAuthProviderConfig();
        WebfleetAuthProviderPlugin plugin = new WebfleetAuthProviderPlugin();

        Map<String, String> queryParameters = new Map<String, String>{
                'code' => 'code',
                'state' => STATE
        };

        Auth.AuthProviderCallbackState callbackState = new Auth.AuthProviderCallbackState(null, null, queryParameters);

        Test.setMock(HttpCalloutMock.class, new WebfleetAccessTokenResponseMock());

        // Act
        Test.startTest();
        Auth.AuthProviderTokenResponse tokenResponse = plugin.handleCallback(config, callbackState);
        Test.stopTest();

        // Assert
        System.assertEquals(ACCESS_TOKEN, tokenResponse.oauthToken, 'Method did not return the (correct) Access Token');
        System.assertEquals(REFRESH_TOKEN, tokenResponse.oauthSecretOrRefreshToken, 'Method did not return the (correct) Refresh Token');
        System.assertEquals(STATE, tokenResponse.state, 'Method did not pass on the state');
    }

    @IsTest
    static void getUserInfo() {
        // Arrange
        Map<String, String> config = setupAuthProviderConfig();
        WebfleetAuthProviderPlugin plugin = new WebfleetAuthProviderPlugin();

        Auth.AuthProviderTokenResponse tokenResponse = new Auth.AuthProviderTokenResponse('WEBFLEET', ACCESS_TOKEN, REFRESH_TOKEN, STATE);

        Test.setMock(HttpCalloutMock.class, new WebfleetUserInfoResponseMock());

        // Act
        Test.startTest();
        Auth.UserData userData = plugin.getUserInfo(config, tokenResponse);
        Test.stopTest();

        // Assert
        System.assertEquals(SUB, userData.identifier, 'Method did not return the sub property as identifier');
        System.assertEquals('en', userData.locale, 'Method did not return the locale');
        System.assertEquals(EMAIL_ADDRESS, userData.email, 'Method did not return the email address');
    }

    private static Map<String, String> setupAuthProviderConfig() {
        Map<String, String> config = new Map<String, String>();
        config.put('AuthorizeURL__c', AUTHORIZE_URL);
        config.put('ClientId__c', AUTHORIZE_URL);
        config.put('Scope__c', SCOPE);
        config.put('CallbackURL__c', CALLBACK_URL);
        config.put('ClientId__c', CLIENT_ID);
        config.put('ClientSecret__c', CLIENT_SECRET);
        config.put('AccessTokenUrl__c', TOKEN_URL);
        config.put('UserInfoURL__c', USER_INFO_URL);

        return config;
    }

    public class WebfleetAccessTokenResponseMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            Map<String, String> jsonObject = new Map<String, String>{
                    'access_token' => ACCESS_TOKEN,
                    'refresh_token' => REFRESH_TOKEN
            };

            response.setBody(JSON.serialize(jsonObject));
            return response;
        }
    }

    public class WebfleetUserInfoResponseMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            Map<String, String> jsonObject = new Map<String, String>{
                    'sub' => SUB,
                    'email' => EMAIL_ADDRESS,
                    'locale' => 'en'
            };

            response.setBody(JSON.serialize(jsonObject));
            return response;
        }
    }
}