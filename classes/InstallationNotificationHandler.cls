/**
 * @description Contains methods to send Installation Partner Assignment emails about work orders.
 *
 * @see InstallationNotification_test Test class
 *
 * @author fheeneman, Salesforce
 *
 * @date 17/4/20    fheeneman    Initial setup
 */
public without sharing class InstallationNotificationHandler {

    /**
     * @description Generates and sends the Installation Partner assignment email to the contacts that are linked to the supplied work orders.
     * <p/>
     * Work orders are grouped per case, so only 1 email is sent per case.
     *
     * @param workOrders Work Orders that need to be send to the customer (currently with status Send Pending or Send Failed)
     *
     * @author fheeneman
     *
     * @date 2020-4-17
     */
    public static void sendInstallationAssignmentEmail(List<WorkOrder> workOrders) {
        // Retrieve the email template
        EmailTemplate emailTemplate = [SELECT Id, Subject, Body FROM EmailTemplate WHERE DeveloperName = 'InstallationPartnerNotification'];
        List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();

        Map<Id, CaseEmail> caseEmailsByIds = new Map<Id, CaseEmail>();
        for (WorkOrder workOrder : workOrders) {
            if (!caseEmailsByIds.containsKey(workOrder.CaseId)) {
                caseEmailsByIds.put(workOrder.CaseId, new CaseEmail(workOrder.CaseId, workOrder.Case.ContactId));
                caseEmailsByIds.get(workOrder.CaseId).relatedWorkOrders.add(workOrder);
            } else {
                caseEmailsByIds.get(workOrder.CaseId).relatedWorkOrders.add(workOrder);
            }
        }

        // Render emails
        List<CaseEmail> caseEmails = caseEmailsByIds.values(); // We'll use this list later to handle the send results

        // We need to get the sending address from the org wide address table. If nothing is set or found, we just use the address of the executing user (default behavior).
        List<FulfillmentEmailSettings__mdt> emailSettings = [SELECT FromAddress__c FROM FulfillmentEmailSettings__mdt WHERE DeveloperName = 'InstallationNotification' LIMIT 1];
        FulfillmentEmailSettings__mdt installationSettings = (emailSettings.size() == 1) ? emailSettings.get(0) : null;
        List<OrgWideEmailAddress> orgWideEmailAddresses = [SELECT Id FROM OrgWideEmailAddress WHERE Address = :installationSettings.FromAddress__c LIMIT 1];
        OrgWideEmailAddress orgWideEmailAddress = (orgWideEmailAddresses.size() == 1) ? orgWideEmailAddresses.get(0) : null;

        for (CaseEmail caseEmail : caseEmails) {
            Messaging.SingleEmailMessage emailMessage = Messaging.renderStoredEmailTemplate(emailTemplate.Id, caseEmail.contactId, caseEmail.caseId);
            emailMessage.setSaveAsActivity(true);
            emailMessage.setUseSignature(false);
            emailMessage.setOptOutPolicy('SEND');
            if (orgWideEmailAddress != null) {
                emailMessage.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
            }
            emailMessages.add(emailMessage);
        }

        // Send and handle results
        Messaging.SendEmailResult[] emailResults = Messaging.sendEmail(emailMessages,false);

        List<WorkOrder> toUpdateWorkOrders = new List<WorkOrder>();

        Integer i = 0;
        for (Messaging.SendEmailResult result : emailResults) {
            if (result.isSuccess()) {
                toUpdateWorkOrders.addAll(handleEmailResult(caseEmails[i], 'Sent'));
            } else {
                toUpdateWorkOrders.addAll(handleEmailResult(caseEmails[i], 'Send Failed'));
            }
            i++;
        }

        // update WorkOrder Installation send status
        update toUpdateWorkOrders;

    }

    /**
     * @description Simply updates the status of the work orders in the CaseEmail wrapper
     *
     * @param caseEmail CaseEmail Wrapper class containing the work orders
     * @param status String that represents the send status of the email, stored in WorkOrder.IPAssignmentEmailStatus__c
     *
     * @return List of Work Orders that were stored in the CaseEmail wrapper
     */
    private static List<WorkOrder> handleEmailResult(CaseEmail caseEmail, String status) {
        for (WorkOrder workOrder : caseEmail.relatedWorkOrders) {
            workOrder.IPAssignmentEmailStatus__c = status;
        }
        return caseEmail.relatedWorkOrders;
    }

    /**
     * @description Wrapper class to group work orders based on the related class. Need to use this to be able to easily update the send status of the work orders
     */
    private class CaseEmail {
        String caseId;
        String contactId;
        List<WorkOrder> relatedWorkOrders;

        CaseEmail(Id caseId, Id contactId) {
            this.caseId = caseId;
            this.contactId = contactId;
            relatedWorkOrders = new List<WorkOrder>();
        }
    }
}