@IsTest
private class NPSTouchpointSchedulableTest {

    @IsTest
    static void testBehavior() {
        // Arrange
        String cron = '0 0 23 * * ?';

        // Act
        Test.startTest();
        System.schedule('testjob', cron, new NpsTouchpointSchedulable());
        Test.stopTest();

        // Assert
        System.assert(true, 'nothing to assert.');
    }
}