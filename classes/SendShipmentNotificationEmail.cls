/**
 * @author          Msalmenautio
 * @author          Salesforce
 * @description     This class sends shipment noticication to the customer and it adds email to case feed
 * @see             SendShipmentNotficationEmail
 * @data 31/03/20    Msalmenautio    Initial setup
 */

public with sharing class SendShipmentNotificationEmail {
@InvocableMethod(label='SendShipmentNotificationEmail' description='This will send Shipment Notification Email.' category='Case')

   
    public static void SendShipmentNotification(List<Case> cases) { 
        
            Integer bulksize;
            
            //Sort Cases            
            cases.sort();
            
            //Get bulk size
            
            for (FulfillmentEmailSettings__mdt bulksizerecord :[Select BulkSize__c from FulfillmentEmailSettings__mdt where DeveloperName = 'ShipmentNotification' LIMIT 1]){
                bulksize=Integer.valueOf(bulksizerecord.BulkSize__c);
            }

            //Size of the list
            Integer numberOfRecords = cases.size();
            
            List<Id> caseids = new List<Id>();         
            integer counter = 0;
            integer recordcounter = 0;
            
        
            while (recordcounter<numberOfRecords) {
                    while(counter<=bulksize && recordcounter <numberOfRecords ) {
                        caseids.add(cases[recordcounter].Id); 
                        counter++;
                        recordcounter++;
                    }
                    CreateShipmentNotification(caseids);
                    caseids.clear();
                    counter = 0;
                
            }
            
    }
    @future
    public static void CreateShipmentNotification(List<Id> caseids) {
 
            //Get Id for Visualforce email template
            EmailTemplate et = [SELECT Id,Subject, Body FROM EmailTemplate WHERE DeveloperName = 'Shipment_Notification_eng_us']; 
            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
            List<Case> cases = new List<Case>([Select Id, contactid from Case where Id in :caseids]);
            List<Id> successfullysend = new List<Id>();
            String fromAddress;
        
            //Get from emaill address
            
            for (FulfillmentEmailSettings__mdt fromaddressrecord :[Select FromAddress__c from FulfillmentEmailSettings__mdt where DeveloperName = 'ShipmentNotification' LIMIT 1]){
                fromAddress=fromaddressrecord.FromAddress__c;
            }
 
            //Get OrgWideEmailAddressId
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = :fromAddress];

            for(Case caserecord :cases){
            
                // Create content of the email message    
                Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(et.id,caserecord.contactid,caserecord.Id) ;
                if (owea.size() > 0) { 
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                mail.setSaveAsActivity(true); //This will add email to case feed
                mail.setUseSignature(false);   
                allmsg.add(mail);
            }
                
            try {
  
                Messaging.SendEmailResult[] results = Messaging.sendEmail(allmsg);
                
                //This will pickup case Id's for emails which are send succesfully. Id's are stored to list successfullysend.         
                Integer i=0;
                for(Messaging.SendEmailResult sendEmailResult :results){                             
                        if (sendEmailResult.isSuccess()==true) {
                            successfullysend.add(caseids[i]);
                        }
                    
                        if (sendEmailResult.isSuccess()==false) {
                            system.debug('============== Email exception caught!!!============='+SendEmailResult.getErrors()[0]);
                        }
                        i++;
                }
              
               try {
                    //Update Cases
                    List<Case> casestoupdate = new  List<Case>();
                    For (case caserecords :[Select Id,ShipmentEmailStatus__c from Case Where Id in :successfullysend]){                   
                            caserecords.ShipmentEmailStatus__c='Sent';
                            caserecords.ScheduledShipmentEmailNotification__c = NULL;
                            casestoupdate.add(caserecords);
                    }
                    update casestoupdate;
                    } catch (System.DMLException ex) {                   
                        System.debug('============== DML exception caught!!!============='+ex.getMessage());
                        return ;                    
                    }
                
                    return;
    
                } catch (System.EmailException ex) {                                  
                    System.debug('============== Email exception caught!!!============='+ex.getMessage());
                    return ;             
                }                
        }    
}