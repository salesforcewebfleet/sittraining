/**
 * @author      fheeneman
 * @author      Salesforce
 * @description Test class for CDOSync
 * @date 12/2/20		fheeneman		Initial setup
 */
@IsTest
private class CDOSync_test extends c_TestFactory {

    // Bulkify or not? Look at the settings on the TestFactory to decide how much data to build
    static Integer totalAccounts = c_TestFactory.BULKIFY_TESTS ? 80 : 2;

    // Static variables to use for field mappings in assertions
    static String acrFields = '';
    static String cdoFields = '';
    static List<CDOMapping__mdt> cdoMappings;

    @TestSetup
    static void createData() {
        c_TestFactory.setDefaultContext();

        Account[] accounts = new List<Account>();

        for (Integer i = 0; i < totalAccounts; i++) {
            Account account = (Account) make(Entity.SERVICE_ACCOUNT, new Account());
            accounts.add(account);
        }

        List<Contact> contacts = new List<Contact>();
        // Giving every account 1 contact, which generates an ACR between them by default.
        for (Account account : accounts) {
            contacts.add((Contact) make(Entity.SERVICE_CONTACT, new Contact(Account = account)));
            make(Entity.CDO, new CDO__c(Account__r = account));
        }

        // make some additional ACR links with some logic to not link the contacts to the same account twice
        Integer i = 0;
        for (Contact contact : contacts) {
            if (i < accounts.size() - 1) {
                make(Entity.SERVICE_ACR, new AccountContactRelation(Contact = contact, Account = accounts[i + 1]));
            }
            i++;
        }
        // We don't want the CDO Sync to be trigger right now, because we need to test it in our methods.
        TriggerHandler.bypass('AccountContactRelationTriggerHandler');
        TriggerHandler.bypass('CDOTriggerHandler');
        run();
        TriggerHandler.clearBypass('AccountContactRelationTriggerHandler');
        TriggerHandler.clearBypass('CDOTriggerHandler');
    }

    private static void getFieldMapping() {
        // Get some field mappings to assert our logic.
        // We don't need to test every mapped field, just a couple
        cdoMappings = [SELECT Id, ACRField__c, CDOField__c FROM CDOMapping__mdt LIMIT 2];

        for (CDOMapping__mdt cdoMapping : cdoMappings) {
            cdoFields += cdoMapping.CDOField__c + ',';
            acrFields += cdoMapping.ACRField__c + ',';
        }
        System.assert(cdoMappings.size() > 0, 'No field mappings retrieved. Are you sure there are entries in the CDOMapping__mdt object?');
    }

    private static String getAssertionQueryString() {
        // Based on the field mapping fields, we later check whether the fields were mapped correctly, so we retrieve the Accounts with their related ACRs and CDOs
        String assertionQueryString = 'SELECT ' +
                '(SELECT Id, {0} FROM CDOs__r), ' +
                '(SELECT Id, {1} FROM AccountContactRelations) ' +
                'FROM Account WHERE ' +
                'Id IN (SELECT AccountId FROM AccountContactRelation) AND ' +
                'Id IN (SELECT Account__c FROM CDO__c)';
        assertionQueryString = assertionQueryString.replace('{0}', cdoFields.removeEnd(','));
        assertionQueryString = assertionQueryString.replace('{1}', acrFields.removeEnd(','));
        return assertionQueryString;
    }

    @IsTest
    static void testSynchronizeFromACRs() {
        // Arrange
        c_TestFactory.setDefaultContext();
        List<AccountContactRelation> acrs = [SELECT Id, AccountId FROM AccountContactRelation];

        getFieldMapping();

        // Act
        Test.startTest();
        CDOSync.synchronizeFromACRs(acrs);
        Test.stopTest();

        // Assert
        // build the query based on the mappings we retrieved
        String assertionQueryString = getAssertionQueryString();

        //  retrieve the accounts with their children. It's easier to assert from here.
        List<Account> accounts = Database.query(assertionQueryString);

        for (Account account : accounts) {
            for (AccountContactRelation acr : account.AccountContactRelations) {
                for (CDOMapping__mdt cdoMapping : cdoMappings) {
                    System.assertEquals(
                            account.CDOs__r[0].get(cdoMapping.CDOField__c),
                            acr.get(cdoMapping.ACRField__c),
                            'The CDO field ' + cdoMapping.CDOField__c +
                                    ' was not properly mapped to the ACR field ' +
                                    cdoMapping.ACRField__c
                    );
                }
            }
        }
    }

    @IsTest
    static void testMapCDOToACRUpdateValues() {
        // Arrange
        c_TestFactory.setDefaultContext();
        getFieldMapping();

        // Build the query based on the mappings we retrieved, for this one we can use the same query twice.
        // We still use this Account query as we need the CDOs and ACRs to be related to the same Accounts.
        String queryString = getAssertionQueryString();
        List<Account> accountsPreLogic = Database.query(queryString);

        // Act
        Test.startTest();
        for (Account account : accountsPreLogic) {
            CDOSync.mapCDOToACR(account.CDOs__r[0], account.AccountContactRelations[0], false);
        }
        Test.stopTest();

        // Assert
        List<Account> accountsPostLogic = Database.query(queryString);
        for (Account account : accountsPostLogic) {
            for (AccountContactRelation acr : account.AccountContactRelations) {
                for (CDOMapping__mdt cdoMapping : cdoMappings) {
                    System.assertEquals(
                            account.CDOs__r[0].get(cdoMapping.CDOField__c),
                            acr.get(cdoMapping.ACRField__c),
                            'The CDO field ' + cdoMapping.CDOField__c +
                                    ' was not properly mapped to the ACR field ' +
                                    cdoMapping.ACRField__c
                    );
                }
            }
        }
    }

    @IsTest
    static void testMapCDOToACRClearValues() {
        // Arrange
        c_TestFactory.setDefaultContext();
        getFieldMapping();

        // Build the query based on the mappings we retrieved, for this one we can use the same query twice.
        // We still use this Account query as we need the CDOs and ACRs to be related to the same Accounts.
        String queryString = getAssertionQueryString();
        List<Account> accountsPreLogic = Database.query(queryString);

        // Act
        Test.startTest();
        for (Account account : accountsPreLogic) {
            CDOSync.mapCDOToACR(account.CDOs__r[0], account.AccountContactRelations[0], true);
        }
        Test.stopTest();

        // Assert
        List<Account> accountsPostLogic = Database.query(queryString);
        for (Account account : accountsPostLogic) {
            for (AccountContactRelation acr : account.AccountContactRelations) {
                for (CDOMapping__mdt cdoMapping : cdoMappings) {
                    System.assertEquals(
                            null,
                            acr.get(cdoMapping.ACRField__c),
                            'ACR field ' + cdoMapping.ACRField__c + 'was not cleared properly'
                    );
                }
            }
        }
    }

    @IsTest
    static void testMapCDOToACRException() {
        // Arrange
        c_TestFactory.setDefaultContext();
        getFieldMapping();

        // Act
        Test.startTest();
        CDOSync.mapCDOToACR(null, new AccountContactRelation(), false);
        Test.stopTest();

        // Assert
        System.assert(true, 'Nothing to assert here :D');
    }
}