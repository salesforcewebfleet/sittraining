/**
 * Created by fheeneman on 10/2/20.
 */

trigger AccountContactRelationTrigger on AccountContactRelation (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    new AccountContactRelationTriggerHandler().run();
}