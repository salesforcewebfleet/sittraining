trigger Update_OwnerLink_on_Opportunity_Owner_Update on Opportunity (before update, before insert) {
  // When 'Owner' field is changed, update 'OwnerLink' too
  list<Contact> updateContacts = new list<Contact>();
    // Loop through the incoming records
    for (Opportunity o : Trigger.new) {
        // Has Owner chagned?
        if (o.OwnerID != o.Owner_Link__c) {
            if (String.valueOf(o.OwnerId).substring(0,3) == '005') {
                o.Owner_Link__c = o.OwnerId;
            }
            else {
                o.Owner_Link__c = null;
            }
        }
        //AG1-40927851 – part 2.sn
        if (Trigger.isUpdate)
        {//start of if
            Opportunity oldOppty = Trigger.oldMap.get(o.Id);
            if (oldOppty.StageName <> o.StageName && o.StageName == 'Closed Lost')
            {
              System.debug('Inside Oppty stage change check');
               if (o.AccountId != null)
               {//start of check acc exists
                   list<Account> acc=[select Id, Type from Account where Id=:o.AccountId and Type='Prospect' LIMIT 1];
                   if (acc.size() > 0)
                   {//start of if
                        System.debug('Inside acc size check');
                        for (Contact c1:[SELECT Id, Active__c FROM Contact
                             WHERE AccountId = :o.AccountId AND Active__c = 'Active'])
                        {//start of for to get all contacts against Oppty
                            System.debug('inside update for contact id' + c1.Id);
                            c1.Active__c = 'Active MKT Nurture';
                            updateContacts.add(c1);
                            System.debug('Inside contact Status Update before update');
                             }//end of for to get all contacts against oppty
                             update updateContacts;
                   }//end of if
               }//end of check for account exists
            }
        }//end of if for isUpdate
        //AG1-40927851 – part 2.en
        //CRISTINA – part 3.sn
        if (Trigger.isUpdate)
        {//start of if
            Opportunity oldOppty = Trigger.oldMap.get(o.Id);
            if (oldOppty.StageName <> o.StageName && o.StageName == 'Closed Won')
            {
              System.debug('Inside Oppty stage change check');
               if (o.AccountId != null)
               {//start of check acc exists
                   list<Account> acc=[select Id, Type from Account where Id=:o.AccountId LIMIT 1];
                   if (acc.size() > 0)
                   {//start of if
                        System.debug('Inside acc size check');
                        for (Contact c1:[SELECT Id, Active__c FROM Contact
                             WHERE AccountId = :o.AccountId AND Active__c = 'Active MKT Nurture'])
                        {//start of for to get all contacts against Oppty
                            System.debug('inside update for contact id' + c1.Id);
                            c1.Active__c = 'Active';
                            updateContacts.add(c1);
                            System.debug('Inside contact Status Update before update');
                             }//end of for to get all contacts against oppty
                             update updateContacts;
                   }//end of if
               }//end of check for account exists
            }
        }//end of if for isUpdate
        //CRISTINA – part 3.en
    }
}