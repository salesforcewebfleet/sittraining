trigger TouchPointParticipantTrigger on TouchPointParticipant__c (before insert) {
	  if(Trigger.isInsert){
        TouchPointParticipantHandler.isEmailContactexcludedforNPS(Trigger.New);
    }
}