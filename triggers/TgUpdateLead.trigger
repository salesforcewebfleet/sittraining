trigger TgUpdateLead on Task (after insert) {

    // set up lists you will need
    List<Lead> LeadsToUpdate = new List<Lead>();
    Map<Id, Task> taskMap = new Map<Id, Task>();

    // go through the list of tasks that were inserted
    for (Task t: Trigger.New)
    {
      // if they are related to a Lead, add the Lead id (whoID) and their values to a map
      if (t.WhoId!= null && (t.Type=='Appointment made – Inside Sales'|| t.Type=='Appointment made – Field'))
        {
            taskMap.put(t.WhoId, t);
        }
    }
    // if the map isnt empty
    if (taskMap.size() > 0)
    {
        // get all of the Leads related to the tasks
        LeadsToUpdate = [SELECT Id, Status FROM Lead WHERE Id IN: taskMap.keySet() ];
        // go through the list for each Lead
        for (Lead l: LeadsToUpdate)
        { 
            // set the new Lead status
            l.Appointment_date__c = system.today();
         
        }
            update LeadsToUpdate;
        
    }
}