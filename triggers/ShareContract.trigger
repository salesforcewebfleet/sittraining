/* Author: Abhijit Ghanekar GIL-36
 When a new contract record is created with Org Unit A and Owner of Contract is Say Abhijit Ghanekar
 Sharing is added on contract record for Manager of owner and Manager of Manager and so on in the hierarchy till Manager is blank
 or Number of levels in hierarchy traversed is 10. For example if Veerle is Manager for Abhijit then veerle gets added in record sharing
 with Type equal to Hierarchy and Manager of veerle also gets added and so on. If Manager is having license of type chatter free user then he wont be added
 Controller present on Org Unit which is associated to contract record also gets added in record sharing with Type of sharing = Controller
 Trigger fires an apex class which gets executed asynchronously. Apex class name is ContractSharing
 ###############################################################################################################
 SFT-153 - Head of finance to be added to sharing logic. Field added on Org Unit
 OTS-326405 - Restricted access field if checked then no contract sharing
 */
trigger ShareContract on TT_Contract__c(after insert, after update) {
	Set<Id> ContIds = new Set<Id>();
	Set<Id> OrgUnitIds = new Set<Id>();
	Map<Id, Id> OrgUnit = new Map<Id, Id>();
	Map<Id, Id> OwnerMap = new Map<Id, Id>();
	//used for owner change logic
	Map<Id, Id> OwnerChangeMap = new Map<Id, Id>();
	Set<Id> ContChangeIds = new Set<Id>();
	//used for owner change logic
	Map<Id, Id> Controller = new Map<Id, Id>();
	Map<Id, Id> StreamOwner = new Map<Id, Id>();
	Map<Id, Id> ControllerMap = new Map<Id, Id>();
	Map<Id, Id> StreamOwnerMap = new Map<Id, Id>();
	Map<Id, Id> HeadofFinance = new Map<Id, Id>(); //SFT-153.n
	Map<Id, Id> HeadofFinanceMap = new Map<Id, Id>(); //SFT-153.n

	if(Trigger.new.size() < 2) { 
		//start of if


		for (TT_Contract__c contract :Trigger.new) {
			if(!contract.Restricted_Access__c) 
				//OTS-326405.n
				{   
				ContIds.add(contract.Id);
				if(Trigger.isUpdate) {
					TT_Contract__c oldCont = Trigger.oldMap.get(contract.Id);
					if(oldCont.OwnerId <> contract.OwnerId) { 
						//prepare owner change map for owner change on contract
						OwnerChangeMap.put(contract.Id, contract.OwnerId);
						ContChangeIds.add(contract.Id);
					}//end of prepare owner change map
				}
			}                        //OTS-326405.n
		}
		list<TT_Contract__c> contlist = [select Id, Org_Unit__c, OwnerId from TT_Contract__c where Id IN :ContIds];
		if(contlist.size() > 0) { 
			//start if 1

			for (TT_Contract__c contlist1 :contlist) {
				OrgUnit.put(contlist1.Org_Unit__c, contlist1.Id);
				OwnerMap.put(contlist1.Id, contlist1.OwnerId);
				OrgUnitIds.Add(contlist1.Org_Unit__c);
			}

		}//end if 1
		if(Trigger.isInsert) {
			list<Org_Unit__c> OrgList = [select Id, Controller__c, Stream_Owner__c, Head_of_Finance__c from Org_Unit__c where Id IN :OrgUnitIds]; //SFT-153
			if(OrgList.size() > 0) { 
				//start if 2

				for (Org_Unit__c Orglist1 :OrgList) {
					Controller.put(Orglist1.Id, Orglist1.Controller__c);
					StreamOwner.put(Orglist1.Id, Orglist1.Stream_Owner__c);
					HeadofFinance.put(Orglist1.Id, Orglist1.Head_of_Finance__c);
				}

			}//end if 2

			//SFT-153.sn
			for (Id id1 :HeadofFinance.keySet()) {
				for (Id id2 :OrgUnit.keySet()) {
					if(id1.equals(id2)) {
						HeadofFinanceMap.put(OrgUnit.get(id2), HeadofFinance.get(id1));
						break;
					}

				}

			}
			//SFT-153.en

			for (Id id1 :Controller.keySet()) {
				for (Id id2 :OrgUnit.keySet()) {
					if(id1.equals(id2)) {
						ControllerMap.put(OrgUnit.get(id2), Controller.get(id1));
						break;
					}

				}

			}
			for (Id id1 :StreamOwner.keySet()) {
				for (Id id2 :OrgUnit.keySet()) {
					if(id1.equals(id2)) {
						StreamOwnerMap.put(OrgUnit.get(id2), StreamOwner.get(id1));
						break;
					}

				}

			}


			ContractSharing.ShareRecord(ControllerMap, OwnerMap, StreamOwnerMap, HeadofFinanceMap); //SFT-153

		}//end of Trigger.isinsert

		if(Trigger.isUpdate) { 
			// on update of owner on contract recalculate sharing
			system.debug('Owner map after update is >>' + OwnerMap + '<<');
			system.debug('Owner map after update is >>' + ContIds + '<<');
			if(OwnerChangeMap.size() > 0) { 
				// if there is owner change on contract then only invoke the future method to add owner sharing
				if(ContractSharing.CalledfromUserTermination) {
					ContractSharing.ShareRecordForOwnerChangeForUserTermination(OwnerChangeMap, ContChangeIds);
				} else {
					ContractSharing.ShareRecordForOwnerChange(OwnerChangeMap, ContChangeIds);
				}
			}
		}// end of trigger.isupdate
	}//end of if for size check
}