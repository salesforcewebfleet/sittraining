trigger checkContactCampaignMembership on Contact(after insert, after update ){
	List<CampaignMember> cmToAdd = new List<CampaignMember>();
	for (Contact c : trigger.new){
		if (c.Campaign_Id__c != null){
			CampaignMember cmem = new CampaignMember(ContactId = c.Id, CampaignId = c.Campaign_Id__c, Status = 'Responded');
			cmToAdd.add(cmem);
		}
	}
	if (cmToAdd.size() > 0){
		Database.insert(cmToAdd,false);
	}
}