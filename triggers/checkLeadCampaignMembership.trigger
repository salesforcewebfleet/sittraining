trigger checkLeadCampaignMembership on Lead (after insert, after update) {
    List<CampaignMember> cmToAdd = new List<CampaignMember>();
    List<TriggerSettings__mdt> leadMetadata = [SELECT LeadCreateCampaignMembersonChange__c FROM TriggerSettings__mdt WHERE DeveloperName = 'TriggerSettingRecord'];
    String triggerUsers = leadMetadata.size() == 1 ? leadMetadata[0].LeadCreateCampaignMembersonChange__c : null;
    List<SObject> results = Database.query('SELECT Id FROM User WHERE Name IN ('+ triggerUsers +')');
    Set<Id> resultIds = (new Map<Id,SObject>(results)).keySet();
    
    if(!resultIds.isEmpty()){
        for (Lead l : trigger.new){
            if (l.Campaign_Id__c != null && (resultIds !=null &&  resultIds.contains(l.LastModifiedById))){             
                cmToAdd.add(new CampaignMember(LeadId = l.Id, CampaignId = l.Campaign_Id__c));
            }
        }
    }
    
    if (!cmToAdd.isEmpty()){
        Database.Insert(cmToAdd, false);
    }
}