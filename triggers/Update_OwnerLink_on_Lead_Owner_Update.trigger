// Last modification by Cristina Andiñach 10/8/2016 - Add if condition: oldLead.Status == 'Raw Data'

trigger Update_OwnerLink_on_Lead_Owner_Update on Lead (before update, before insert) {
  // When 'Owner' field is changed, update 'OwnerLink' too
    // Loop through the incoming records
 
Map<String,String> m2 = new Map<String,String>();
public set<id> soapnew=new set<id>();
    for (Lead l : Trigger.new) {
        // Has Owner chagned?
        if (l.OwnerID != l.Owner_Link__c) {
            if (String.valueOf(l.OwnerId).substring(0,3) == '005') {
                l.Owner_Link__c = l.OwnerId;
            }
            else {
                l.Owner_Link__c = null;
            }
        }
        //AG-1-40927851.sn Lead conversion owner Id gets stored in Former Owner custom field
       //new requirement is that if the Former Lead Owner field is a queue (thus : not a person), the Lead Owner should NOT be updated when the lead status is set to “Ready to convert”
            if (l.Status == 'MKT Nurture' && Trigger.isInsert)
            {//start of if
                if (String.valueOf(l.OwnerId).substring(0,3) == '005') {
                l.User__c = l.OwnerId;
                }
                else {
                l.User__c = null;
                }
               // l.User__c = l.OwnerId;
            }//end of if
            if (Trigger.isUpdate)
            {//start of isUpdate
  
                Lead oldLead = Trigger.oldMap.get(l.Id);
           
                if (l.Status == 'New' && l.Status <> oldLead.Status && oldLead.Status == 'UNQ Nurture')
                {//start if for Lead MQL
                  soapnew.add(l.Id);
                  m2.put('LeadId',l.Id);
                  //Invoke active lead assignment rule to assign lead needed to write logic in apex class because of self reference
                  //error if we try to assign lead inside apex trigger.
                  AssignLeadMQL.InvokeRule(m2);
                }//end of if Lead MQL Ticket
                if (l.Status == 'MKT Nurture' && Trigger.isUpdate && l.Status <> oldLead.Status)
                {//start of if
                    if (String.valueOf(l.OwnerId).substring(0,3) == '005') {
                    l.User__c = l.OwnerId;
                    }
                     else {
                     l.User__c = null;
                    }

                   // l.User__c = l.OwnerId;
                }//end of if
            }//end of isUpdate
        //AG-1-40927851.en
    }
}