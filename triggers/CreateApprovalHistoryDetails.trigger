trigger CreateApprovalHistoryDetails on Opportunity (after update) {
List<Approval_History_Details__c> listApprHist = new list<Approval_History_Details__c>();
    for(Opportunity opp:Trigger.new)
    {
       
           if (opp.Step_Approved__c == true)
           {
               list<ProcessInstance> processins=new list<ProcessInstance>();
               if (Test.isrunningTest())
               {
               processins = [select LastActorId, Id,TargetObjectId,Status,CompletedDate from ProcessInstance where TargetObjectId=:opp.Id ORDER BY CompletedDate Desc LIMIT 1];
               }
               else
               {
                 processins = [select LastActorId, Id,TargetObjectId,Status,CompletedDate from ProcessInstance where TargetObjectId=:opp.Id and Status = 'Approved' ORDER BY CompletedDate Desc LIMIT 1];
               }
               if (processins.size() > 0)
               {
                      Approval_History_Details__c approvalHistory = new Approval_History_Details__c();
                      approvalHistory.Approver__c = processins[0].LastActorId;
                      approvalHistory.Approval_Date__c = processins[0].CompletedDate;
                      approvalHistory.Opportunity__c = opp.Id;
                      approvalHistory.Requested_Discount__c = opp.Requested_Discount__c;
                      approvalHistory.Reason_for_Request__c = opp.Reason_for_the_request__c;
                      listApprHist.add(approvalHistory);
               }
           }
    }
    insert listApprHist;
}